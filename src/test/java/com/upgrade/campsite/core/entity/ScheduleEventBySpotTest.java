package com.upgrade.campsite.core.entity;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class ScheduleEventBySpotTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void test_addDateRange() {
        //given
        DateRange range1 = DateRange.builder().from(LocalDate.parse("2019-03-10")).to(LocalDate.parse("2019-03-12")).build();
        DateRange range2 = DateRange.builder().from(LocalDate.parse("2019-03-12")).to(LocalDate.parse("2019-03-15")).build();
        DateRange range3 = DateRange.builder().from(LocalDate.parse("2019-03-14")).to(LocalDate.parse("2019-03-17")).build();
        DateRange range4 = DateRange.builder().from(LocalDate.parse("2019-03-19")).to(LocalDate.parse("2019-03-20")).build();
        DateRange range5 = DateRange.builder().from(LocalDate.parse("2019-03-20")).to(LocalDate.parse("2019-03-21")).build();

        ScheduleEventBySpot event = new ScheduleEventBySpot( new Spot(1L) );

        //when
        event.addDateRange(range4.getFrom(), range4.getTo());
        event.addDateRange(range5.getFrom(), range5.getTo());
        event.addDateRange(range2.getFrom(), range2.getTo());
        event.addDateRange(range1.getFrom(), range1.getTo());
        event.addDateRange(range3.getFrom(), range3.getTo());

        //then
        assertEquals( range1, event.getDatesAvailable().get(0));
        assertEquals( range2, event.getDatesAvailable().get(1));
        assertEquals( range3, event.getDatesAvailable().get(2));
        assertEquals( range4, event.getDatesAvailable().get(3));
        assertEquals( range5, event.getDatesAvailable().get(4));
    }

    @Test
    void test_compactConsecutiveDateRanges() {
        //given
        DateRange range1 = DateRange.builder().from(LocalDate.parse("2019-03-10")).to(LocalDate.parse("2019-03-12")).build();
        DateRange range2 = DateRange.builder().from(LocalDate.parse("2019-03-12")).to(LocalDate.parse("2019-03-15")).build();
        DateRange range3 = DateRange.builder().from(LocalDate.parse("2019-03-14")).to(LocalDate.parse("2019-03-17")).build();
        DateRange range4 = DateRange.builder().from(LocalDate.parse("2019-03-19")).to(LocalDate.parse("2019-03-20")).build();
        DateRange range5 = DateRange.builder().from(LocalDate.parse("2019-03-20")).to(LocalDate.parse("2019-03-21")).build();
        DateRange range6 = DateRange.builder().from(LocalDate.parse("2019-03-25")).to(LocalDate.parse("2019-03-28")).build();

        ScheduleEventBySpot event = new ScheduleEventBySpot( new Spot(1L) );

        event.addDateRange(range1.getFrom(), range1.getTo());
        event.addDateRange(range2.getFrom(), range2.getTo());
        event.addDateRange(range3.getFrom(), range3.getTo());
        event.addDateRange(range4.getFrom(), range4.getTo());
        event.addDateRange(range5.getFrom(), range5.getTo());
        event.addDateRange(range6.getFrom(), range6.getTo());

        //when
        event.compactConsecutiveDateRanges();

        //then
        assertEquals(3, event.getDatesAvailable().size());
        assertEquals(DateRange.builder().from(LocalDate.parse("2019-03-10")).to(LocalDate.parse("2019-03-17")).build(), event.getDatesAvailable().get(0));
        assertEquals(DateRange.builder().from(LocalDate.parse("2019-03-19")).to(LocalDate.parse("2019-03-21")).build(), event.getDatesAvailable().get(1));
        assertEquals(DateRange.builder().from(LocalDate.parse("2019-03-25")).to(LocalDate.parse("2019-03-28")).build(), event.getDatesAvailable().get(2));
    }
}
