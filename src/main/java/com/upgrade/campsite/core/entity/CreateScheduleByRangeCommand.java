package com.upgrade.campsite.core.entity;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings("unchecked")
public class CreateScheduleByRangeCommand implements ScheduleCommand {

    /**
     * Get the list of available schedule events grouped by date range.
     * If the result list is empty, means there are not available events for the given parameters.
     *
     * For build the list, this method fragments the given date range by the value of amountDaysForTheReservation
     * and calculates the free spots for that period.
     */
    public List<ScheduleEventByRange> execute(ScheduleEventsHandler handler) {
        Stream<LocalDate> dateRangeStream = handler.getDateFrom().datesUntil(handler.getDateTo());

        List<ScheduleEventByRange> listScheduleByRange = dateRangeStream

            .map(this.createEventWithFreeSpotsForTheGivenDateRange(handler))

            .filter(this.takeOutTheRemaingRanges(handler))

            .filter(this.keepWithAllEventsThatApplyForTheGivenRequiredSpotsParam(handler))

            .collect(Collectors.toList());

        return listScheduleByRange;
    }

    Function<LocalDate, ScheduleEventByRange> createEventWithFreeSpotsForTheGivenDateRange(ScheduleEventsHandler handler){
        return dateStreamFrom -> {
            LocalDate dateStreamTo = dateStreamFrom.plusDays(handler.getAmountDaysForTheReservation());

            Set<Spot> freeSpotsForThisDay = new HashSet<>(handler.getCampsite().getFreeSpots(dateStreamFrom, dateStreamTo));

            return new ScheduleEventByRange(dateStreamFrom, dateStreamTo, freeSpotsForThisDay);
        };
    }

    Predicate<ScheduleEventByRange> takeOutTheRemaingRanges(ScheduleEventsHandler handler){
        return event -> event.getTo().isBefore(handler.getDateTo()) || event.getTo().isEqual(handler.getDateTo());
    }

    Predicate<ScheduleEventByRange> keepWithAllEventsThatApplyForTheGivenRequiredSpotsParam(ScheduleEventsHandler handler){
        return event -> event.hasEnoughSpace(handler.getRequiredSpots());
    }
}
