package com.upgrade.campsite.core.entity;

public enum CampsiteAvailabilityStatus {
    AVAILABLE(MessageId.CAMPSITE_AVAILABLE),
    FULL(MessageId.CAMPSITE_FULL_CAPACITY),
    CLOSED(MessageId.CAMPSITE_CLOSED);

    private MessageId messageId;

    CampsiteAvailabilityStatus(MessageId messageId) {
        this.messageId = messageId;
    }

    public MessageId getMessageId() {
        return messageId;
    }

    @Override
    public String toString() {
        return this.name().toLowerCase();
    }
}
