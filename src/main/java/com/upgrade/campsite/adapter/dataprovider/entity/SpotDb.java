package com.upgrade.campsite.adapter.dataprovider.entity;

import com.upgrade.campsite.core.entity.Spot;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "spot")
public class SpotDb {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String description;

    @ManyToOne
	@JoinColumn(name = "campsite_id", referencedColumnName = "id")
    private CampsiteDb campsite;

    public SpotDb() { }

    public SpotDb(Long id) {
        this.id = id;
    }

    public Spot toBusinessEntity(){
	    Spot spot = new Spot();
	    spot.setDescription(getDescription());
	    spot.setId(getId());
	    return spot;
    }
}
