USE `campsitedb`;

INSERT INTO `campsitedb`.`camper`(`id`, `email`, `fullname`)
VALUES (1, 'francoagarcia@gmail.com', 'Franco');

INSERT INTO `campsitedb`.`campsite`(`id`,
                                    `maximum_reservation_time`,
                                    `minimum_period_before_reservation`,
                                    `maximum_period_before_reservation`,
                                    `opened_from`,
                                    `opened_to`,
                                    `initial_checkin_time`,
                                    `limit_checkin_time`)
VALUES ('5f108bfc-47d6-4a75-8a52-ce79790c73d6',
        259200000000000, -- 3 days in nanos
        86400000000000, -- 1 day in nanos
        2592000000000000, -- 30 days in nanos
        '2019-1-1',
        '2019-6-30',
        '12:00',
        '12:00');

COMMIT;

CALL `campsitedb`.`createSpots`(30);
