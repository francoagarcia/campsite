# Run the app
## Local with a memory database (H2)
For a quick view of the app you can run it with:
```bash
./gradlew bootRun -Dspring.profiles.active=h2
```

## Local with a MySQL Database
The app run inside a docker container that interacts with another container but for the MySQL Database.
First, you will need to build the project and the docker image:
```bash
./gradlew build docker
```

Then, you can use docker-compose to start both containers:
```bash
docker-compose up
``` 

The app will start on `localhost:8080`.

## AWS
The database is also installed on a Amazon Relational Database Service. <br /> 

Run the app pointing to this database using the profile _prod_:

```bash
docker run -e "SPRING_PROFILES_ACTIVE=prod" -p 8080:8080 -t francoagarcia/campsite
```

Also, you can run it using gradle:

```bash
./gradlew bootRun -Dspring.profiles.active=prod
```

The application is also running in a EC2 container on AWS. It's configured with the auto-scale setup for **support a large and incremental volume of requests**. <br />
You can check it using, for example: 
```bash
curl -X GET http://18.224.45.240:8080/campers/1
```

### Debugging
- To debug the application, **JPDA Transport** can be used. To enable this feature pass a java agent settings in JAVA_OPTS variable and map agent’s port to localhost during a container run.
```bash
docker run -e "JAVA_OPTS=-agentlib:jdwp=transport=dt_socket,address=5005,server=y,suspend=n" -p 8080:8080 -p 5005:5005 -t francoagarcia/campsite
``` 

# Architecture

* [x] Clean Architecture
* [x] Docker
* [x] Gradle
* [x] Spring Boot
* [x] JPA
* [x] JUnit + Mockito for Unit Tests
* [x] Gatling for Load Tests

## Clean architecture
This project follows the [Uncle Bob's guides](http://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html):
* The core of the application is the use cases, not the database.
* Independent of frameworks: Isolate the core of the application from all the framework's things.
* Independent of databases.
* Independent of any external agency.

![image](https://user-images.githubusercontent.com/4080509/55927390-4257c000-5beb-11e9-8d9c-1f5ac86e61cb.png)

I developed the project with the next rules:

* The `core/` directory contains the use cases and entities.
  - The `core/entities/` has all the entities with the enterprise business rules.
  - The `core/usecases/` has all the use cases with has the business rules for the use case.
  - The `core/port/` has the *details*. The core of the application is the use cases, all the rest are details. This directory has all the interfaces of the details for example, repositories, utils, etc.
  - The use cases could throw UseCaseExceptions.
* The `entrypoint/` directory is the entry point to our Use Cases. An entry point prepares the request model for the use case and prepares the response model for the outside world. It has nothing about framework's things.

Until this point, there is nothing about frameworks. No spring, no hibernate, just pure java.
* The `adapter/` directory has the details implementation, like Repositories.
* The `configuration/` directory has all the framework things! 
  - The `configuration/router/` directory has all the Router objects (old @Controllers) and is the entry point to our API. It has framework's things.
  - The `configuration/router/exception` directory has the Exception Handlers.
  - The `configuration/injectors/` has the injectors configurations like the beans definition. It also has some libraries configuration like Jackson.

# DER

![image](https://user-images.githubusercontent.com/4080509/55927306-f86eda00-5bea-11e9-9577-5ac8c1dc70d8.png)

# Endpoints
## Get the campsite availability
Retrieve when the campsite is available for a given date range. 
If the date range is empty, takes one month by default.

* **URL**

  /campsite/availability

* **Method:**
  
  `GET`
  
*  **URL Params**

   **Optional:**
   - `date_from=[string]`         format (yyyy-MM-dd), default now
   - `date_to=[string]`           format (yyyy-MM-dd), default one month after now
   - `required_spots=[integer]`   default 1
   - `reservation_days=[integer]` default 1
   - `schedule_type=[string]`     enumerated {'by_spot', 'by_range'}, default 'by_spot' 

* **Success Response:**

  * **Code:** 200 <br />
    **Content: by_spot** <br />
    ```json
        {
            "status": "available|full|closed",
            "schedule": [
                {
                    "spot_id": 1,
                    "dates_available": [
                        {
                            "from": "2019-04-10",
                            "to": "2019-05-10"
                        }
                    ]
                 }
            ]
        }
    ```
  
  * **Code:** 200 <br />
    **Content: by_range** <br />
      ```json
          {
              "status": "available|full|closed",
              "schedule": [
                  {
                      "from": "2019-04-07",
                      "to": "2019-04-08",
                      "free_spots": [ 1 ]
                  }
              ]
          }
      ```
 
* **Error Response:**

  * **Code:** 400 BAD REQUEST - Thrown when the request data break any business rule <br />
    **Content:** <br /> 
    ```json
        {
            "timestamp": "2019-04-11T00:29:39.451+0000",
            "message": "some message indicating that a business rule has been broken",
            "detail": "some detail message about the exception",
            "status": 400
        }
    ```

  * **Code:** 422 UNPROCESSABLE ENTITY - Thrown when the server cannot deserialize the request data (letters in a number field, empty body, etc) <br />
    **Content:** <br /> 
    ```json
        { 
            "timestamp": "2019-04-11T00:29:39.451+0000",
            "message": "The parameter date_to of value 2019-99-99 could not be converted to type LocalDate",
            "detail": "[org.springframework.web.method.annotation.MethodArgumentTypeMismatchException] - Failed to convert value...",
            "status": 422
        }
    ```

  * **Code:** 500 INTERNAL SERVER ERROR - Thrown when an unexpected error on the server occurs <br />
    **Content:** <br /> 
    ```json
        { 
            "timestamp": "2019-04-11T00:29:39.451+0000",
            "message": "An error occurred on the server",
            "detail": "[java.lang.NullPointerException]...",
            "status": 500
        }
    ```

* **Sample Call:**

```bash
    curl -X GET 'http://localhost:8080/campsite/availability?date_from=2019-04-10&date_to=2019-04-30&required_spots=1&reservation_days=1&schedule_type=by_spot'
```

## Make a reservation

* **URL**

  /campsite/reservations

* **Method:**
  
  `POST`
  
*  **Data Request**
  
  ```json
      {
          "email": "test@gmail.com",
          "fullname": "Test Name",
          "arrival_date": "2019-04-15",
          "departure_date": "2019-04-17",
          "required_spots": 1
      }
  ```
  
  * The `required_spots` field is optional, for default is one.

* **Success Response:**

  * **Code:** 200 <br />
    **Content** <br />
      ```json
          {
              "reservation_id": "ZeQjvn"      
          }
      ```
 
* **Error Response:**

  * **Code:** 400 BAD REQUEST - Thrown when the request data break any business rule <br />
    **Content:** <br /> 
    ```json
        {
            "timestamp": "2019-04-11T00:29:39.451+0000",
            "message": "some message indicating that a business rule has been broken",
            "detail": "some detail message about the exception",
            "status": 400
        }
    ```

  * **Code:** 422 UNPROCESSABLE ENTITY - Thrown when the server cannot deserialize the request data (letters in a number field, empty body, etc) <br />
    **Content:** <br /> 
    ```json
        { 
            "timestamp": "2019-04-11T00:29:39.451+0000",
            "message": "The parameter date_to of value 2019-99-99 could not be converted to type LocalDate",
            "detail": "[org.springframework.web.method.annotation.MethodArgumentTypeMismatchException] - Failed to convert value...",
            "status": 422
        }
    ```

  * **Code:** 500 INTERNAL SERVER ERROR - Thrown when an unexpected error on the server occurs <br />
    **Content:** <br /> 
    ```json
        { 
            "timestamp": "2019-04-11T00:29:39.451+0000",
            "message": "An error occurred on the server",
            "detail": "[java.lang.NullPointerException]...",
            "status": 500
        }
    ```

* **Sample Call:**

```bash
    curl -X POST 'http://localhost:8080/campsite/reservations' \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    -d '{ 
        "email": "test@gmail.com",
        "fullname": "Test Name",
        "arrival_date": "2019-04-15",
        "departure_date": "2019-04-17"
     }'
```

## Modify a reservation
* **URL**

  /campsite/reservations/${reservation_id}

* **Method:**
  
  `PUT`
  
*  **Data Request**
  
  ```json
      {
          "email": "test@gmail.com",
          "fullname": "Test Name",
          "arrival_date": "2019-04-15",
          "departure_date": "2019-04-17",
          "required_spots": 1
      }
  ```
  
  * Every field is optional, but you have to pass at least one. 

* **Success Response:**

  * **Code:** 204 NO CONTENT
 
* **Error Response:**

  * **Code:** 404 NOT FOUND - Thrown when doesn't exist a reservation with the given reservation ID <br />
    **Content:** <br /> 
    ```json
        {
            "timestamp": "2019-04-11T00:29:39.451+0000",
            "message": "Reservation not found",
            "detail": "some detail message about the exception",
            "status": 404
        }
    ```

  * **Code:** 400 BAD REQUEST - Thrown when the request data break any business rule <br />
    **Content:** <br /> 
    ```json
        {
            "timestamp": "2019-04-11T00:29:39.451+0000",
            "message": "some message indicating that a business rule has been broken",
            "detail": "some detail message about the exception",
            "status": 400
        }
    ```

  * **Code:** 422 UNPROCESSABLE ENTITY - Thrown when the server cannot deserialize the request data (letters in a number field, empty body, etc) <br />
    **Content:** <br /> 
    ```json
        { 
            "timestamp": "2019-04-11T00:29:39.451+0000",
            "message": "The parameter date_to of value 2019-99-99 could not be converted to type LocalDate",
            "detail": "[org.springframework.web.method.annotation.MethodArgumentTypeMismatchException] - Failed to convert value...",
            "status": 422
        }
    ```

  * **Code:** 500 INTERNAL SERVER ERROR - Thrown when an unexpected error on the server occurs <br />
    **Content:** <br /> 
    ```json
        { 
            "timestamp": "2019-04-11T00:29:39.451+0000",
            "message": "An error occurred on the server",
            "detail": "[java.lang.NullPointerException]...",
            "status": 500
        }
    ```

* **Sample Call:**

```bash
    curl -X POST 'http://localhost:8080/campsite/reservations/ABDE' \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    -d '{ 
        "fullname": "NEW TEST NAME"
     }'
```

## Cancel a reservation
* **URL**

  /campsite/reservations/${reservation_id}

* **Method:**
  
  `DELETE`

* **Success Response:**

  * **Code:** 204 NO CONTENT
 
* **Error Response:**

  * **Code:** 404 NOT FOUND - Thrown when doesn't exist a reservation with the given reservation ID <br />
    **Content:** <br /> 
    ```json
        {
            "timestamp": "2019-04-11T00:29:39.451+0000",
            "message": "Reservation not found",
            "detail": "some detail message about the exception",
            "status": 404
        }
    ```

  * **Code:** 400 BAD REQUEST - Thrown when the request data break any business rule <br />
    **Content:** <br /> 
    ```json
        {
            "timestamp": "2019-04-11T00:29:39.451+0000",
            "message": "some message indicating that a business rule has been broken",
            "detail": "some detail message about the exception",
            "status": 400
        }
    ```

  * **Code:** 422 UNPROCESSABLE ENTITY - Thrown when the server cannot deserialize the request data (letters in a number field, empty body, etc) <br />
    **Content:** <br /> 
    ```json
        { 
            "timestamp": "2019-04-11T00:29:39.451+0000",
            "message": "The parameter date_to of value 2019-99-99 could not be converted to type LocalDate",
            "detail": "[org.springframework.web.method.annotation.MethodArgumentTypeMismatchException] - Failed to convert value...",
            "status": 422
        }
    ```

  * **Code:** 500 INTERNAL SERVER ERROR - Thrown when an unexpected error on the server occurs <br />
    **Content:** <br /> 
    ```json
        { 
            "timestamp": "2019-04-11T00:29:39.451+0000",
            "message": "An error occurred on the server",
            "detail": "[java.lang.NullPointerException]...",
            "status": 500
        }
    ```

* **Sample Call:**

```bash
    curl -X POST 'http://localhost:8080/campsite/reservations/ABDE'
```

## Get camper by ID
Addiotionally, I added this endpoint for retrieve the camper data using its Id (with testing purposes).

* **URL**

  /campers/${ID}

* **Method:**
  
  `GET`

* **Success Response:**

  * **Code:** 200 <br />
    **Content** <br />
    ```json
        {
            "id": 1,
            "fullname": "Test Name",
            "email": "testemail@gmail.com"
        }
    ```

* **Error Response:**

  * **Code:** 404 NOT FOUND - Thrown when there is no Camper with the given ID <br />
    **Content:** <br /> 
    ```json
        {
            "timestamp": 1234567890,
            "message": "The Camper ID is required",
            "detail": "some detail message about the exception",
            "status": 400
        }
    ```

  * **Code:** 422 UNPROCESSABLE ENTITY - Thrown when the server cannot deserialize the request data (letters in a number field, empty body, etc) <br />
    **Content:** <br /> 
    ```json
        { 
            "timestamp": "2019-04-11T00:29:39.451+0000",
            "message": "The parameter date_to of value 2019-99-99 could not be converted to type LocalDate",
            "detail": "[org.springframework.web.method.annotation.MethodArgumentTypeMismatchException] - Failed to convert value...",
            "status": 422
        }
    ```

  * **Code:** 500 INTERNAL SERVER ERROR - Thrown when an unexpected error on the server occurs <br />
    **Content:** <br /> 
    ```json
        { 
            "timestamp": "2019-04-11T00:29:39.451+0000",
            "message": "An error occurred on the server",
            "detail": "[java.lang.NullPointerException]...",
            "status": 500
        }
    ```

* **Sample Call:**

```bash
    curl -X GET 'http://localhost:8080/campers/1'
```

# Tests
## Unit tests
In the `test/` directory we have all the unit tests. For execute them: 

```bash
./gradlew test
```

## Load tests
For running the load tests, first you have to start the app in `localhost:8080`. For a real test, use the mysql db with Docker:
```bash
./gradlew build docker
docker-compose up
``` 

In the `load/` directory we have the load test for the **Make a reservation Endpoint**. For execute them: 

```bash
./gradlew loadTest
```

The results will be saved in the `build/simulation-results` directory.
