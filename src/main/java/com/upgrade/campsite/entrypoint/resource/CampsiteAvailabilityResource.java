package com.upgrade.campsite.entrypoint.resource;

import com.upgrade.campsite.core.entity.CampsiteAvailabilitySchedule;
import com.upgrade.campsite.core.entity.CampsiteAvailabilityStatus;
import com.upgrade.campsite.core.entity.ScheduleEvent;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

@Getter
@Setter
public class CampsiteAvailabilityResource<RESOURCE> {

    private CampsiteAvailabilityStatus status;
    private List<RESOURCE> schedule;

    private CampsiteAvailabilityResource(CampsiteAvailabilityStatus status, List<RESOURCE> schedule) {
        this.status = status;
        this.schedule = schedule;
    }

    public static <BRESOURCE> CampsiteAvailabilityResourceBuilder<BRESOURCE> builder() {
        return new CampsiteAvailabilityResourceBuilder<>();
    }

    public static class CampsiteAvailabilityResourceBuilder<BRESOURCE> {
        private CampsiteAvailabilityResourceBuilder() { }

        public CampsiteAvailabilityResource<BRESOURCE> toResource(CampsiteAvailabilitySchedule<? extends ScheduleEvent> campsiteAvailabilitySchedule) {
            requireNonNull(campsiteAvailabilitySchedule, "Can't create a valid resource from a null entity");

            CampsiteAvailabilityStatus status = campsiteAvailabilitySchedule.getStatus();
            List<BRESOURCE> scheduleEvent = (List<BRESOURCE>) campsiteAvailabilitySchedule.getSchedule().stream()
                .map(ScheduleEvent::toResource)
                .collect(Collectors.toList());

            CampsiteAvailabilityResource<BRESOURCE> resource = new CampsiteAvailabilityResource<>(status, scheduleEvent);

            return resource;
        }
    }

}
