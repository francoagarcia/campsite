package com.upgrade.campsite.entrypoint.impl;

import com.upgrade.campsite.core.entity.CampsiteAvailabilitySchedule;
import com.upgrade.campsite.core.entity.ScheduleEvent;
import com.upgrade.campsite.core.entity.ScheduleType;
import com.upgrade.campsite.core.usecase.RetrieveCampsiteAvailability;
import com.upgrade.campsite.entrypoint.RetrieveCampsiteAvailabilityEndpoint;
import com.upgrade.campsite.entrypoint.resource.CampsiteAvailabilityResource;

import javax.inject.Inject;
import java.time.LocalDate;

public class DefaultRetrieveCampsiteAvailabilityEndpoint<EVENTENTITY extends ScheduleEvent, EVENTRESOURCE> implements RetrieveCampsiteAvailabilityEndpoint<EVENTRESOURCE> {

    private RetrieveCampsiteAvailability<EVENTENTITY> retrieveCampsiteAvailability;

    @Inject
    public DefaultRetrieveCampsiteAvailabilityEndpoint(RetrieveCampsiteAvailability<EVENTENTITY> retrieveCampsiteAvailability) {
        this.retrieveCampsiteAvailability = retrieveCampsiteAvailability;
    }

    @Override
    public CampsiteAvailabilityResource<EVENTRESOURCE> execute(String campsiteId, LocalDate dateFrom, LocalDate dateTo, Integer requiredSpots, Integer reservationDays, ScheduleType scheduleType) {
        CampsiteAvailabilitySchedule<? extends ScheduleEvent> campsiteAvailability = this.retrieveCampsiteAvailability
            .initialize(campsiteId, dateFrom, dateTo, requiredSpots, reservationDays, scheduleType)
            .get();
        return CampsiteAvailabilityResource.<EVENTRESOURCE>builder().toResource(campsiteAvailability);
    }
}
