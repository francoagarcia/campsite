package com.upgrade.campsite.core.entity;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class DateRangeTest {


    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void test_isBeforeOrIncludes_includes() {
        //given
        LocalDate from = LocalDate.parse("2019-03-15");
        LocalDate to = LocalDate.parse("2019-03-17");
        DateRange dateRange = DateRange.builder().from(from).to(to).build();

        LocalDate anotherFrom = LocalDate.parse("2019-03-16");
        LocalDate anotherTo = LocalDate.parse("2019-03-18");
        DateRange anotherDateRange = DateRange.builder().from(anotherFrom).to(anotherTo).build();

        //when
        boolean result = dateRange.isImmediatelyPreviousOrIncludes(anotherDateRange);

        //then
        assertTrue(result);
    }

    @Test
    void test_isBeforeOrIncludes_consecutive() {
        //given
        LocalDate from = LocalDate.parse("2019-03-15");
        LocalDate to = LocalDate.parse("2019-03-17");
        DateRange dateRange = DateRange.builder().from(from).to(to).build();

        LocalDate anotherFrom = LocalDate.parse("2019-03-17");
        LocalDate anotherTo = LocalDate.parse("2019-03-18");
        DateRange anotherDateRange = DateRange.builder().from(anotherFrom).to(anotherTo).build();

        //when
        boolean result = dateRange.isImmediatelyPreviousOrIncludes(anotherDateRange);

        //then
        assertTrue(result);
    }

    @Test
    void test_isBeforeOrIncludes_notConsecutive() {
        //given
        LocalDate from = LocalDate.parse("2019-03-15");
        LocalDate to = LocalDate.parse("2019-03-16");
        DateRange dateRange = DateRange.builder().from(from).to(to).build();

        LocalDate anotherFrom = LocalDate.parse("2019-03-17");
        LocalDate anotherTo = LocalDate.parse("2019-03-18");
        DateRange anotherDateRange = DateRange.builder().from(anotherFrom).to(anotherTo).build();

        //when
        boolean result = dateRange.isImmediatelyPreviousOrIncludes(anotherDateRange);

        //then
        assertFalse(result);
    }

    @Test
    void test_isBeforeOrIncludes_notConsecutiveNeitherIncludes() {
        //given
        LocalDate from = LocalDate.parse("2019-03-15");
        LocalDate to = LocalDate.parse("2019-03-16");
        DateRange dateRange = DateRange.builder().from(from).to(to).build();

        LocalDate anotherFrom = LocalDate.parse("2019-03-10");
        LocalDate anotherTo = LocalDate.parse("2019-03-12");
        DateRange anotherDateRange = DateRange.builder().from(anotherFrom).to(anotherTo).build();

        //when
        boolean result = dateRange.isImmediatelyPreviousOrIncludes(anotherDateRange);

        //then
        assertFalse(result);
    }

    @Test
    void test_builder_ok() {
        //given
        LocalDate from = LocalDate.parse("2019-03-15");
        LocalDate to = LocalDate.parse("2019-03-16");

        //when
        DateRange dateRange = DateRange.builder().from(from).to(to).build();

        //then
        assertNotNull(dateRange);
        assertNotNull(dateRange.getFrom());
        assertNotNull(dateRange.getTo());
    }

    @Test
    void test_builder_fromCannotBeEmpty() {
        //given
        LocalDate from = null;
        LocalDate to = LocalDate.parse("2019-03-16");

        //then
        assertThrows(NullPointerException.class, () -> DateRange.builder().from(from).to(to).build());
    }

    @Test
    void test_builder_toCannotBeEmpty() {
        //given
        LocalDate from = LocalDate.parse("2019-03-15");
        LocalDate to = null;

        //then
        assertThrows(NullPointerException.class, () -> DateRange.builder().from(from).to(to).build());
    }

    @Test
    void test_builder_fromCannotBeGreaterThanTo() {
        //given
        LocalDate from = LocalDate.parse("2019-03-15");
        LocalDate to = LocalDate.parse("2019-03-12");

        //then
        assertThrows(IllegalArgumentException.class, () -> DateRange.builder().from(from).to(to).build());
    }

    @Test
    void test_builder_fromCannotBeEqualsToTo() {
        //given
        LocalDate from = LocalDate.parse("2019-03-15");
        LocalDate to = LocalDate.parse("2019-03-15");

        //then
        assertThrows(IllegalArgumentException.class, () -> DateRange.builder().from(from).to(to).build());
    }

    @Test
    void test_compareTo_zero() {
        //given
        LocalDate from = LocalDate.parse("2019-03-15");
        LocalDate to = LocalDate.parse("2019-03-17");
        DateRange dateRange = DateRange.builder().from(from).to(to).build();

        LocalDate anotherFrom = LocalDate.parse("2019-03-15");
        LocalDate anotherTo = LocalDate.parse("2019-03-17");
        DateRange anotherDateRange = DateRange.builder().from(anotherFrom).to(anotherTo).build();

        //when
        int result = dateRange.compareTo(anotherDateRange);

        //then
        assertEquals(0, result);
    }

    @ParameterizedTest
    @CsvSource({
        "2019-03-19, 2019-03-20",
        "2019-03-16, 2019-03-19",
        "2019-03-16, 2019-03-18",
        "2019-03-15, 2019-03-19",
    })
    void test_compareTo_minusOne(String anotherFromStr, String anotherToStr) {
        //given
        LocalDate from = LocalDate.parse("2019-03-15");
        LocalDate to = LocalDate.parse("2019-03-18");
        DateRange dateRange = DateRange.builder().from(from).to(to).build();

        LocalDate anotherFrom = LocalDate.parse(anotherFromStr);
        LocalDate anotherTo = LocalDate.parse(anotherToStr);
        DateRange anotherDateRange = DateRange.builder().from(anotherFrom).to(anotherTo).build();

        //when
        int result = dateRange.compareTo(anotherDateRange);

        //then
        assertEquals(-1, result);
    }

    @ParameterizedTest
    @CsvSource({
        "2019-03-10, 2019-03-12",
        "2019-03-15, 2019-03-16",
        "2019-03-12, 2019-03-15",
        "2019-03-14, 2019-03-16",
    })
    void test_compareTo_plusOne(String anotherFromStr, String anotherToStr) {
        //given
        LocalDate from = LocalDate.parse("2019-03-15");
        LocalDate to = LocalDate.parse("2019-03-18");
        DateRange dateRange = DateRange.builder().from(from).to(to).build();

        LocalDate anotherFrom = LocalDate.parse(anotherFromStr);
        LocalDate anotherTo = LocalDate.parse(anotherToStr);
        DateRange anotherDateRange = DateRange.builder().from(anotherFrom).to(anotherTo).build();

        //when
        int result = dateRange.compareTo(anotherDateRange);

        //then
        assertEquals(1, result);
    }
}
