package com.upgrade.campsite.entrypoint.resource;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Getter
@Setter
public class ErrorApi {

    private Timestamp timestamp = new Timestamp(System.currentTimeMillis());
    private String message;
    private String detail;
    private int status;

    private ErrorApi(){}

    public static ErrorApiBuilder builder(){
        return new ErrorApiBuilder();
    }

    public static class ErrorApiBuilder {
        private String detail;
        private String message;
        private int status;

        private ErrorApiBuilder(){}

        public ErrorApiBuilder message(String message){
            this.message = message;
            return this;
        }

        public ErrorApiBuilder detail(String detail){
            this.detail = detail;
            return this;
        }

        public ErrorApiBuilder status(int status){
            this.status = status;
            return this;
        }

        public ErrorApi build(){
            ErrorApi errorApi = new ErrorApi();
            errorApi.message = this.message;
            errorApi.detail = this.detail;
            errorApi.status = this.status;
            return errorApi;
        }
    }
}
