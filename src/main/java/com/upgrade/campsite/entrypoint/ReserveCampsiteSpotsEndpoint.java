package com.upgrade.campsite.entrypoint;

import com.upgrade.campsite.entrypoint.resource.ReservationResource;
import lombok.Data;

import java.time.LocalDate;

/**
 * Entry point for the ReserveCampsiteSpots use case.
 * Initialize the use case and return the presentation model.
 */
public interface ReserveCampsiteSpotsEndpoint {

    /**
     * Execute the use case and return the presentation model.
     *
     * @param campsiteId camspite id
     * @param reservationRequest reservation request
     * @return reservation resource
     */
    ReservationResource execute(String campsiteId, ReservationRequest reservationRequest);

    @Data
    class ReservationRequest {
        private String email;
        private String fullname;
        private LocalDate arrivalDate;
        private LocalDate departureDate;
        private Integer requiredSpots;
    }
}
