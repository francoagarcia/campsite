package com.upgrade.campsite.core.port.repository;

import com.upgrade.campsite.core.entity.Campsite;
import com.upgrade.campsite.core.entity.Spot;

import java.util.Set;

public interface SpotRepository {

    /**
     * Find the Spots for a given Campsite.
     *
     * @param campsite Campsite's spots
     * @return Set of Spots
     */
    Set<Spot> findForCampsite(Campsite campsite);
}
