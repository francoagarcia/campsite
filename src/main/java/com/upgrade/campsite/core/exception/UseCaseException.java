package com.upgrade.campsite.core.exception;

import com.upgrade.campsite.core.entity.MessageId;
import org.springframework.http.HttpStatus;

public class UseCaseException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	protected Integer statusCode = HttpStatus.BAD_REQUEST.value();
    protected MessageId messageId;
    protected Object[] args;

    public UseCaseException(MessageId messageId) {
        this.messageId = messageId;
    }

    public UseCaseException(MessageId messageId, Object... args) {
        this.messageId = messageId;
        this.args = args;
    }

    public String getMessageId() {
        return messageId.getId();
    }

    public Object[] getArgs() {
        return args;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    @Override
    public String getMessage() {
        return this.messageId.getDefaultMessage();
    }
}
