package com.upgrade.campsite.configuration.injector;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.upgrade.campsite.adapter.util.impl.DefaultHashingHelper;
import com.upgrade.campsite.adapter.util.impl.DefaultI18n;
import com.upgrade.campsite.core.port.util.HashingHelper;
import com.upgrade.campsite.core.port.util.I18n;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.filter.CommonsRequestLoggingFilter;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static org.springframework.beans.factory.config.BeanDefinition.SCOPE_SINGLETON;

@Configuration
public class UtilsModule {

    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:messages");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

    @Bean
    public AcceptHeaderLocaleResolver localeResolver(){
        List<Locale> supportedLocales = Arrays.asList(
            Locale.US,
            new Locale("es", "AR")
        );

        AcceptHeaderLocaleResolver headerLocaleResolver = new AcceptHeaderLocaleResolver();
        headerLocaleResolver.setDefaultLocale(Locale.US);
        headerLocaleResolver.setSupportedLocales(supportedLocales);

        return headerLocaleResolver;
    }

    @Bean
    @Scope(SCOPE_SINGLETON)
    public I18n defaultI18n(MessageSource messageSource){
        return new DefaultI18n(messageSource);
    }

    @Bean
    @Scope(SCOPE_SINGLETON)
    public HashingHelper hashidsHelper(){
        return new DefaultHashingHelper();
    }

    @Bean
    public ObjectMapper jacksonObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();

        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        objectMapper.configure(SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
        objectMapper.configure(DeserializationFeature.READ_ENUMS_USING_TO_STRING, true);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper
            .registerModule(new Jdk8Module())
            .registerModule(new JavaTimeModule());

        return objectMapper;
    }

    @Bean
    public CommonsRequestLoggingFilter logFilter() {
        CommonsRequestLoggingFilter filter = new CommonsRequestLoggingFilter();
        filter.setIncludeQueryString(true);
        filter.setIncludePayload(false);
        filter.setMaxPayloadLength(10000);
        filter.setIncludeHeaders(false);
        filter.setBeforeMessagePrefix(">>>>>> ");
        filter.setAfterMessagePrefix("<<<<<< ");
        return filter;
    }
}
