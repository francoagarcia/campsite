package com.upgrade.campsite.core.usecase;

import com.upgrade.campsite.core.entity.CampsiteAvailabilitySchedule;
import com.upgrade.campsite.core.entity.DateRange;
import com.upgrade.campsite.core.entity.ScheduleEvent;
import com.upgrade.campsite.core.entity.ScheduleType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.function.Supplier;

/**
 * Retrieve the campsite schedule.
 * For a given Campsite, tentative arrival & departure date and a number of spots required,
 * returns if the campsite is available.
 *
 * Throws UseCaseException if any constraint is violated.
 * Throws NotFoundException if any entity is not found.
 */
public interface RetrieveCampsiteAvailability<EVENTENTITY extends ScheduleEvent> extends Supplier<CampsiteAvailabilitySchedule<EVENTENTITY>> {

    /**
     * Initialize the use case.
     *
     * @param campsiteId Campsite for which the reservation was made.
     * @param dateFrom arrival date
     * @param dateTo departure date
     * @param requiredSpots number of required spots
     * @return RetrieveCampsiteAvailability instance initialized
     */
	default RetrieveCampsiteAvailability<EVENTENTITY> initialize(String campsiteId, LocalDate dateFrom, LocalDate dateTo, Integer requiredSpots, Integer reservationDays, ScheduleType scheduleType){
		this.setModel(new Model(campsiteId, dateFrom, dateTo, requiredSpots, reservationDays, scheduleType));
		return this;
	}

    /**
     * Set & validate the request model handled by the Use Case.
     *
     * @param model request model
     */
	void setModel(Model model);

    /**
     * Request model
     */
    @Getter
    @Setter
	class Model {
	    private String campsiteId;
		private LocalDate dateFrom;
		private LocalDate dateTo;
		private Integer requiredSpots;
		private Integer reservationDays;
		private ScheduleType scheduleType;

        public Model(String campsiteId, LocalDate dateFrom, LocalDate dateTo, Integer requiredSpots, Integer reservationDays, ScheduleType scheduleType) {
            this.campsiteId = campsiteId;
            this.dateFrom = dateFrom;
            this.dateTo = dateTo;
            this.requiredSpots = requiredSpots;
            this.reservationDays = reservationDays;
            this.scheduleType = scheduleType;
        }

        public DateRange getDateRange(){
            return DateRange.builder().from(this.dateFrom).to(this.dateTo).build();
        }
    }
}
