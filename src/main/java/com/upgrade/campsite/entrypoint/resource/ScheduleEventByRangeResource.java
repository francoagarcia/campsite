package com.upgrade.campsite.entrypoint.resource;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@Builder
public class ScheduleEventByRangeResource {
    private LocalDate from;
    private LocalDate to;
    private Set<Long> freeSpots;

}
