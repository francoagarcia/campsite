package com.upgrade.campsite.core.entity;

import com.upgrade.campsite.entrypoint.resource.ScheduleEventBySpotResource;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class ScheduleEventBySpot implements ScheduleEvent {

    private Spot spot;
    private List<DateRange> datesAvailable = new ArrayList<>();

    public ScheduleEventBySpot() { }
    public ScheduleEventBySpot(Spot spot) {
        this.spot = spot;
    }

    public void addDateRange(LocalDate from, LocalDate to){
        this.datesAvailable.add(DateRange.builder().from(from).to(to).build());
        this.datesAvailable = this.datesAvailable.stream().sorted().collect(Collectors.toList());
    }

    public void compactConsecutiveDateRanges(){
        List<DateRange> newRanges = new ArrayList<>();

        DateRange currDateRange = this.datesAvailable.get(0);
        for (int i = 0; i < this.datesAvailable.size()-1; i++) {
            if(currDateRange.isImmediatelyPreviousOrIncludes(this.datesAvailable.get(i+1))){ //if are consecutive => join them
                currDateRange = DateRange.builder()
                    .from(currDateRange.getFrom())
                    .to(this.datesAvailable.get(i+1).getTo())
                    .build();
            } else {
                newRanges.add(currDateRange);
                currDateRange = this.datesAvailable.get(i+1);
            }
        }
        if(currDateRange != null){
            newRanges.add(currDateRange);
        }

        this.datesAvailable = newRanges;
    }

    @Override
    public ScheduleEventBySpotResource toResource() {
        return ScheduleEventBySpotResource.builder()
            .spotId(this.spot.getId())
            .datesAvailable(this.datesAvailable)
            .build();
    }
}
