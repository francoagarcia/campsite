package com.upgrade.campsite.core.usecase.impl;

import com.upgrade.campsite.core.entity.Camper;
import com.upgrade.campsite.core.entity.MessageId;
import com.upgrade.campsite.core.exception.NotFoundException;
import com.upgrade.campsite.core.exception.UseCaseException;
import com.upgrade.campsite.core.port.repository.CamperRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doReturn;

class DefaultRetrieveCamperByIdTest {

    private DefaultRetrieveCamperById useCase;
    private CamperRepository camperRepository;

    @BeforeEach
    void setUp() {
        this.camperRepository = Mockito.mock(CamperRepository.class);

        this.useCase = Mockito.spy(
            new DefaultRetrieveCamperById(camperRepository)
        );
    }

    @AfterEach
    void tearDown() {
    }

    /*
     * Tests for DefaultRetrieveCamperById#initialize()
     */
    @Test
    void test_initialize() {
        //given
        Long id = 1L;

        //when
        assertDoesNotThrow(() -> {
            useCase.initialize(id);
        });
    }

    @Test
    void test_initialize_requiredId() {
        //given
        Long id = null;

        //when
        var ex = assertThrows(UseCaseException.class, () -> {
            useCase.initialize(id);
        });
        assertEquals(MessageId.CAMPER_ID_REQUIRED.getId(), ex.getMessageId());
    }

    /*
     * Tests for DefaultRetrieveCamperById#get()
     */
    @Test
    void test_get() {
        //given
        Long id = 1L;

        doReturn(Optional.of(Camper.builder().id(id).email("test_1@gmail.com").build()))
            .when(camperRepository).findOne(anyLong());

        useCase.initialize(id);

        //when
        Camper camper = useCase.get();

        //then
        assertNotNull(camper);
        assertEquals(id, camper.getId());
    }

    @Test
    void test_get_notFound() {
        //given
        Long id = 1L;

        doReturn(Optional.empty())
            .when(camperRepository).findOne(anyLong());

        useCase.initialize(id);

        //then
        assertThrows(NotFoundException.class, () -> {
            useCase.get();
        });
    }
}
