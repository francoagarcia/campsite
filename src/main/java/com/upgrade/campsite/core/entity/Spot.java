package com.upgrade.campsite.core.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
public class Spot {

    private Long id;
	private String description;

    public Spot() { }

    public Spot(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Spot spot = (Spot) o;
        return id.equals(spot.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
