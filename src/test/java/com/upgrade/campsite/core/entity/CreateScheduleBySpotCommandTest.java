package com.upgrade.campsite.core.entity;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;

class CreateScheduleBySpotCommandTest {

    private CreateScheduleBySpotCommand command;
    private Campsite campsite;

    @BeforeEach
    void setUp() {
        this.command = Mockito.spy(CreateScheduleBySpotCommand.class);
        this.campsite = Mockito.spy(Campsite.class);
        this.campsite.setSpots( createSpots() );
    }

    private Set<Spot> createSpots(){
        return new HashSet<>( Arrays.asList(
            new Spot(1L),
            new Spot(2L),
            new Spot(3L)
        ));
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void test_execute_oneDayForReservation_allSpotsAvailable_dateRangesAllCompacted() {
        //given
        LocalDate dateFrom = LocalDate.of(2019, 3, 1);
        LocalDate dateTo = LocalDate.of(2019, 3, 5);
        Integer requiredSpots = 1;
        Integer amountDaysForTheReservation = 1;

        doReturn(new ArrayList<>(Arrays.asList(new Spot(1L), new Spot(2L), new Spot(3L))))
            .when(campsite)
            .getFreeSpots(LocalDate.parse("2019-03-01"), LocalDate.parse("2019-03-02"));

        doReturn(new ArrayList<>(Arrays.asList(new Spot(1L), new Spot(2L), new Spot(3L))))
            .when(campsite)
            .getFreeSpots(LocalDate.parse("2019-03-02"), LocalDate.parse("2019-03-03"));

        doReturn(new ArrayList<>(Arrays.asList(new Spot(1L), new Spot(2L), new Spot(3L))))
            .when(campsite)
            .getFreeSpots(LocalDate.parse("2019-03-03"), LocalDate.parse("2019-03-04"));

        doReturn(new ArrayList<>(Arrays.asList(new Spot(1L), new Spot(2L), new Spot(3L))))
            .when(campsite)
            .getFreeSpots(LocalDate.parse("2019-03-04"), LocalDate.parse("2019-03-05"));

        doReturn(new ArrayList<>(Arrays.asList(new Spot(1L), new Spot(2L), new Spot(3L))))
            .when(campsite)
            .getFreeSpots(LocalDate.parse("2019-03-05"), LocalDate.parse("2019-03-06"));

        ScheduleEventsHandler handler = new ScheduleEventsHandler(dateFrom, dateTo, requiredSpots, amountDaysForTheReservation, ScheduleType.BY_SPOT);
        handler.setCampsite(campsite);

        //when
        List<ScheduleEventBySpot> events = this.command.execute(handler);

        //then
        assertNotNull(events);
        assertEquals(3, events.size());
        assertEquals(3, events.stream().map(ScheduleEventBySpot::getDatesAvailable).mapToLong(Collection::size).sum() );
        events.forEach(event -> assertEquals(1, event.getDatesAvailable().size()));
    }

    @Test
    void test_execute_oneDayForReservation_someSpotsNotAvailable_datesAreNotCompated() {
        //given
        LocalDate dateFrom = LocalDate.of(2019, 3, 1);
        LocalDate dateTo = LocalDate.of(2019, 3, 5);
        Integer requiredSpots = 1;
        Integer amountDaysForTheReservation = 1;

        doReturn(new ArrayList<>(Arrays.asList(new Spot(1L))))
            .when(campsite)
            .getFreeSpots(LocalDate.parse("2019-03-01"), LocalDate.parse("2019-03-02"));

        doReturn(new ArrayList<>(Arrays.asList(new Spot(2L))))
            .when(campsite)
            .getFreeSpots(LocalDate.parse("2019-03-02"), LocalDate.parse("2019-03-03"));

        doReturn(new ArrayList<>(Arrays.asList(new Spot(1L))))
            .when(campsite)
            .getFreeSpots(LocalDate.parse("2019-03-03"), LocalDate.parse("2019-03-04"));

        doReturn(new ArrayList<>(Arrays.asList(new Spot(1L), new Spot(2L))))
            .when(campsite)
            .getFreeSpots(LocalDate.parse("2019-03-04"), LocalDate.parse("2019-03-05"));

        doReturn(new ArrayList<>(Arrays.asList(new Spot(1L), new Spot(2L))))
            .when(campsite)
            .getFreeSpots(LocalDate.parse("2019-03-05"), LocalDate.parse("2019-03-06"));

        ScheduleEventsHandler handler = new ScheduleEventsHandler(dateFrom, dateTo, requiredSpots, amountDaysForTheReservation, ScheduleType.BY_SPOT);
        handler.setCampsite(campsite);

        //when
        List<ScheduleEventBySpot> events = this.command.execute(handler);

        //then
        assertNotNull(events);
        assertEquals(2, events.size());
        assertEquals(2, events.get(0).getDatesAvailable().size());
        assertEquals(2, events.get(1).getDatesAvailable().size());
    }

    @Test
    void test_execute_throwsException_forUnknownSpot() {
        //given
        LocalDate dateFrom = LocalDate.of(2019, 3, 1);
        LocalDate dateTo = LocalDate.of(2019, 3, 5);
        Integer requiredSpots = 1;
        Integer amountDaysForTheReservation = 1;

        doReturn(new ArrayList<>(Arrays.asList(new Spot(1L), new Spot(2L), new Spot(9999999L))))
            .when(campsite)
            .getFreeSpots(LocalDate.parse("2019-03-01"), LocalDate.parse("2019-03-02"));

        ScheduleEventsHandler handler = new ScheduleEventsHandler(dateFrom, dateTo, requiredSpots, amountDaysForTheReservation, ScheduleType.BY_SPOT);
        handler.setCampsite(campsite);

        //when
        assertThrows(IllegalArgumentException.class, () -> this.command.execute(handler));
    }
}
