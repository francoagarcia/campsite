package com.upgrade.campsite.adapter.dataprovider.repository.impl;

import com.upgrade.campsite.adapter.dataprovider.entity.*;
import com.upgrade.campsite.core.entity.Campsite;
import com.upgrade.campsite.core.entity.DateRange;
import com.upgrade.campsite.core.entity.Reservation;
import com.upgrade.campsite.core.entity.ReservationStatus;
import com.upgrade.campsite.core.port.repository.ReservationRepository;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class DefaultReservationRepository extends SimpleJpaRepository<ReservationDb, Long> implements ReservationRepository {

    private EntityManager em;

	public DefaultReservationRepository(Class<ReservationDb> domainClass, EntityManager em) {
		super(domainClass, em);
		this.em = em;
	}

	@Override
	public Optional<Reservation> findOne(Long id) {
		Optional<ReservationDb> reservationDb = super.findById(id);
        return reservationDb.map(ReservationDb::toBusinessEntity);
    }

    @Override
    public List<Reservation> findActiveReservations(Campsite campsite) {
        CriteriaBuilder cb = this.em.getCriteriaBuilder();

        CriteriaQuery<ReservationDb> cq = cb.createQuery(ReservationDb.class);
        Root<ReservationDb> reservation = cq.from(ReservationDb.class);

        Predicate byCampsiteId = cb.equal(reservation.get(ReservationDb_.campsite).get(CampsiteDb_.id), campsite.getId());
        Predicate activeReservation = cb.and(
            cb.notEqual(reservation.get(ReservationDb_.status), ReservationStatus.CANCELLED),
            cb.notEqual(reservation.get(ReservationDb_.status), ReservationStatus.FINALIZED)
        );

        cq.where( cb.and(byCampsiteId, activeReservation) );

        TypedQuery<ReservationDb> typedQuery = em.createQuery(cq);
        List<ReservationDb> results = typedQuery.getResultList();
        return results.stream().map(ReservationDb::toBusinessEntity).collect(Collectors.toList());
    }

    @Override
    public List<Reservation> findActiveReservations(Campsite campsite, DateRange dateRange) {
        CriteriaBuilder cb = this.em.getCriteriaBuilder();

        CriteriaQuery<ReservationDb> cq = cb.createQuery(ReservationDb.class);
        Root<ReservationDb> reservation = cq.from(ReservationDb.class);

        Predicate byCampsiteId = cb.equal(reservation.get(ReservationDb_.campsite).get(CampsiteDb_.id), campsite.getId());
        Predicate activeReservation = cb.and(
            cb.notEqual(reservation.get(ReservationDb_.status), ReservationStatus.CANCELLED),
            cb.notEqual(reservation.get(ReservationDb_.status), ReservationStatus.FINALIZED)
        );
        Predicate includedInRange = cb.and(
            cb.greaterThanOrEqualTo(reservation.get(ReservationDb_.arrivalDate), dateRange.getFrom()),
            cb.lessThanOrEqualTo(reservation.get(ReservationDb_.departureDate), dateRange.getTo())
        );

        cq.where( cb.and(byCampsiteId, activeReservation, includedInRange) );

        TypedQuery<ReservationDb> typedQuery = em.createQuery(cq);
        List<ReservationDb> results = typedQuery.getResultList();
        return results.stream().map(ReservationDb::toBusinessEntity).collect(Collectors.toList());
    }

    @Override
    public Reservation createReservationForCampsite(Reservation reservation, Campsite campsite){
	    ReservationDb reservationDb = fromBusinessEntity(reservation, campsite);

	    super.saveAndFlush(reservationDb);

	    reservation.setId(reservationDb.getId());

	    return reservation;
    }

    @Override
    public void update(Reservation reservation){
        CriteriaBuilder cb = this.em.getCriteriaBuilder();
        CriteriaUpdate<ReservationDb> update = cb.createCriteriaUpdate(ReservationDb.class);
        Root<ReservationDb> root = update.from(ReservationDb.class);
        Expression<Boolean> filterPredicate = cb.equal(root.get(ReservationDb_.id), reservation.getId());


        if(reservation.getStatus() != null){
            update.set(root.get(ReservationDb_.status), reservation.getStatus());
        }

        if(reservation.getArrivalDate() != null){
            update.set(root.get(ReservationDb_.arrivalDate), reservation.getArrivalDate());
        }

        if(reservation.getDepartureDate() != null){
            update.set(root.get(ReservationDb_.departureDate), reservation.getDepartureDate());
        }

        if(reservation.getCamper() != null && reservation.getCamper().getId() != null){
            update.set(root.get(ReservationDb_.camper).get(CamperDb_.id), reservation.getCamper().getId());
        }

        update.where(filterPredicate);

        this.em
            .createQuery(update)
            .executeUpdate();
    }

    private static ReservationDb fromBusinessEntity(Reservation reservation, Campsite campsite){
        ReservationDb reservationDb = new ReservationDb();
        reservationDb.setId(reservation.getId());
        reservationDb.setCreatedOn(reservation.getCreatedOn());
        reservationDb.setArrivalDate(reservation.getArrivalDate());
        reservationDb.setDepartureDate(reservation.getDepartureDate());
        reservationDb.setStatus(reservation.getStatus());
        reservationDb.setCamper( new CamperDb(reservation.getCamper().getId()) );
        reservationDb.setCampsite( new CampsiteDb(campsite.getId()) );
        return reservationDb;
    }
}
