package com.upgrade.campsite.adapter.dataprovider.entity;

import com.upgrade.campsite.core.entity.Camper;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "camper")
public class CamperDb {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String email;

	private String fullname;

    public CamperDb() { }

    public CamperDb(Long id) {
        this.id = id;
    }

    public Camper toBusinessEntity(){
	    Camper camper = Camper.builder()
            .id(getId())
            .email(getEmail())
            .fullname(getFullname())
            .build();
	    return camper;
    }
}
