package com.upgrade.campsite.configuration.router;

import com.upgrade.campsite.entrypoint.RetrieveCamperByIdEndpoint;
import com.upgrade.campsite.entrypoint.resource.CamperResource;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping(path = "/campers")
public class CamperRouter {

	@Transactional(readOnly = true)
	@RequestMapping(method = GET, path = "/{id}")
	public ResponseEntity<CamperResource> retrieveCamperById(@PathVariable(value = "id") Long id,
                                                             RetrieveCamperByIdEndpoint retrieveCamperByIdEndpoint){
        CamperResource camperResource = retrieveCamperByIdEndpoint.execute(id);
        return ok(camperResource);
	}
}
