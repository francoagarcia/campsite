package com.upgrade.campsite.entrypoint.resource;

import com.upgrade.campsite.core.entity.Camper;
import lombok.Getter;
import lombok.Setter;

import static java.util.Objects.requireNonNull;

@Getter
@Setter
public class CamperResource{

    private Long id;
    private String email;
    private String fullname;

    private CamperResource(Long id, String email, String fullname) {
        this.id = id;
        this.email = email;
        this.fullname = fullname;
    }

    public static CamperResourceBuilder builder() {
        return new CamperResourceBuilder();
    }

    public static class CamperResourceBuilder {
        private CamperResourceBuilder() { }

        public CamperResource toResource(Camper entity) {
            requireNonNull(entity, "Can't create a valid resource from a null entity");
            var resource = new CamperResource(entity.getId(), entity.getEmail(), entity.getFullname());
            return resource;
        }
    }
}
