package com.upgrade.campsite.core.entity;

import java.util.List;

@FunctionalInterface
public interface ScheduleCommand {

    <SE extends ScheduleEvent> List<SE> execute(ScheduleEventsHandler handler);

}
