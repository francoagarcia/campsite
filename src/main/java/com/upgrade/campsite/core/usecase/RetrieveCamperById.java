package com.upgrade.campsite.core.usecase;

import com.upgrade.campsite.core.entity.Camper;
import lombok.Getter;
import lombok.Setter;

import java.util.function.Supplier;

/**
 * Retrieve the Camper data given its ID.
 */
public interface RetrieveCamperById extends Supplier<Camper> {

    /**
     * Initialize the use case.
     *
     * @param id camper ID
     * @return the use case instance
     */
	default RetrieveCamperById initialize(Long id){
		this.setModel(new Model(id));
		return this;
	}

    /**
     * Set & validate the request model handled by the Use Case.
     *
     * @param model request model
     */
	void setModel(Model model);

    /**
     * Request model
     */
    @Getter
    @Setter
	class Model {
		private final Long id;

		private Model(Long id){
			this.id = id;
		}
	}
}
