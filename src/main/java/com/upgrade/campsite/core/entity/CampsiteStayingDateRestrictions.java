package com.upgrade.campsite.core.entity;

import com.upgrade.campsite.core.exception.UseCaseException;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

import static com.upgrade.campsite.core.entity.CampsiteAvailabilityStatus.CLOSED;
import static com.upgrade.campsite.core.entity.MessageId.INVALID_DATE_RANGE;

@Getter
@Setter
@Builder
public class CampsiteStayingDateRestrictions {

    private Duration maximumReservationTime;
    private Duration minimumPeriodBeforeReservation;
    private Duration maximumPeriodBeforeReservation;
    private LocalDate openedFrom;
    private LocalDate openedTo;
    private LocalTime initialCheckinTime;
    private LocalTime limitCheckinTime;

    /**
     * Verify is the campsite is open in the given range.
     *
     * @param dateFrom tentative arrival date
     * @param dateTo tentative departure date
     * @return true if campsite is open
     */
    public Boolean isCampsiteOpenInRange(LocalDate dateFrom, LocalDate dateTo){
        boolean dateFromIsValid = dateFrom.isAfter(openedFrom) && dateFrom.isBefore(openedTo);
        boolean dateToIsValid = dateTo.isAfter(openedFrom) && dateTo.isBefore(openedTo);
        return dateFromIsValid && dateToIsValid;
    }

    /**
     * Validates the date range for the requested reservation.
     * @throws UseCaseException if the date range is invalid for the campsite.
     */
    public void validateStayingDate(LocalDate arrivalDate, LocalDate departureDate) throws UseCaseException{
        if(arrivalDate.isBefore(departureDate) || arrivalDate.isEqual(departureDate)){
            throw new UseCaseException(INVALID_DATE_RANGE);
        }
        if(!isCampsiteOpenInRange(arrivalDate, departureDate)){
            throw new UseCaseException(CLOSED.getMessageId());
        }

        if(isMaxReservationTimeExceed(arrivalDate, departureDate)){
            throw new UseCaseException(MessageId.MAX_RESERVATION_TIME_EXCEED, maximumReservationTime.toDays());
        }

        LocalDateTime reservationInstant = LocalDateTime.now();

        if(isPeriodBeforeReservationLesserThanMinimum(reservationInstant, arrivalDate)){
            throw new UseCaseException(MessageId.MIN_PERIOD_BEFORE_RESERVATION_EXCEED, minimumPeriodBeforeReservation.toDays());
        }

        if(isPeriodBeforeReservationGreaterThanMaximum(reservationInstant, arrivalDate)){
            throw new UseCaseException(MessageId.MAX_PERIOD_BEFORE_RESERVATION_EXCEED, maximumPeriodBeforeReservation.toDays());
        }
    }

    /**
     * Checks if the reservation time between the arrival&departure is greater than the accepted.
     *
     * @param arrivalDate tentative arrival date
     * @param departureDate tentative departure date
     * @return true if is exceed
     */
    public Boolean isMaxReservationTimeExceed(LocalDate arrivalDate, LocalDate departureDate){
        Duration daysBetweenArrivalAndDeparture = Duration.ofDays(ChronoUnit.DAYS.between(arrivalDate, departureDate));
        return maximumReservationTime.compareTo(daysBetweenArrivalAndDeparture) < 0;
    }

    /**
     * Checks if the request reservation time is greater than the accepted.
     *
     * @param amountOfDaysOfTheReservation amount of days of the desired reservation
     * @return true if is exceed
     */
    public Boolean isMaxReservationTimeExceed(Integer amountOfDaysOfTheReservation){
        Duration daysBetweenArrivalAndDeparture = Duration.ofDays(amountOfDaysOfTheReservation);
        return maximumReservationTime.compareTo(daysBetweenArrivalAndDeparture) < 0;
    }

    /**
     * Checks if the period since the request and the reservation start is lesser than the minimum allowed.
     *
     * @param now instant in which the person is making the request (i.e. LocalDateTime.now())
     * @param arrivalDate tentative arrival date
     * @return true if is lesser
     */
    public Boolean isPeriodBeforeReservationLesserThanMinimum(LocalDateTime now, LocalDate arrivalDate){
        LocalDateTime arrivalDateUntilCheckin = arrivalDate.atTime(this.initialCheckinTime);
        Duration periodBeforeReservation = Duration.ofDays(ChronoUnit.DAYS.between(now, arrivalDateUntilCheckin));
        return periodBeforeReservation.compareTo(this.minimumPeriodBeforeReservation) < 0;
    }

    /**
     * Checks if the period since the request and the reservation start is greater than the maximum allowed.
     *
     * @param now instant in which the person is making the request (i.e. LocalDateTime.now())
     * @param arrivalDate tentative arrival date
     * @return true if is greater
     */
    public Boolean isPeriodBeforeReservationGreaterThanMaximum(LocalDateTime now, LocalDate arrivalDate){
        LocalDateTime arrivalDateUntilCheckin = arrivalDate.atTime(this.initialCheckinTime);
        Duration periodBeforeReservation = Duration.ofDays(ChronoUnit.DAYS.between(now, arrivalDateUntilCheckin));
        return periodBeforeReservation.compareTo(this.maximumPeriodBeforeReservation) >= 0;
    }
}
