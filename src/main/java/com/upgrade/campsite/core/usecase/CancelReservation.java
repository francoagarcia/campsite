package com.upgrade.campsite.core.usecase;

import lombok.Getter;
import lombok.Setter;

/**
 * Cancel a reservation given its ID.
 *
 * Throws NotFoundException if the reservation is not found.
 */
public interface CancelReservation {

    /**
     * Initialize the use case.
     *
     * @param campsiteId (optional) Campsite for which the reservation is.
     * @param reservationId reservation ID
     * @return CancelReservation instance initialized
     */
	default CancelReservation initialize(String campsiteId, Long reservationId){
		this.setModel(new Model(campsiteId, reservationId));
		return this;
	}

    /**
     * This use case does not return anything, just executes an action.
     */
	void execute();

    /**
     * Set & validate the request model handled by the Use Case.
     *
     * @param model request model
     */
	void setModel(Model model);

    /**
     * Request model
     */
    @Getter
    @Setter
	class Model {
		private String campsiteId;
		private Long reservationId;

        public Model(String campsiteId, Long reservationId) {
            this.campsiteId = campsiteId;
            this.reservationId = reservationId;
        }
    }
}
