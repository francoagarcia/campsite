package com.upgrade.campsite.entrypoint.impl;

import com.upgrade.campsite.core.usecase.RetrieveCamperById;
import com.upgrade.campsite.entrypoint.RetrieveCamperByIdEndpoint;
import com.upgrade.campsite.entrypoint.resource.CamperResource;

import javax.inject.Inject;

public class DefaultRetrieveCamperByIdEndpoint implements RetrieveCamperByIdEndpoint {

    private RetrieveCamperById retrieveCamperById;

    @Inject
    public DefaultRetrieveCamperByIdEndpoint(RetrieveCamperById retrieveCamperById) {
        this.retrieveCamperById = retrieveCamperById;
    }

    @Override
    public CamperResource execute(Long id) {
        var camper = this.retrieveCamperById.initialize(id).get();
        return CamperResource.builder().toResource(camper);
    }
}
