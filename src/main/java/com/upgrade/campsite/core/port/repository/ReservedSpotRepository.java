package com.upgrade.campsite.core.port.repository;

import com.upgrade.campsite.core.entity.Reservation;
import com.upgrade.campsite.core.entity.ReservedSpot;

import java.util.List;

public interface ReservedSpotRepository {

    /**
     * Find all ReservedSpots for a given Reservation with its Spot data loaded.
     *
     * @return list of ReservedSpots
     */
    List<ReservedSpot> findAllByReservation(Reservation reservation);

    /**
     * Creates the ReservedSpots for a given Reservation.
     *
     * @param reservedSpots list †o store in the db
     * @param reservation reservation to associate the spots
     * @return List of ReservedSpots created
     */
    List<ReservedSpot> createForReservation(List<ReservedSpot> reservedSpots, Reservation reservation);

    /**
     * Updates the list of ReservedSpots for a given reservation
     *
     * @param reservedSpots the new list of ReservedSpots
     * @param reservation reservation with the new ReservedSpots
     */
    void updateReservedSpots(List<ReservedSpot> reservedSpots, Reservation reservation);
}
