package com.upgrade.campsite.adapter.util.impl;

import com.upgrade.campsite.core.port.util.HashingHelper;
import org.apache.commons.lang3.ArrayUtils;
import org.hashids.Hashids;
import org.springframework.beans.factory.annotation.Value;

public class DefaultHashingHelper implements HashingHelper {

    @Value("${hashids.encode.key}")
    private String encodingSalt;

    private final Long NUMBER_PADDING = 9999999L;

    @Override
    public String obfuscate(Long id) {
        Hashids hashids = new Hashids(encodingSalt);
        String hash = hashids.encode(id + NUMBER_PADDING);
        return hash;
    }

    @Override
    public Long decode(String obfuscatedId) {
        Hashids hashids = new Hashids(encodingSalt);
        long[] decodedId = hashids.decode(obfuscatedId);
        return ArrayUtils.isEmpty(decodedId) ? null : (decodedId[0] - NUMBER_PADDING);
    }
}
