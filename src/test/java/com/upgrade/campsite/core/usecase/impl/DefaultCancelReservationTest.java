package com.upgrade.campsite.core.usecase.impl;

import com.upgrade.campsite.core.entity.Campsite;
import com.upgrade.campsite.core.entity.MessageId;
import com.upgrade.campsite.core.entity.Reservation;
import com.upgrade.campsite.core.entity.ReservationStatus;
import com.upgrade.campsite.core.exception.UseCaseException;
import com.upgrade.campsite.core.port.repository.ReservationRepository;
import com.upgrade.campsite.core.usecase.CancelReservation;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doReturn;

class DefaultCancelReservationTest {

    private DefaultCancelReservation useCase;
    private ReservationRepository reservationRepository;

    @BeforeEach
    void setUp() {
        this.reservationRepository = Mockito.mock(ReservationRepository.class);

        this.useCase = Mockito.spy(
            new DefaultCancelReservation(reservationRepository)
        );
    }

    @AfterEach
    void tearDown() {
    }

    /*
     * Tests for DefaultCancelRepository#initialize
     */
    @Test
    void test_initialize_ok() {
        //given
        String campsiteId = "123ABC";
        Long reservationId = 1L;

        //when
        assertDoesNotThrow(() -> {
            useCase.initialize(campsiteId, reservationId);
        });
    }

    @Test
    void test_initialize_reservationIdIsRequired() {
        //given
        String campsiteId = "123ABC";
        Long reservationId = null;

        //when
        UseCaseException ex = assertThrows(UseCaseException.class, () -> {
            useCase.initialize(campsiteId, reservationId);
        });
        assertEquals(MessageId.RESERVATION_ID_REQUIRED.getId(), ex.getMessageId());
    }

    @Test
    void test_initialize_campsiteIdIsNotRequired_setDefault() {
        //given
        String campsiteId = null;
        Long reservationId = 1L;

        //when
        assertDoesNotThrow(() -> {
            useCase.initialize(campsiteId, reservationId);
        });
    }

    /*
     * Tests for DefaultCancelRepository#setDefaults
     */
    @Test
    void test_setDefaults() {
        //given
        String campsiteId = null;
        Long reservationId = 1L;

        CancelReservation.Model model = new CancelReservation.Model(campsiteId, reservationId);

        //when
        useCase.setDefaults(model);

        //then
        assertEquals(Campsite.CAMPSITE_ID, model.getCampsiteId());
    }

    /*
     * Tests for DefaultCancelRepository#execute
     */
    @Test
    void test_execute() {
        //given
        String campsiteId = "123ABC";
        Long reservationId = 1L;

        Reservation r = Reservation.builder().id(reservationId).status(ReservationStatus.PENDING).build();

        doReturn(Optional.of( r ))
            .when(reservationRepository).findOne(anyLong());

        useCase.initialize(campsiteId, reservationId);

        //when
        useCase.execute();

        //then
        assertEquals(r.getStatus(), ReservationStatus.CANCELLED);
    }
}
