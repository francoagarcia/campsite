package com.upgrade.campsite.adapter.dataprovider.repository.impl;

import com.upgrade.campsite.adapter.dataprovider.entity.*;
import com.upgrade.campsite.core.entity.Reservation;
import com.upgrade.campsite.core.entity.ReservedSpot;
import com.upgrade.campsite.core.port.repository.ReservedSpotRepository;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class DefaultReservedSpotRepository extends SimpleJpaRepository<ReservedSpotDb, ReservedSpotDb.ReservedSpotId> implements ReservedSpotRepository {

    private EntityManager em;

	public DefaultReservedSpotRepository(Class<ReservedSpotDb> domainClass, EntityManager em) {
		super(domainClass, em);
		this.em = em;
	}

    @Override
    public List<ReservedSpot> findAllByReservation(Reservation reservation) {
        CriteriaBuilder cb = this.em.getCriteriaBuilder();

        CriteriaQuery<ReservedSpotDb> cq = cb.createQuery(ReservedSpotDb.class);
        Root<ReservedSpotDb> spot = cq.from(ReservedSpotDb.class);

        cq.where(cb.equal(spot.get(ReservedSpotDb_.reservation).get(ReservationDb_.id), reservation.getId()));

        TypedQuery<ReservedSpotDb> typedQuery = em.createQuery(cq);
        List<ReservedSpotDb> results = typedQuery.getResultList();

        return results.stream().map(ReservedSpotDb::toBusinessEntity).collect(Collectors.toList());
    }

    @Override
    public List<ReservedSpot> createForReservation(List<ReservedSpot> reservedSpots, Reservation reservation){
	    List<ReservedSpotDb> reservedSpotDbs = reservedSpots.stream().map(rs -> fromBusinessEntity(rs, reservation)).collect(Collectors.toList());

	    super.saveAll(reservedSpotDbs);
	    super.flush();

	    return reservedSpots;
    }

    @Override
    public void updateReservedSpots(List<ReservedSpot> reservedSpots, Reservation reservation){
	    this.deleteAllByReservation(reservation);
	    this.createForReservation(reservedSpots, reservation);
    }

    private void deleteAllByReservation(Reservation reservation){
        CriteriaBuilder cb = this.em.getCriteriaBuilder();

        CriteriaDelete<ReservedSpotDb> delete = cb.createCriteriaDelete(ReservedSpotDb.class);
        Root<ReservedSpotDb> root = delete.from(ReservedSpotDb.class);
        Expression<Boolean> filterPredicate = cb.equal(root.get(ReservedSpotDb_.reservation).get(ReservationDb_.id), reservation.getId());

        delete.where(filterPredicate);
    }

    private static ReservedSpotDb fromBusinessEntity(ReservedSpot rs, Reservation reservation){
        ReservedSpotDb reservedSpotDb = new ReservedSpotDb();
        reservedSpotDb.setCheckIn(rs.getCheckIn());
        reservedSpotDb.setCheckOut(rs.getCheckOut());
        reservedSpotDb.setSpot( new SpotDb(rs.getSpot().getId()) );
        reservedSpotDb.setReservation( new ReservationDb(reservation.getId()) );
        return reservedSpotDb;
    }

}
