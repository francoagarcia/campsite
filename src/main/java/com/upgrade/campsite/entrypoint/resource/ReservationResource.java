package com.upgrade.campsite.entrypoint.resource;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReservationResource {

    private String reservationId;

    public ReservationResource() { }

    public ReservationResource(String reservationId) {
        this.reservationId = reservationId;
    }
}
