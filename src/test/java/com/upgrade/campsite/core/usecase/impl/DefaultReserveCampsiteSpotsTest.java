package com.upgrade.campsite.core.usecase.impl;

import com.upgrade.campsite.core.entity.*;
import com.upgrade.campsite.core.exception.NotFoundException;
import com.upgrade.campsite.core.exception.UseCaseException;
import com.upgrade.campsite.core.port.repository.*;
import com.upgrade.campsite.core.usecase.ReserveCampsiteSpots;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;

class DefaultReserveCampsiteSpotsTest {

    private DefaultReserveCampsiteSpots useCase;
    private CamperRepository camperRepository;
    private CampsiteRepository campsiteRepository;
    private SpotRepository spotRepository;
    private ReservationRepository reservationRepository;
    private ReservedSpotRepository reservedSpotRepository;

    @BeforeEach
    void setUp() {
        this.camperRepository = Mockito.mock(CamperRepository.class);
        this.campsiteRepository = Mockito.mock(CampsiteRepository.class);
        this.reservationRepository = Mockito.mock(ReservationRepository.class);
        this.spotRepository = Mockito.mock(SpotRepository.class);
        this.reservedSpotRepository = Mockito.mock(ReservedSpotRepository.class);

        this.useCase = Mockito.spy(
            new DefaultReserveCampsiteSpots(camperRepository, campsiteRepository, reservationRepository, spotRepository, reservedSpotRepository)
        );
    }

    @AfterEach
    void tearDown() {
    }

    /*
     * Tests for DefaultReserveCampsiteSpots#initialize
     */
    @Test
    void test_initialize_ok() {
        //given
        String campsiteId = "123ABC";
        String email = "test_1@gmail.com";
        String name = "Test Name";
        LocalDate arrivalDate = LocalDate.of(2019, 3,10);
        LocalDate departureDate = LocalDate.of(2019, 3,13);
        Integer requiredSpots = 1;

        //when
        assertDoesNotThrow(() -> {
            useCase.initialize(campsiteId, name, email, arrivalDate, departureDate, requiredSpots);
        });
    }

    @Test
    void test_initialize_dateRangeIsInvalid() {
        //given
        String campsiteId = "123ABC";
        String email = "test_1@gmail.com";
        String name = "Test Name";
        LocalDate arrivalDate = LocalDate.of(2019, 3,10);
        LocalDate departureDate = LocalDate.of(2019, 3,9);
        Integer requiredSpots = 1;

        //when
        UseCaseException ex = assertThrows(UseCaseException.class, () -> {
            useCase.initialize(campsiteId, name, email, arrivalDate, departureDate, requiredSpots);
        });
        assertEquals(MessageId.INVALID_DATE_RANGE.getId(), ex.getMessageId());
    }

    @Test
    void test_initialize_requiredSpotsIsInvalid() {
        //given
        String campsiteId = "123ABC";
        String email = "test_1@gmail.com";
        String name = "Test Name";
        LocalDate arrivalDate = LocalDate.of(2019, 3,10);
        LocalDate departureDate = LocalDate.of(2019, 3,13);
        Integer requiredSpots = -1;

        //when
        UseCaseException ex = assertThrows(UseCaseException.class, () -> {
            useCase.initialize(campsiteId, name, email, arrivalDate, departureDate, requiredSpots);
        });
        assertEquals(MessageId.INVALID_SPOTS_NUMBER.getId(), ex.getMessageId());
    }

    @Test
    void test_initialize_emailIsRequired() {
        //given
        String campsiteId = "123ABC";
        String email = null;
        String name = "Test Name";
        LocalDate arrivalDate = LocalDate.of(2019, 3,10);
        LocalDate departureDate = LocalDate.of(2019, 3,13);
        Integer requiredSpots = 1;

        //when
        UseCaseException ex = assertThrows(UseCaseException.class, () -> {
            useCase.initialize(campsiteId, name, email, arrivalDate, departureDate, requiredSpots);
        });
        assertEquals(MessageId.CAMPER_EMAIL_REQUIRED.getId(), ex.getMessageId());
    }

    @Test
    void test_initialize_nameIsRequired() {
        //given
        String campsiteId = "123ABC";
        String email = "test_1@gmail.com";
        String name = null;
        LocalDate arrivalDate = LocalDate.of(2019, 3,10);
        LocalDate departureDate = LocalDate.of(2019, 3,13);
        Integer requiredSpots = 1;

        //when
        UseCaseException ex = assertThrows(UseCaseException.class, () -> {
            useCase.initialize(campsiteId, name, email, arrivalDate, departureDate, requiredSpots);
        });
        assertEquals(MessageId.CAMPER_FULLNAME_REQUIRED.getId(), ex.getMessageId());
    }

    @Test
    void test_initialize_dateFromIsRequired() {
        //given
        String campsiteId = "123ABC";
        String email = "test_1@gmail.com";
        String name = "Test Name";
        LocalDate arrivalDate = null;
        LocalDate departureDate = LocalDate.of(2019, 3,13);
        Integer requiredSpots = 1;

        //when
        UseCaseException ex = assertThrows(UseCaseException.class, () -> {
            useCase.initialize(campsiteId, name, email, arrivalDate, departureDate, requiredSpots);
        });
        assertEquals(MessageId.ARRIVAL_DATE_REQUIRED.getId(), ex.getMessageId());
    }


    @Test
    void test_initialize_dateToIsRequired() {
        //given
        String campsiteId = "123ABC";
        String email = "test_1@gmail.com";
        String name = "Test Name";
        LocalDate arrivalDate = LocalDate.of(2019, 3,10);
        LocalDate departureDate = null;
        Integer requiredSpots = 1;

        //when
        UseCaseException ex = assertThrows(UseCaseException.class, () -> {
            useCase.initialize(campsiteId, name, email, arrivalDate, departureDate, requiredSpots);
        });
        assertEquals(MessageId.DEPARTURE_DATE_REQUIRED.getId(), ex.getMessageId());
    }


    /*
     * Tests for DefaultReserveCampsiteSpots#setDefaults
     */
    @Test
    void test_setDefaults_campsiteId() {
        //given
        String campsiteId = null;
        String email = "test_1@gmail.com";
        String name = "Test Name";
        LocalDate arrivalDate = LocalDate.of(2019, 3,10);
        LocalDate departureDate = LocalDate.of(2019, 3,13);
        Integer requiredSpots = 1;

        ReserveCampsiteSpots.Model model = new ReserveCampsiteSpots.Model(campsiteId, email, name, arrivalDate, departureDate, requiredSpots);

        //when
        useCase.setDefaults(model);

        //then
        assertEquals(Campsite.CAMPSITE_ID, model.getCampsiteId());
    }

    @Test
    void test_setDefaults_requiredSpots() {
        //given
        String campsiteId = "123ABC";
        String email = "test_1@gmail.com";
        String name = "Test Name";
        LocalDate arrivalDate = LocalDate.of(2019, 3,10);
        LocalDate departureDate = LocalDate.of(2019, 3,13);
        Integer requiredSpots = null;

        ReserveCampsiteSpots.Model model = new ReserveCampsiteSpots.Model(campsiteId, email, name, arrivalDate, departureDate, requiredSpots);

        //when
        useCase.setDefaults(model);

        //then
        assertEquals(1, model.getRequiredSpots().intValue());
    }

    /*
     * Tests for DefaultReserveCampsiteSpots#get
     */
    @Test
    void test_execute_ok() {
        //given
        String campsiteId = "123ABC";
        String email = "test_1@gmail.com";
        String name = "Test Name";
        LocalDate arrivalDate = LocalDate.of(2019, 3,10);
        LocalDate departureDate = LocalDate.of(2019, 3,13);
        Integer requiredSpots = 1;

        Reservation reservation = createReservation();
        var campsite = Mockito.mock(Campsite.class);
        doReturn(reservation).when(campsite).reserve(any(), any(), any(), any());

        doReturn(Optional.empty()).when(camperRepository).findOneByEmail(anyString());
        doReturn(Optional.of( campsite )).when(campsiteRepository).findOne(anyString());
        doReturn(Collections.<Spot>emptySet()).when(spotRepository).findForCampsite(any());
        doReturn(Collections.<Reservation>emptyList()).when(reservationRepository).findActiveReservations(any(), any());

        useCase.initialize(campsiteId, name, email, arrivalDate, departureDate, requiredSpots);

        //when
        Reservation result = useCase.get();

        //then
        assertNotNull(result);
        assertEquals(ReservationStatus.PENDING, result.getStatus());
    }

    @Test
    void test_execute_camperExists_ok() {
        //given
        String campsiteId = "123ABC";
        String email = "test_1@gmail.com";
        String name = "Test Name";
        LocalDate arrivalDate = LocalDate.of(2019, 3,10);
        LocalDate departureDate = LocalDate.of(2019, 3,13);
        Integer requiredSpots = 1;

        Reservation reservation = createReservation();
        var campsite = Mockito.mock(Campsite.class);
        doReturn(reservation).when(campsite).reserve(any(), any(), any(), any());

        var camper = Camper.builder().id(1L).email(email).fullname(name).build();
        doReturn(Optional.of(camper)).when(camperRepository).findOneByEmail(anyString());

        doReturn(Optional.of( campsite )).when(campsiteRepository).findOne(anyString());
        doReturn(Collections.<Spot>emptySet()).when(spotRepository).findForCampsite(any());
        doReturn(Collections.<Reservation>emptyList()).when(reservationRepository).findActiveReservations(any(), any());

        useCase.initialize(campsiteId, name, email, arrivalDate, departureDate, requiredSpots);

        //when
        Reservation result = useCase.get();

        //then
        assertNotNull(result);
        assertEquals(ReservationStatus.PENDING, result.getStatus());
    }

    @Test
    void test_execute_campsiteNotExists() {
        //given
        String campsiteId = "123ABC";
        String email = "test_1@gmail.com";
        String name = "Test Name";
        LocalDate arrivalDate = LocalDate.of(2019, 3,10);
        LocalDate departureDate = LocalDate.of(2019, 3,13);
        Integer requiredSpots = 1;

        doReturn(Optional.empty()).when(campsiteRepository).findOne(anyString());

        useCase.initialize(campsiteId, name, email, arrivalDate, departureDate, requiredSpots);

        //when
        NotFoundException ex = assertThrows(NotFoundException.class, () -> {
            useCase.get();
        });

        //then
        assertEquals(MessageId.CAMPSITE_NOT_FOUND.getId(), ex.getMessageId());
    }

    @Test
    void test_execute_campsiteReserveThrowsException() {
        //given
        String campsiteId = "123ABC";
        String email = "test_1@gmail.com";
        String name = "Test Name";
        LocalDate arrivalDate = LocalDate.of(2019, 3,10);
        LocalDate departureDate = LocalDate.of(2019, 3,13);
        Integer requiredSpots = 1;

        var campsite = Mockito.mock(Campsite.class);
        doThrow(UseCaseException.class).when(campsite).reserve(any(), any(), any(), any());

        doReturn(Optional.of( campsite )).when(campsiteRepository).findOne(anyString());
        doReturn(Optional.empty()).when(camperRepository).findOneByEmail(anyString());
        doReturn(Collections.<Spot>emptySet()).when(spotRepository).findForCampsite(any());
        doReturn(Collections.<Reservation>emptyList()).when(reservationRepository).findActiveReservations(any(), any());

        useCase.initialize(campsiteId, name, email, arrivalDate, departureDate, requiredSpots);

        //when
        assertThrows(UseCaseException.class, () -> {
            useCase.get();
        });
    }

    private Reservation createReservation(){
        return Reservation.builder()
            .id(1L)
            .spots( Arrays.asList(
                new Spot(1L),
                new Spot(2L))
            )
            .arrivalDate( LocalDate.of(2019, 2, 15) )
            .departureDate( LocalDate.of(2019, 2, 18) )
            .status(ReservationStatus.PENDING)
            .camper(Camper.builder().email("test_1@gmail.com").build())
            .build();
    }

}
