package com.upgrade.campsite.core.entity;

import com.upgrade.campsite.core.exception.UseCaseException;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static com.upgrade.campsite.core.entity.CampsiteAvailabilityStatus.*;

@Getter
@Setter
public class Campsite {

    public final static String CAMPSITE_ID = "5f108bfc-47d6-4a75-8a52-ce79790c73d6";
    private String id;
    private CampsiteStayingDateRestrictions stayingRestriction;
    private Set<Spot> spots = new LinkedHashSet<>();
    private List<Reservation> reservations = new ArrayList<>();

    /**
     * Create the new reservation
     * @param camper        Camper who ask for the reservation
     * @param arrivalDate   Requested arrival date
     * @param departureDate Request departure date
     * @param requiredSpots Number of spots required
     * @throws UseCaseException if any constraint is violated
     * @return reservation
     */
    public Reservation reserve(Camper camper, LocalDate arrivalDate, LocalDate departureDate, Integer requiredSpots){

        validateReservation(camper, arrivalDate, departureDate, requiredSpots);

        List<Spot> freeSpots = this.getFreeSpots(arrivalDate, departureDate).subList(0, requiredSpots);

        Reservation reservation = Reservation.builder()
            .spots(freeSpots)
            .camper(camper)
            .arrivalDate(arrivalDate)
            .departureDate(departureDate)
            .createdOn(Instant.now())
            .status(ReservationStatus.PENDING)
            .build();

        this.addReservation(reservation);

        return reservation;
    }

    /**
     * Validates if the camper can make the reservation with the requested parameters
     * @param camper        Camper who ask for the reservation
     * @param arrivalDate   Requested arrival date
     * @param departureDate Request departure date
     * @param requiredSpots Number of required spots
     * @throws UseCaseException if any constraint is violated
     */
    public void validateReservation(Camper camper, LocalDate arrivalDate, LocalDate departureDate, Integer requiredSpots){
        stayingRestriction.validateStayingDate(arrivalDate, departureDate);

        Integer campsiteTotalSpots = this.getSpots().size();
        if(requiredSpots > campsiteTotalSpots){
            throw new UseCaseException(MessageId.REQUIRED_SPOTS_GREATER_THAN_MAX_CAPACITY, requiredSpots, campsiteTotalSpots);
        }

        if(!canHarborTheRequiredSpots(arrivalDate, departureDate, requiredSpots)){
            throw new UseCaseException(FULL.getMessageId());
        }

        boolean camperHasAnActiveReservation = this.getReservations().stream().anyMatch(r -> r.isActiveAndOwnedBy(camper));
        if(camperHasAnActiveReservation){
            throw new UseCaseException(MessageId.CAMPER_HAS_ACTIVE_RESERVATION);
        }
    }

    /**
     * Get the campsite schedule for the given date range, number of required spots
     * and for a given amount of days for the required reservation.
     *
     * @return Campsite schedule
     * @throws UseCaseException if a campsite restriction is broken
     */
    public <SE extends ScheduleEvent> CampsiteAvailabilitySchedule<SE> getAvailabilitySchedule(LocalDate dateFrom,
                                                                               LocalDate dateTo,
                                                                               Integer requiredSpots,
                                                                               Integer amountDaysForTheReservation,
                                                                               ScheduleType type){
        Integer campsiteTotalSpots = this.getSpots().size();
        if(requiredSpots > campsiteTotalSpots){
            throw new UseCaseException(MessageId.REQUIRED_SPOTS_GREATER_THAN_MAX_CAPACITY, requiredSpots, campsiteTotalSpots);
        }

        if(stayingRestriction.isMaxReservationTimeExceed(amountDaysForTheReservation)){
            throw new UseCaseException(MessageId.MAX_RESERVATION_TIME_EXCEED, this.stayingRestriction.getMaximumReservationTime().toDays());
        }

        if(!stayingRestriction.isCampsiteOpenInRange(dateFrom, dateTo)){
            return new CampsiteAvailabilitySchedule<SE>().withStatus(CLOSED);
        }

        List<SE> availableSchedule = this.createScheduleHandler(dateFrom, dateTo, requiredSpots, amountDaysForTheReservation, type).execute();

        if(availableSchedule.isEmpty()){
            return new CampsiteAvailabilitySchedule<SE>().withStatus(FULL);
        }

        return new CampsiteAvailabilitySchedule<SE>()
            .withStatus(AVAILABLE)
            .withSchedule(availableSchedule);
    }

    public ScheduleEventsHandler createScheduleHandler(LocalDate dateFrom, LocalDate dateTo, Integer requiredSpots, Integer amountDaysForTheReservation, ScheduleType type){
        return new ScheduleEventsHandler(dateFrom, dateTo, requiredSpots, amountDaysForTheReservation, type).setCampsite(this);
    }

    /**
     * Get the campsite's free spots for the given date range
     * @return list of free spots
     */
    public List<Spot> getFreeSpots(LocalDate dateFrom, LocalDate dateTo){
        List<Spot> allSpots = new ArrayList<>(this.spots);
        List<Spot> reservedSpots = this.getReservedSpots(dateFrom, dateTo).stream().map(ReservedSpot::getSpot).collect(Collectors.toList());
        allSpots.removeAll(reservedSpots);
        return allSpots;
    }

    /**
     * Get the campsite's current reserved spots for the given date range
     */
    public List<ReservedSpot> getReservedSpots(LocalDate dateFrom, LocalDate dateTo){
        return this.reservations.stream()
            .filter(r -> r.collapseWithAnotherReservation(dateFrom, dateTo))
            .map(Reservation::getReservedSpots)
            .flatMap(Collection::stream)
            .collect(Collectors.toList());
    }

    /**
     * Add a reservation
     */
    public void addReservation(Reservation r){
        this.reservations.add(r);
    }

    /**
     * Remove a reservation
     */
    public void removeReservation(Reservation r){
        this.reservations.remove(r);
    }

    /**
     * Verify if the campsite can harbor the required spots.
     *
     * @param requiredSpots required spots
     * @return true if campsite can
     */
    public Boolean canHarborTheRequiredSpots(LocalDate dateFrom, LocalDate dateTo, Integer requiredSpots){
        return this.spots.size() >= (this.getReservedSpots(dateFrom, dateTo).size() + requiredSpots);
    }

}
