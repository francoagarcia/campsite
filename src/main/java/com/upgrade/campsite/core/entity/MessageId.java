package com.upgrade.campsite.core.entity;

public enum MessageId {

    INTERNAL_SERVER_ERROR("error.server", "Internal server error"),
    RESOURCE_NOT_FOUND("error.resource-not-found", "Resource not found"),
    UNPROCESSABLE_ENTITY_ERROR("error.unprocessable-entity", "There is an error with your request data"),
    UNPROCESSABLE_ENTITY_METHOD_ARG_ERROR("error.unprocessable-entity.method-argument", "There is an error with your request data"),

    RESERVATION_NOT_FOUND("error.not-found.reservation", "Reservation not found"),
    CAMPER_NOT_FOUND("error.not-found.camper", "Camper not found"),
    CAMPSITE_NOT_FOUND("error.not-found.campsite", "Campsite not found"),

    CAMPER_ID_REQUIRED("error.required.camper-id", "Camper ID required"),
    RESERVATION_ID_REQUIRED("error.required.reservation-id", "Reservation ID required"),
    CAMPER_EMAIL_REQUIRED("error.required.camper-email", "Camper email is required"),
    CAMPER_FULLNAME_REQUIRED("error.required.camper-fullname", "Camper full name is required"),
    ARRIVAL_DATE_REQUIRED("error.required.arrival-date", "Arrival date is required"),
    DEPARTURE_DATE_REQUIRED("error.required.departure-date", "Departure date is required"),

    INVALID_DATE_RANGE("error.invalid.date-range", "Invalid date range. Arrival date must be before than the departure date"),
    INVALID_ARRIVAL_DATE("error.invalid.arrival-date", "Arrival date must be after than now"),
    INVALID_DEPARTURE_DATE("error.invalid.departure-date", "Departure date must be after than now"),
    INVALID_SPOTS_NUMBER("error.invalid.spots-number", "The number of spots must be greater than zero"),
    INVALID_RESERVATION_DAYS("error.invalid.reservation-days", "The number of days for the required reservations must be greater than zero"),
    INVALID_UPDATE_RESERVATION("error.invalid.update-reservation", "Cannot modify an ended reservation"),

    CAMPSITE_FULL_CAPACITY("campsite.availability.full", "The Campsite is at full capacity"),
    CAMPSITE_CLOSED("campsite.availability.closed", "The Campsite is closed in the given date range"),
    CAMPSITE_AVAILABLE("campsite.availability.available", "The Campsite is available in the given date range"),

    MAX_RESERVATION_TIME_EXCEED("campsite.restrictions.reservation-time-exceed", "Maximum reservation time exceed"),
    REQUIRED_SPOTS_GREATER_THAN_MAX_CAPACITY("campsite.restrictions.required-spots-greater-than-max-capacity", "The required spots are more than the campsite max capacity"),
    MIN_PERIOD_BEFORE_RESERVATION_EXCEED("campsite.restrictions.min-period-before-reservation-exceed", "Minimum period before reservation exceed"),
    MAX_PERIOD_BEFORE_RESERVATION_EXCEED("campsite.restrictions.max-period-before-reservation-exceed", "Maximum period before reservation exceed"),
    CAMPER_HAS_ACTIVE_RESERVATION("campsite.restrictions.camper-has-reservation", "The camper already has an active reservation"),

    RESERVATION_IS_NOT_ACTIVE("cancellation.restrictions.reservation-not-active", "Cannot cancel the reservation because it's is not active")
    ;

    MessageId(String id, String defaultMessage) {
        this.id = id;
        this.defaultMessage = defaultMessage;
    }

    private String id;
    private String defaultMessage;

    public String getId() {
        return id;
    }

    public String getDefaultMessage() {
        return defaultMessage;
    }

    @Override
    public String toString() {
        return this.name().toLowerCase();
    }
}
