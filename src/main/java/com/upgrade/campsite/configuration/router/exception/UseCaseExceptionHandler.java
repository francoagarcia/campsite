package com.upgrade.campsite.configuration.router.exception;

import com.upgrade.campsite.core.exception.UseCaseException;
import com.upgrade.campsite.core.port.util.I18n;
import com.upgrade.campsite.entrypoint.resource.ErrorApi;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.inject.Inject;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;

@ControllerAdvice
@Order(HIGHEST_PRECEDENCE)
public class UseCaseExceptionHandler {

    private static final Logger logger = Logger.getLogger(UseCaseExceptionHandler.class.getName());
    private I18n i18n;

    @Inject
    public UseCaseExceptionHandler(I18n i18n) {
        this.i18n = i18n;
    }

    @ExceptionHandler(UseCaseException.class)
    public ResponseEntity<ErrorApi> handle(UseCaseException e, Locale locale) {
        Integer status = e.getStatusCode();

        String message = ObjectUtils.firstNonNull(i18n.tr(e.getMessageId(), e.getArgs(), locale), e.getMessage());

        String detail = String.format("[%s] - %s", e.getClass().getName(), e.getMessage());

        ErrorApi response = ErrorApi.builder()
            .message(message)
            .status(status)
            .detail(detail)
            .build();

        logger.log(Level.INFO, String.format("[%s] - %s", response.getTimestamp(), message));

        return ResponseEntity
            .status(HttpStatus.valueOf(status))
            .body(response);
    }
}
