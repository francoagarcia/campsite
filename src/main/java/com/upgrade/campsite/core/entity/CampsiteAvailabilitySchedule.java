package com.upgrade.campsite.core.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class CampsiteAvailabilitySchedule<EVENTENTITY extends ScheduleEvent> {

    private CampsiteAvailabilityStatus status;
    private List<EVENTENTITY> schedule = new ArrayList<>();


    public CampsiteAvailabilitySchedule<EVENTENTITY> withStatus(CampsiteAvailabilityStatus status){
        this.status = status;
        return this;
    }

    public CampsiteAvailabilitySchedule<EVENTENTITY> withSchedule(List<EVENTENTITY> schedule){
        this.schedule = schedule;
        return this;
    }
}
