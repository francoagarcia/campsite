package com.upgrade.campsite.entrypoint;

/**
 * Entry point for the CancelReservation use case.
 * Initialize the use case and return the presentation model.
 */
public interface CancelReservationEndpoint {

    /**
     * Execute the use case and return the presentation model.
     *
     * @param campsiteId campsite id
     * @param reservationId obfuscated reservation id
     */
    void execute(String campsiteId, String reservationId);

}
