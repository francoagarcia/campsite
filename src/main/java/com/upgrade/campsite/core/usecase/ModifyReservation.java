package com.upgrade.campsite.core.usecase;

import com.upgrade.campsite.core.entity.DateRange;
import com.upgrade.campsite.core.entity.Reservation;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.function.Supplier;

/**
 * Modify the reservation made using its ID.
 * The user can modify the arrival & departure date, the number of spots reserved and also the camper for who the reservation is.
 *
 * Throws UseCaseException if any constraint is violated.
 * Throws NotFoundException if any required entity is not found.
 */
public interface ModifyReservation extends Supplier<Reservation> {

    /**
     * Initialize the use case.
     *
     * @param campsiteId Campsite for which the reservation was made.
     * @param reservationId reservation ID
     * @param fullname camper full name
     * @param email camper email
     * @param arrivalDate arrival date
     * @param departureDate departure date
     * @param requiredSpots required spots
     * @return ModifyReservation instance initialized
     */
	default ModifyReservation initialize(String campsiteId, Long reservationId, String fullname, String email, LocalDate arrivalDate, LocalDate departureDate, Integer requiredSpots){
		this.setModel(new Model(campsiteId, reservationId, fullname, email, arrivalDate, departureDate, requiredSpots));
		return this;
	}

    /**
     * Set & validate the request model handled by the Use Case.
     *
     * @param model request model
     */
	void setModel(Model model);

    /**
     * Request model
     */
    @Getter
    @Setter
	class Model {
	    private String campsiteId;
	    private Long reservationId;
	    private String fullname;
	    private String email;
        private LocalDate arrivalDate;
        private LocalDate departureDate;
        private Integer requiredSpots;

        public Model(String campsiteId, Long reservationId, String fullname, String email, LocalDate arrivalDate, LocalDate departureDate, Integer requiredSpots) {
            this.campsiteId = campsiteId;
            this.fullname = fullname;
            this.email = email;
            this.arrivalDate = arrivalDate;
            this.reservationId = reservationId;
            this.departureDate = departureDate;
            this.requiredSpots = requiredSpots;
        }

        public DateRange getDateRange(){
            return DateRange.builder().from(arrivalDate).to(departureDate).build();
        }
    }
}
