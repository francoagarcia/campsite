
CREATE SCHEMA IF NOT EXISTS `campsitedb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

USE `campsitedb`;

DROP TABLE IF EXISTS `campsitedb`.`reserved_spot`;
DROP TABLE IF EXISTS `campsitedb`.`reservation`;
DROP TABLE IF EXISTS `campsitedb`.`spot`;
DROP TABLE IF EXISTS `campsitedb`.`campsite`;
DROP TABLE IF EXISTS `campsitedb`.`camper`;

CREATE TABLE `campsitedb`.`camper` (
    `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `email` VARCHAR(255) NOT NULL,
    `fullname` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;

CREATE TABLE `campsitedb`.`campsite` (
    `id` CHAR(36) NOT NULL,
    `initial_checkin_time` TIME NOT NULL,
    `limit_checkin_time` TIME NOT NULL,
    `maximum_period_before_reservation` BIGINT UNSIGNED NOT NULL COMMENT 'Nanoseconds time. I.E. 3 days in nanos is 259200000000000',
    `maximum_reservation_time` BIGINT UNSIGNED NOT NULL COMMENT 'Nanoseconds time. I.E. 1 day in nanos is 86400000000000',
    `minimum_period_before_reservation` BIGINT UNSIGNED NOT NULL COMMENT 'Nanoseconds time. I.E. 30 days in nanos is 2592000000000000',
    `opened_from` DATE NOT NULL,
    `opened_to` DATE NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;

CREATE TABLE `campsitedb`.`reservation` (
    `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `arrival_date` DATE NOT NULL,
    `created_on` TIMESTAMP  NOT NULL,
    `departure_date` DATE NOT NULL,
    `status` VARCHAR(255) NOT NULL,
    `camper_id` BIGINT UNSIGNED NOT NULL,
    `campsite_id` CHAR(36) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;

CREATE TABLE `campsitedb`.`reserved_spot` (
    `reservation_id` BIGINT UNSIGNED NOT NULL,
    `spot_id` BIGINT UNSIGNED NOT NULL,
    `check_in` TIMESTAMP ,
    `check_out` TIMESTAMP ,
    PRIMARY KEY (`reservation_id`, `spot_id`)
) ENGINE = InnoDB;

CREATE TABLE `campsitedb`.`spot` (
    `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `description` VARCHAR(255) NOT NULL,
    `campsite_id` CHAR(36),
    PRIMARY KEY (id)
) ENGINE = InnoDB;

# Foreign Keys
ALTER TABLE `campsitedb`.`reservation` ADD CONSTRAINT `fk_reservation_camper` FOREIGN KEY (`camper_id`) REFERENCES `campsitedb`.`camper`(`id`);

ALTER TABLE `campsitedb`.`reservation` ADD CONSTRAINT `fk_reservation_campsite` FOREIGN KEY (`campsite_id`) REFERENCES `campsitedb`.`campsite`(`id`);

ALTER TABLE `campsitedb`.`reserved_spot` ADD CONSTRAINT `fk_reserved_spot_reservation` FOREIGN KEY (`reservation_id`) REFERENCES `campsitedb`.`reservation`(`id`);

ALTER TABLE `campsitedb`.`reserved_spot` ADD CONSTRAINT `fk_reserved_spot_spot` FOREIGN KEY (`spot_id`) REFERENCES `campsitedb`.`spot`(`id`);

ALTER TABLE `campsitedb`.`spot` ADD CONSTRAINT `fk_spot_campsite` FOREIGN KEY (`campsite_id`) REFERENCES `campsitedb`.`campsite`(`id`);

# Indexes
ALTER TABLE `campsitedb`.`camper` ADD UNIQUE INDEX `camper_email_UNIQUE` (`email` ASC);

ALTER TABLE `campsitedb`.`reservation` ADD INDEX `reservation_status_arrival_departure` (`status`, `arrival_date`, `departure_date` ASC);

DROP PROCEDURE IF EXISTS `campsitedb`.`createSpots`;
DELIMITER //
CREATE PROCEDURE `campsitedb`.`createSpots`(IN numberOfSpots INT)
BEGIN
    SET @i = 1;

    WHILE (@i <= numberOfSpots) DO

    SET @desc = 'Spot #'+CAST(@i as CHAR(5));
    INSERT INTO `campsitedb`.`spot`(`id`, `description`, `campsite_id`)
    VALUES (@i, @desc, '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

    SET @i = @i+1;
    END WHILE;
END;
//
