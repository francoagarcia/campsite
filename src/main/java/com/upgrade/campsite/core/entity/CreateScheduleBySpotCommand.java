package com.upgrade.campsite.core.entity;

import com.upgrade.campsite.core.exception.UseCaseException;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings("unchecked")
public class CreateScheduleBySpotCommand implements ScheduleCommand {

    /**
     * Get the list of available schedule events grouped by spots.
     * If the result list is empty, means there are not available events for the given parameters.
     */
    public List<ScheduleEventBySpot> execute(ScheduleEventsHandler handler) throws UseCaseException {
        List<ScheduleEventBySpot> listEventsBySpot = this.initializeScheduleWithEventsForEverySpot(handler);

        List<ScheduleEventByRange> listEventsByRange = this.getEventsGroupedByRange(handler);

        for (ScheduleEventByRange eventByRange : listEventsByRange){
            Set<Spot> spotsInEventDateRange = eventByRange.getFreeSpots();

            for(Spot spot : spotsInEventDateRange){
                ScheduleEventBySpot eventBySpot = listEventsBySpot.stream()
                    .filter(anEventSpot -> anEventSpot.getSpot().equals(spot))
                    .findFirst()
                    .orElseThrow(()->new IllegalArgumentException("Cannot build the schedule By Spot with an Unknown spot"));

                eventBySpot.addDateRange(eventByRange.getFrom(), eventByRange.getTo());
            }
        }

        listEventsBySpot = this.cleanUnavailableSpots( listEventsBySpot.stream() );

        listEventsBySpot = this.compactDateRangesForEverySpot( listEventsBySpot );

        return listEventsBySpot;
    }

    /**
     * Creates a list of ScheduleEventBySpot for every campsite spot despite is available or not
     */
    List<ScheduleEventBySpot> initializeScheduleWithEventsForEverySpot(ScheduleEventsHandler handler){
        List<ScheduleEventBySpot> listScheduleBySpot = new ArrayList<>();
        handler.getCampsite().getSpots().forEach(spot -> listScheduleBySpot.add(new ScheduleEventBySpot(spot)));
        return listScheduleBySpot;
    }

    /**
     * Get the list of ScheduleEventByRange for the handler. The list grouped by spot will be built based on this list.
     */
    List<ScheduleEventByRange> getEventsGroupedByRange(ScheduleEventsHandler handler){
        return new CreateScheduleByRangeCommand().execute(handler);
    }

    /**
     * Take out of the results the spots whose are not available.
     */
    List<ScheduleEventBySpot> cleanUnavailableSpots(Stream<ScheduleEventBySpot> eventBySpotStream){
        return eventBySpotStream.filter(eventSpot -> !eventSpot.getDatesAvailable().isEmpty()).collect(Collectors.toList());
    }

    /**
     * Compact the date ranges for every spot if it's possible.
     *
     * Example: the date range ['2019-04-15', '2019-04-16'] and ['2019-04-16', '2019-04-18'] can be compacted
     * into a single date range of ['2019-04-15', '2019-04-18']
     */
    List<ScheduleEventBySpot> compactDateRangesForEverySpot(List<ScheduleEventBySpot> listScheduleBySpot){
        for (ScheduleEventBySpot scheduleEventBySpot : listScheduleBySpot) { //dates are already in order, not necessity to order them again
            scheduleEventBySpot.compactConsecutiveDateRanges();
        }
        return listScheduleBySpot;
    }

}
