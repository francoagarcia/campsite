package com.upgrade.campsite.core.entity;

import com.upgrade.campsite.entrypoint.resource.ScheduleEventByRangeResource;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
public class ScheduleEventByRange implements ScheduleEvent {

    private LocalDate from;
    private LocalDate to;
    private Set<Spot> freeSpots;

    public ScheduleEventByRange() { }
    public ScheduleEventByRange(LocalDate from, LocalDate to, Set<Spot> freeSpots) {
        this.from = from;
        this.to = to;
        this.freeSpots = freeSpots;
    }

    public boolean hasEnoughSpace(Integer desiredRequiredSpots){
        return getFreeSpots().size() >= desiredRequiredSpots;
    }

    @Override
    public ScheduleEventByRangeResource toResource() {
        return ScheduleEventByRangeResource.builder()
            .to(to)
            .from(from)
            .freeSpots(this.freeSpots.stream().map(Spot::getId).collect(Collectors.toSet()))
            .build();
    }
}
