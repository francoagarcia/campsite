package com.upgrade.campsite.entrypoint.impl;

import com.upgrade.campsite.core.port.util.HashingHelper;
import com.upgrade.campsite.core.usecase.CancelReservation;
import com.upgrade.campsite.entrypoint.CancelReservationEndpoint;

import javax.inject.Inject;

public class DefaultCancelReservationEndpoint implements CancelReservationEndpoint {

    private CancelReservation cancelReservation;
    private HashingHelper hashingHelper;

    @Inject
    public DefaultCancelReservationEndpoint(CancelReservation cancelReservation, HashingHelper hashingHelper) {
        this.cancelReservation = cancelReservation;
        this.hashingHelper = hashingHelper;
    }

    @Override
    public void execute(String campsiteId, String reservationId) {
        Long decodedId = this.hashingHelper.decode(reservationId);
        this.cancelReservation.initialize(campsiteId, decodedId).execute();
    }
}
