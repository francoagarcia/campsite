package com.upgrade.campsite.core.port.util;

public interface HashingHelper {

    /**
     * Obfuscate a given number ID.
     *
     * @param id number ID
     * @return the string obfuscated ID
     */
    String obfuscate(Long id);

    /**
     * Decode an obfuscated id and returns the number ID.
     *
     * @param obfuscatedId string ID
     * @return the number decoded ID
     */
    Long decode(String obfuscatedId);
}
