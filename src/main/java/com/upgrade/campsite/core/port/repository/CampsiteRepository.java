package com.upgrade.campsite.core.port.repository;

import com.upgrade.campsite.core.entity.Campsite;

import java.util.Optional;

public interface CampsiteRepository {

    /**
     * Find campsite by ID.
     *
     * @param id campsite ID
     * @return optional container for the Camper
     */
    Optional<Campsite> findOne(String id);
}
