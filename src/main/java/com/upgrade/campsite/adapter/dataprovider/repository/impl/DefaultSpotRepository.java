package com.upgrade.campsite.adapter.dataprovider.repository.impl;

import com.upgrade.campsite.adapter.dataprovider.entity.CampsiteDb_;
import com.upgrade.campsite.adapter.dataprovider.entity.SpotDb;
import com.upgrade.campsite.adapter.dataprovider.entity.SpotDb_;
import com.upgrade.campsite.core.entity.Campsite;
import com.upgrade.campsite.core.entity.Spot;
import com.upgrade.campsite.core.port.repository.SpotRepository;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Repository
public class DefaultSpotRepository extends SimpleJpaRepository<SpotDb, Long> implements SpotRepository {

    private EntityManager em;

	public DefaultSpotRepository(Class<SpotDb> domainClass, EntityManager em) {
		super(domainClass, em);
		this.em = em;
	}

    @Override
    public Set<Spot> findForCampsite(Campsite campsite) {
        CriteriaBuilder cb = this.em.getCriteriaBuilder();

        CriteriaQuery<SpotDb> cq = cb.createQuery(SpotDb.class);
        Root<SpotDb> spot = cq.from(SpotDb.class);

        cq.where(cb.equal(spot.get(SpotDb_.campsite).get(CampsiteDb_.id), campsite.getId()));

        TypedQuery<SpotDb> typedQuery = em.createQuery(cq);
        List<SpotDb> results = typedQuery.getResultList();

        return results.stream().map(SpotDb::toBusinessEntity).collect(Collectors.toSet());
    }
}
