package com.upgrade.campsite.adapter.dataprovider.entity;

import com.upgrade.campsite.core.entity.Reservation;
import com.upgrade.campsite.core.entity.ReservedSpot;
import com.upgrade.campsite.core.entity.Spot;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Getter
@Setter
@Table(name = "reserved_spot")
@IdClass(ReservedSpotDb.ReservedSpotId.class)
public class ReservedSpotDb {

    @Id
    @ManyToOne
    @JoinColumn(name = "reservation_id", referencedColumnName = "id")
    private ReservationDb reservation;

    @Id
    @ManyToOne
    @JoinColumn(name = "spot_id", referencedColumnName = "id")
    private SpotDb spot;
    private LocalDateTime checkIn;
    private LocalDateTime checkOut;

    public ReservedSpot toBusinessEntity(){
        ReservedSpot reservedSpot = new ReservedSpot();
        reservedSpot.setSpot(this.spot.toBusinessEntity());
        reservedSpot.setCheckIn(this.checkIn);
        reservedSpot.setCheckOut(this.checkOut);
        return reservedSpot;
    }

    public static class ReservedSpotId implements Serializable {

        private Long reservation;
        private Long spot;

        public ReservedSpotId() {}
        public ReservedSpotId(Reservation reservation, Spot spot) {
            this.reservation = reservation.getId();
            this.spot = spot.getId();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            ReservedSpotId that = (ReservedSpotId) o;
            return Objects.equals(reservation, that.reservation) &&
                Objects.equals(spot, that.spot);
        }

        @Override
        public int hashCode() {
            return Objects.hash(reservation, spot);
        }
    }
}
