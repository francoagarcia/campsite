package com.upgrade.campsite.core.entity;

import lombok.Getter;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Objects.requireNonNull;

@Getter
public class ScheduleEventsHandler {

    private LocalDate dateFrom;
    private LocalDate dateTo;
    private Integer requiredSpots;
    private Integer amountDaysForTheReservation;
    private ScheduleType type;
    private Campsite campsite;
    private Map<ScheduleType, ScheduleCommand> commands = new HashMap<>(){{
        put(ScheduleType.BY_RANGE, new CreateScheduleByRangeCommand());
        put(ScheduleType.BY_SPOT, new CreateScheduleBySpotCommand());
    }};

    public ScheduleEventsHandler() {}
    public ScheduleEventsHandler(LocalDate dateFrom, LocalDate dateTo, Integer requiredSpots, Integer amountDaysForTheReservation, ScheduleType type) {
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.requiredSpots = requiredSpots;
        this.amountDaysForTheReservation = amountDaysForTheReservation;
        this.type = type;
    }

    public ScheduleEventsHandler setCampsite(Campsite campsite) {
        this.campsite = campsite;
        return this;
    }

    public void setCommands(Map<ScheduleType, ScheduleCommand> commands) {
        this.commands = commands;
    }

    public <SE extends ScheduleEvent> List<SE> execute() {
        requireNonNull(this.campsite, "campsite has not been settled.");
        ScheduleCommand command = this.commands.get(this.type);
        if(command == null) {
            throw new UnsupportedOperationException("Schedule Type must be one of: " + Arrays.toString(ScheduleType.values()));
        }
        List<SE> availableSchedule = command.execute(this);
        return availableSchedule;
    }

}
