package com.upgrade.campsite.entrypoint;

import com.upgrade.campsite.entrypoint.resource.CamperResource;

/**
 * Entry point for the RetrieveCamperById use case.
 * Initialize the use case and return the presentation model.
 */
public interface RetrieveCamperByIdEndpoint {

    /**
     * Execute the use case and return the presentation model.
     *
     * @param camperId camper id
     * @return camper resource
     */
    CamperResource execute(Long camperId);

}
