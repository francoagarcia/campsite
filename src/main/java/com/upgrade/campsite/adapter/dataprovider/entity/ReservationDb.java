package com.upgrade.campsite.adapter.dataprovider.entity;

import com.upgrade.campsite.core.entity.Reservation;
import com.upgrade.campsite.core.entity.ReservationStatus;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

@Entity
@Getter
@Setter
@DynamicUpdate(value = false)
@Table(name = "reservation")
public class ReservationDb {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(optional = false)
    private CamperDb camper;

    @Column(nullable = false, updatable = false)
    private Instant createdOn;

    @Column(nullable = false)
    private LocalDate arrivalDate;

    @Column(nullable = false)
	private LocalDate departureDate;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private ReservationStatus status;

    @OneToMany(mappedBy = "reservation")
    private List<ReservedSpotDb> reservedSpots;

    @ManyToOne(optional = false)
    private CampsiteDb campsite;

    public ReservationDb() {
        super();
    }
    public ReservationDb(Long id){
        super();
        this.setId(id);
    }

    public Reservation toBusinessEntity(){
        Reservation reservation = Reservation.builder()
            .id(getId())
            .status(getStatus())
            .createdOn(getCreatedOn())
            .arrivalDate(getArrivalDate())
            .departureDate(getDepartureDate())
            .camper(camper.toBusinessEntity())
            .build();
        return reservation;
    }
}
