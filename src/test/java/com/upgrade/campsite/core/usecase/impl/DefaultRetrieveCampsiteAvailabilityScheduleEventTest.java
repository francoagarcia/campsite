package com.upgrade.campsite.core.usecase.impl;

import com.upgrade.campsite.core.entity.*;
import com.upgrade.campsite.core.exception.NotFoundException;
import com.upgrade.campsite.core.exception.UseCaseException;
import com.upgrade.campsite.core.port.repository.CampsiteRepository;
import com.upgrade.campsite.core.port.repository.ReservationRepository;
import com.upgrade.campsite.core.port.repository.SpotRepository;
import com.upgrade.campsite.core.usecase.RetrieveCampsiteAvailability;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;

class DefaultRetrieveCampsiteAvailabilityScheduleEventTest {

    private DefaultRetrieveCampsiteAvailability useCase;
    private CampsiteRepository campsiteRepository;
    private SpotRepository spotRepository;
    private ReservationRepository reservationRepository;

    @BeforeEach
    void setUp() {
        this.campsiteRepository = Mockito.mock(CampsiteRepository.class);
        this.spotRepository = Mockito.mock(SpotRepository.class);
        this.reservationRepository = Mockito.mock(ReservationRepository.class);

        this.useCase = Mockito.spy(
            new DefaultRetrieveCampsiteAvailability(campsiteRepository, spotRepository, reservationRepository)
        );
    }

    @AfterEach
    void tearDown() {
    }

    /*
     * Tests for DefaultRetrieveCampsiteAvailability#initialize
     */
    @Test
    void test_initialize_ok() {
        //given
        String campsiteId = "123ABC";
        LocalDate arrivalDate = LocalDate.of(2019, 3,10);
        LocalDate departureDate = LocalDate.of(2019, 3,13);
        Integer requiredSpots = 1;
        Integer reservationDays = 1;
        ScheduleType scheduleType = ScheduleType.BY_SPOT;

        //when
        assertDoesNotThrow(() -> {
            useCase.initialize(campsiteId, arrivalDate, departureDate, requiredSpots, reservationDays, scheduleType);
        });
    }

    @Test
    void test_initialize_dateRangeIsInvalid() {
        //given
        String campsiteId = "123ABC";
        LocalDate arrivalDate = LocalDate.of(2019, 3,10);
        LocalDate departureDate = LocalDate.of(2019, 3,9);
        Integer requiredSpots = 1;
        Integer reservationDays = 1;
        ScheduleType scheduleType = ScheduleType.BY_SPOT;

        //when
        UseCaseException ex = assertThrows(UseCaseException.class, () -> {
            useCase.initialize(campsiteId, arrivalDate, departureDate, requiredSpots, reservationDays, scheduleType);
        });
        assertEquals(MessageId.INVALID_DATE_RANGE.getId(), ex.getMessageId());
    }

    @Test
    void test_initialize_requiredSpotsIsInvalid() {
        //given
        String campsiteId = "123ABC";
        LocalDate arrivalDate = LocalDate.of(2019, 3,10);
        LocalDate departureDate = LocalDate.of(2019, 3,13);
        Integer requiredSpots = -1;
        Integer reservationDays = 1;
        ScheduleType scheduleType = ScheduleType.BY_SPOT;

        //when
        UseCaseException ex = assertThrows(UseCaseException.class, () -> {
            useCase.initialize(campsiteId, arrivalDate, departureDate, requiredSpots, reservationDays, scheduleType);
        });
        assertEquals(MessageId.INVALID_SPOTS_NUMBER.getId(), ex.getMessageId());
    }

    @Test
    void test_initialize_reservationDaysIsInvalid() {
        //given
        String campsiteId = "123ABC";
        LocalDate arrivalDate = LocalDate.of(2019, 3,10);
        LocalDate departureDate = LocalDate.of(2019, 3,13);
        Integer requiredSpots = 1;
        Integer reservationDays = -1;
        ScheduleType scheduleType = ScheduleType.BY_SPOT;

        //when
        UseCaseException ex = assertThrows(UseCaseException.class, () -> {
            useCase.initialize(campsiteId, arrivalDate, departureDate, requiredSpots, reservationDays, scheduleType);
        });
        assertEquals(MessageId.INVALID_RESERVATION_DAYS.getId(), ex.getMessageId());
    }

    /*
     * Tests for DefaultRetrieveCampsiteAvailability#setDefaults
     */
    @Test
    void test_setDefaults_campsiteId() {
        //given
        String campsiteId = null;
        LocalDate arrivalDate = LocalDate.of(2019, 3,10);
        LocalDate departureDate = LocalDate.of(2019, 3,13);
        Integer requiredSpots = 1;
        Integer reservationDays = 1;
        ScheduleType scheduleType = ScheduleType.BY_SPOT;

        RetrieveCampsiteAvailability.Model model = new RetrieveCampsiteAvailability.Model(campsiteId, arrivalDate, departureDate, requiredSpots, reservationDays, scheduleType);

        //when
        useCase.setDefaults(model);

        //then
        assertEquals(Campsite.CAMPSITE_ID, model.getCampsiteId());
    }

    @Test
    void test_setDefaults_requiredSpots() {
        //given
        String campsiteId = "123ABC";
        LocalDate arrivalDate = LocalDate.of(2019, 3,10);
        LocalDate departureDate = LocalDate.of(2019, 3,13);
        Integer requiredSpots = null;
        Integer reservationDays = 1;
        ScheduleType scheduleType = ScheduleType.BY_SPOT;

        RetrieveCampsiteAvailability.Model model = new RetrieveCampsiteAvailability.Model(campsiteId, arrivalDate, departureDate, requiredSpots, reservationDays, scheduleType);

        //when
        useCase.setDefaults(model);

        //then
        assertEquals(1, model.getRequiredSpots().intValue());
    }

    @Test
    void test_setDefaults_scheduleType() {
        //given
        String campsiteId = "123ABC";
        LocalDate arrivalDate = LocalDate.of(2019, 3,10);
        LocalDate departureDate = LocalDate.of(2019, 3,13);
        Integer requiredSpots = 1;
        Integer reservationDays = 1;
        ScheduleType scheduleType = null;

        RetrieveCampsiteAvailability.Model model = new RetrieveCampsiteAvailability.Model(campsiteId, arrivalDate, departureDate, requiredSpots, reservationDays, scheduleType);

        //when
        useCase.setDefaults(model);

        //then
        assertEquals(ScheduleType.BY_SPOT, model.getScheduleType());
    }

    @Test
    void test_setDefaults_reservationDays() {
        //given
        String campsiteId = "123ABC";
        LocalDate arrivalDate = LocalDate.of(2019, 3,10);
        LocalDate departureDate = LocalDate.of(2019, 3,13);
        Integer requiredSpots = 1;
        Integer reservationDays = null;
        ScheduleType scheduleType = ScheduleType.BY_SPOT;

        RetrieveCampsiteAvailability.Model model = new RetrieveCampsiteAvailability.Model(campsiteId, arrivalDate, departureDate, requiredSpots, reservationDays, scheduleType);

        //when
        useCase.setDefaults(model);

        //then
        assertEquals(1, model.getReservationDays().intValue());
    }

    @Test
    void test_setDefaults_emptyDateRange() {
        //given
        String campsiteId = "123ABC";
        LocalDate arrivalDate = null;
        LocalDate departureDate = null;
        Integer requiredSpots = 1;
        Integer reservationDays = 1;
        ScheduleType scheduleType = ScheduleType.BY_SPOT;

        RetrieveCampsiteAvailability.Model model = new RetrieveCampsiteAvailability.Model(campsiteId, arrivalDate, departureDate, requiredSpots, reservationDays, scheduleType);

        //when
        useCase.setDefaults(model);

        //then
        assertEquals(LocalDate.now(), model.getDateFrom());
        assertEquals(LocalDate.now().plusMonths(1), model.getDateTo());
    }

    @Test
    void test_setDefaults_dateFromIsEmpty() {
        //given
        String campsiteId = "123ABC";
        LocalDate arrivalDate = null;
        LocalDate departureDate = LocalDate.of(2019, 3,13);
        Integer requiredSpots = 1;
        Integer reservationDays = 1;
        ScheduleType scheduleType = ScheduleType.BY_SPOT;

        RetrieveCampsiteAvailability.Model model = new RetrieveCampsiteAvailability.Model(campsiteId, arrivalDate, departureDate, requiredSpots, reservationDays, scheduleType);

        //when
        useCase.setDefaults(model);

        //then
        assertEquals(departureDate, model.getDateTo());
        assertEquals(departureDate.minusMonths(1), model.getDateFrom());
    }

    @Test
    void test_setDefaults_dateToIsEmpty() {
        //given
        String campsiteId = "123ABC";
        LocalDate arrivalDate = LocalDate.of(2019, 3,10);
        LocalDate departureDate = null;
        Integer requiredSpots = 1;
        Integer reservationDays = 1;
        ScheduleType scheduleType = ScheduleType.BY_SPOT;

        RetrieveCampsiteAvailability.Model model = new RetrieveCampsiteAvailability.Model(campsiteId, arrivalDate, departureDate, requiredSpots, reservationDays, scheduleType);

        //when
        useCase.setDefaults(model);

        //then
        assertEquals(arrivalDate, model.getDateFrom());
        assertEquals(arrivalDate.plusMonths(1), model.getDateTo());
    }

    /*
     * Tests for DefaultCancelRepository#get
     */
    @ParameterizedTest
    @CsvSource({ "AVAILABLE", "FULL", "CLOSED"})
    void test_execute_ok(String statusStr) {
        //given
        String campsiteId = "123ABC";
        LocalDate arrivalDate = LocalDate.of(2019, 3,10);
        LocalDate departureDate = LocalDate.of(2019, 3,13);
        Integer requiredSpots = 1;
        Integer reservationDays = 1;
        ScheduleType scheduleType = ScheduleType.BY_SPOT;

        CampsiteAvailabilitySchedule campsiteAvailabilitySchedule = new CampsiteAvailabilitySchedule().withStatus(CampsiteAvailabilityStatus.valueOf(statusStr));
        var campsite = Mockito.mock(Campsite.class);
        doReturn(campsiteAvailabilitySchedule).when(campsite).getAvailabilitySchedule(any(), any(), any(), any(), any());

        doReturn(Optional.of( campsite )).when(campsiteRepository).findOne(anyString());
        doReturn(Collections.<Spot>emptySet()).when(spotRepository).findForCampsite(any());
        doReturn(Collections.<Reservation>emptyList()).when(reservationRepository).findActiveReservations(any(), any());

        useCase.initialize(campsiteId, arrivalDate, departureDate, requiredSpots, reservationDays, scheduleType);

        //when
        CampsiteAvailabilitySchedule result = useCase.get();

        //then
        assertNotNull(result);
        assertEquals(campsiteAvailabilitySchedule.getStatus(), result.getStatus());
    }

    @Test
    void test_execute_campsiteNotFound() {
        //given
        String campsiteId = "XXXXXXX";
        LocalDate arrivalDate = LocalDate.of(2019, 3,10);
        LocalDate departureDate = LocalDate.of(2019, 3,13);
        Integer requiredSpots = 1;
        Integer reservationDays = 1;
        ScheduleType scheduleType = ScheduleType.BY_SPOT;

        doReturn(Optional.empty()).when(campsiteRepository).findOne(anyString());

        useCase.initialize(campsiteId, arrivalDate, departureDate, requiredSpots, reservationDays, scheduleType);

        //when
        NotFoundException ex = assertThrows(NotFoundException.class, () -> {
            useCase.get();
        });

        //then
        assertEquals(MessageId.CAMPSITE_NOT_FOUND.getId(), ex.getMessageId());
    }

    @Test
    void test_execute_campsiteGetAvailabilityThrowsException() {
        //given
        String campsiteId = "XXXXXXX";
        LocalDate arrivalDate = LocalDate.of(2019, 3,10);
        LocalDate departureDate = LocalDate.of(2019, 3,13);
        Integer requiredSpots = 1;
        Integer reservationDays = 1;
        ScheduleType scheduleType = ScheduleType.BY_SPOT;

        var campsite = Mockito.mock(Campsite.class);
        doThrow(UseCaseException.class).when(campsite).getAvailabilitySchedule(any(), any(), any(), any(), any());

        doReturn(Optional.of( campsite )).when(campsiteRepository).findOne(anyString());
        doReturn(Collections.<Spot>emptySet()).when(spotRepository).findForCampsite(any());
        doReturn(Collections.<Reservation>emptyList()).when(reservationRepository).findActiveReservations(any(), any());

        useCase.initialize(campsiteId, arrivalDate, departureDate, requiredSpots, reservationDays, scheduleType);

        //when
        assertThrows(UseCaseException.class, () -> {
            useCase.get();
        });
    }

}
