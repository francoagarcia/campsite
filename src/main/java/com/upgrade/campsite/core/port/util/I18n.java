package com.upgrade.campsite.core.port.util;

import java.util.Locale;

public interface I18n {

    /**
     * Retrieve the message's key for a given locale.
     *
     * @param key message's key
     * @param locale request locale
     * @return message translated
     */
    String tr(String key, Locale locale);

    /**
     * Retrieve the message's key for a given locale.
     *
     * @param key message's key
     * @param args array of arguments for the mssage
     * @param locale request locale
     * @return message translated
     */
    String tr(String key, Object[] args, Locale locale);

}
