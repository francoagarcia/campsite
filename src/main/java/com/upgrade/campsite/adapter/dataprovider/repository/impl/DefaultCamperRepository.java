package com.upgrade.campsite.adapter.dataprovider.repository.impl;

import com.upgrade.campsite.adapter.dataprovider.entity.CamperDb;
import com.upgrade.campsite.adapter.dataprovider.entity.CamperDb_;
import com.upgrade.campsite.core.entity.Camper;
import com.upgrade.campsite.core.port.repository.CamperRepository;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.Optional;

@Repository
public class DefaultCamperRepository extends SimpleJpaRepository<CamperDb, Long> implements CamperRepository {

	public DefaultCamperRepository(Class<CamperDb> domainClass, EntityManager em) {
		super(domainClass, em);
	}

	@Override
	public Optional<Camper> findOne(Long id) {
        Optional<CamperDb> camperDb = super.findById(id);
        return camperDb.map(CamperDb::toBusinessEntity);
    }

    @Override
    public Optional<Camper> findOneByEmail(String email) {
        Specification<CamperDb> findByEmailSpecification = (root, query, cb) -> cb.equal(root.get(CamperDb_.email), email);
        Optional<CamperDb> camperDb = super.findOne( findByEmailSpecification );
        return camperDb.map(CamperDb::toBusinessEntity);
    }

    @Override
    public Camper createOrUpdate(Camper camper) {
        CamperDb camperDb = fromBusinessEntity(camper);

        super.saveAndFlush(camperDb);

        camper.setId(camperDb.getId());

        return camper;
    }

    private static CamperDb fromBusinessEntity(Camper camper){
	    CamperDb camperDb = new CamperDb();
	    camperDb.setId(camper.getId());
	    camperDb.setEmail(camper.getEmail());
	    camperDb.setFullname(camper.getFullname());
	    return camperDb;
    }
}
