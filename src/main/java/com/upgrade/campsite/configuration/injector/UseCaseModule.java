package com.upgrade.campsite.configuration.injector;

import com.upgrade.campsite.core.entity.ScheduleEventByRange;
import com.upgrade.campsite.core.entity.ScheduleEventBySpot;
import com.upgrade.campsite.core.port.repository.*;
import com.upgrade.campsite.core.usecase.*;
import com.upgrade.campsite.core.usecase.impl.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import static org.springframework.beans.factory.config.BeanDefinition.SCOPE_PROTOTYPE;

@Configuration
public class UseCaseModule {

	@Bean
	@Scope(SCOPE_PROTOTYPE)
	public RetrieveCamperById retrieveCamperById(CamperRepository camperRepository) {
		return new DefaultRetrieveCamperById(camperRepository);
	}

    @Bean
    @Scope(SCOPE_PROTOTYPE)
    public RetrieveCampsiteAvailability<ScheduleEventByRange> retrieveCampsiteIsAvailableByRange(CampsiteRepository campsiteRepository,
                                                                                                 SpotRepository spotRepository,
                                                                                                 ReservationRepository reservationRepository) {
        return new DefaultRetrieveCampsiteAvailability<>(campsiteRepository, spotRepository, reservationRepository);
    }

    @Bean
    @Scope(SCOPE_PROTOTYPE)
    public RetrieveCampsiteAvailability<ScheduleEventBySpot> retrieveCampsiteIsAvailableBySpot(CampsiteRepository campsiteRepository,
                                                                                               SpotRepository spotRepository,
                                                                                               ReservationRepository reservationRepository) {
        return new DefaultRetrieveCampsiteAvailability<>(campsiteRepository, spotRepository, reservationRepository);
    }

    @Bean
    @Scope(SCOPE_PROTOTYPE)
    public ReserveCampsiteSpots reserveCampsiteSpots(CamperRepository camperRepository,
                                                     CampsiteRepository campsiteRepository,
                                                     ReservationRepository reservationRepository,
                                                     SpotRepository spotRepository,
                                                     ReservedSpotRepository reservedSpotRepository) {
        return new DefaultReserveCampsiteSpots(camperRepository, campsiteRepository, reservationRepository, spotRepository, reservedSpotRepository);
    }

    @Bean
    @Scope(SCOPE_PROTOTYPE)
    public CancelReservation cancelReservation(ReservationRepository reservationRepository) {
        return new DefaultCancelReservation(reservationRepository);
    }

    @Bean
    @Scope(SCOPE_PROTOTYPE)
    public ModifyReservation modifyReservation(CamperRepository camperRepository,
                                               CampsiteRepository campsiteRepository,
                                               ReservationRepository reservationRepository,
                                               SpotRepository spotRepository,
                                               ReservedSpotRepository reservedSpotRepository) {
        return new DefaultModifyReservation(camperRepository, campsiteRepository, reservationRepository, spotRepository, reservedSpotRepository);
    }
}
