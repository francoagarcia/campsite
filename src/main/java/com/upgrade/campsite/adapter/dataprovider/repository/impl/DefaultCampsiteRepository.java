package com.upgrade.campsite.adapter.dataprovider.repository.impl;

import com.upgrade.campsite.adapter.dataprovider.entity.CampsiteDb;
import com.upgrade.campsite.core.entity.Campsite;
import com.upgrade.campsite.core.port.repository.CampsiteRepository;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.Optional;

@Repository
public class DefaultCampsiteRepository extends SimpleJpaRepository<CampsiteDb, String> implements CampsiteRepository {

	public DefaultCampsiteRepository(Class<CampsiteDb> domainClass, EntityManager em) {
		super(domainClass, em);
	}

	@Override
	public Optional<Campsite> findOne(String id) {
        Optional<CampsiteDb> campsiteDb = super.findById(id);
        return campsiteDb.map(CampsiteDb::toBusinessEntity);
	}
}
