package com.upgrade.campsite.core.usecase.impl;

import com.upgrade.campsite.core.entity.Camper;
import com.upgrade.campsite.core.entity.Campsite;
import com.upgrade.campsite.core.entity.Reservation;
import com.upgrade.campsite.core.exception.NotFoundException;
import com.upgrade.campsite.core.exception.UseCaseException;
import com.upgrade.campsite.core.port.repository.*;
import com.upgrade.campsite.core.usecase.ModifyReservation;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.validation.constraints.Email;
import java.time.LocalDate;

import static com.upgrade.campsite.core.entity.MessageId.*;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

public class DefaultModifyReservation implements ModifyReservation {

	private Model model;
	private CamperRepository camperRepository;
	private CampsiteRepository campsiteRepository;
	private ReservationRepository reservationRepository;
	private SpotRepository spotRepository;
    private ReservedSpotRepository reservedSpotRepository;

	@Inject
	public DefaultModifyReservation(CamperRepository camperRepository,
                                    CampsiteRepository campsiteRepository,
                                    ReservationRepository reservationRepository,
                                    SpotRepository spotRepository,
                                    ReservedSpotRepository reservedSpotRepository){
		this.camperRepository = camperRepository;
		this.campsiteRepository = campsiteRepository;
		this.reservationRepository = reservationRepository;
		this.spotRepository = spotRepository;
		this.reservedSpotRepository = reservedSpotRepository;
	}

	@Override
	public Reservation get() {
        // Load data
        Reservation reservation = this.reservationRepository
            .findOne(this.model.getReservationId())
            .map( reservationVal -> {
                if(!reservationVal.isActive()){
                    throw new UseCaseException(INVALID_UPDATE_RESERVATION);
                }
                return reservationVal;
            })
            .orElseThrow(NotFoundException.reservation());

        Campsite campsite = this.campsiteRepository
            .findOne(this.model.getCampsiteId())
            .orElseThrow(NotFoundException.campsite());
        campsite.setReservations( this.reservationRepository.findActiveReservations(campsite) );
        campsite.getReservations().forEach( r -> r.setReservedSpots(this.reservedSpotRepository.findAllByReservation(r)) );
        campsite.setSpots( this.spotRepository.findForCampsite(campsite) );
        reservation.setReservedSpots( this.reservedSpotRepository.findAllByReservation(reservation) );

        // Analyzing which fields we should update
        Camper camper = reservation.getCamper();
        if(isNotBlank(this.model.getEmail())){
            camper = this.camperRepository
                .findOneByEmail(this.model.getEmail())
                .orElseGet( () -> Camper.builder().email(this.model.getEmail()).build());

            // I will take the previous full name as default.
            camper.setFullname(reservation.getCamper().getFullname());
        }

        // If a new name is provided, set it to the camper
        if(isNotBlank(this.model.getFullname())){
            camper.setFullname( this.model.getFullname() );
        }

        LocalDate arrivalDate = reservation.getArrivalDate();
        if(this.model.getArrivalDate() != null){
            arrivalDate = this.model.getArrivalDate();
        }

        LocalDate departureDate = reservation.getDepartureDate();
        if(this.model.getDepartureDate() != null){
            departureDate = this.model.getDepartureDate();
        }

        Integer requiredSpots = reservation.getReservedSpots().size();
        if(this.model.getRequiredSpots() != null){
            requiredSpots = this.model.getRequiredSpots();
        }

        // Simulate we are making a new reservation. Remove it from campsite, create a new one and then set its Id
        campsite.removeReservation(reservation);
        Reservation newReservation = campsite.reserve(camper, arrivalDate, departureDate, requiredSpots);
        newReservation.setId(reservation.getId());

        // Persist
        this.camperRepository.createOrUpdate(camper);
        this.reservationRepository.update(newReservation);
        this.reservedSpotRepository.updateReservedSpots(newReservation.getReservedSpots(), newReservation);

        // Response
	    return newReservation;
	}

	@Override
	public void setModel(Model model) {
	    this.setDefaults(model);
		this.validateModel(model);
		this.model = model;
	}

    /**
     * Set defaults for the request model
     * @param model
     */
	public void setDefaults(Model model) {
	    if(model.getCampsiteId() == null){
	        model.setCampsiteId(Campsite.CAMPSITE_ID);
        }
    }

    /**
     * Validates RequestModel and throw an UseCaseException if necessary
     * @param model requestModel
     */
	public void validateModel(Model model){
	    if(model.getReservationId() == null){
	        throw new UseCaseException(RESERVATION_ID_REQUIRED);
        }
        validateDates(model);
        validaterequiredSpots(model);
	}

	private void validaterequiredSpots(Model model){
        if(model.getRequiredSpots() != null && model.getRequiredSpots() <= 0){
            throw new UseCaseException(INVALID_SPOTS_NUMBER);
        }
    }

	private void validateDates(Model model){
        if(model.getArrivalDate() != null && model.getDepartureDate() != null && !model.getArrivalDate().isBefore(model.getDepartureDate())){
            throw new UseCaseException(INVALID_DATE_RANGE);
        }
    }

}
