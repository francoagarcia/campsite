package com.upgrade.campsite.core.entity;

import com.upgrade.campsite.core.exception.UseCaseException;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Getter
@Setter
@Builder
public class Reservation {

    private Long id;
    private Instant createdOn;
    private Camper camper;
	private LocalDate arrivalDate;
	private LocalDate departureDate;
	private ReservationStatus status;
	private List<ReservedSpot> reservedSpots;

    /**
     * Get the Reservation booking number
     */
	public Long getBookingNumber(){
	    return this.id;
    }

    /**
     * Check if the reservation is still active.
     * A reservation is considered "active" if his status is neither Finalized or Cancelled.
     *
     * @return true if it's active
     */
    public Boolean isActive(){
        return !this.getStatus().equals(ReservationStatus.FINALIZED) && !this.getStatus().equals(ReservationStatus.CANCELLED);
    }

    /**
     * Check if the reservation is owned by the given camper.
     *
     * @param camper camper
     * @return true if is owned by the given camper
     */
    public Boolean isOwnedBy(Camper camper){
        return this.camper.equals(camper);
    }

    /**
     * Check if the reservation is active and owned by the given camper.
     *
     * @see Reservation#isActive()
     * @see Reservation#isOwnedBy(Camper)
     * @param camper camper
     * @return true if is active and owned by the given camper
     */
	public Boolean isActiveAndOwnedBy(Camper camper){
        return this.isActive() && this.isOwnedBy(camper);
    }

    /**
     * Change the reservation status to Cancelled
     * @throws UseCaseException if the reservation is not active
     */
    public void cancel() {
        if(!this.isActive()){
            throw new UseCaseException(MessageId.RESERVATION_IS_NOT_ACTIVE);
        }
        this.setStatus( ReservationStatus.CANCELLED );
    }

    /**
     * Check is the given date range collapse with the Reservation date range.
     * @param dateFrom tentative arrival date
     * @param dateTo tentative departure date
     * @return
     */
    public Boolean collapseWithAnotherReservation(LocalDate dateFrom, LocalDate dateTo){
        return this.isActive() && (this.isDateRangeInsideReservation(dateFrom, dateTo) || this.dateRangeContainsReservation(dateFrom, dateTo));
    }

    private Boolean isDateRangeInsideReservation(LocalDate dateFrom, LocalDate dateTo){
        boolean includeRange = isAfterOrEqualsThan(dateFrom, this.arrivalDate) && isBeforeOrEqualsThan(dateTo, this.departureDate);
        boolean dateFromIsIncluded = isAfterOrEqualsThan(dateFrom, this.arrivalDate) && isBeforeOrEqualsThan(dateFrom, this.departureDate);
        boolean dateToIsIncluded = isBeforeOrEqualsThan(dateTo, this.departureDate) && isAfterOrEqualsThan(dateTo, this.arrivalDate);
        return includeRange || dateFromIsIncluded || dateToIsIncluded;
    }

    private Boolean dateRangeContainsReservation(LocalDate dateFrom, LocalDate dateTo){
	    return isBeforeOrEqualsThan(dateFrom, this.arrivalDate) && isAfterOrEqualsThan(dateTo, this.departureDate);
    }

    private Boolean isBeforeOrEqualsThan(LocalDate date, LocalDate anotherDate){
	    return date.isBefore(anotherDate) || date.isEqual(anotherDate);
    }

    private Boolean isAfterOrEqualsThan(LocalDate date, LocalDate anotherDate){
        return date.isAfter(anotherDate) || date.isEqual(anotherDate);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reservation that = (Reservation) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    /**
     * Customizing the lombok builder
     */
    public static class ReservationBuilder {
        public ReservationBuilder spots(List<Spot> freeSpots){
            this.reservedSpots = freeSpots.stream().map(ReservedSpot::new).collect(Collectors.toList());
            return this;
        }
    }
}
