package com.upgrade.campsite.adapter.dataprovider.entity;

import com.upgrade.campsite.core.entity.Campsite;
import com.upgrade.campsite.core.entity.CampsiteStayingDateRestrictions;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "campsite")
public class CampsiteDb extends BaseEntityDb {

    public final static String CAMPSITE_ID = "5f108bfc-47d6-4a75-8a52-ce79790c73d6";

    @Column(nullable = false)
    private Duration maximumReservationTime;

    @Column(nullable = false)
    private Duration minimumPeriodBeforeReservation;

    @Column(nullable = false)
    private Duration maximumPeriodBeforeReservation;

    @Column(nullable = false)
    private LocalDate openedFrom;

    @Column(nullable = false)
    private LocalDate openedTo;

    @Column(nullable = false)
    private LocalTime initialCheckinTime;

    @Column(nullable = false)
    private LocalTime limitCheckinTime;

    @OneToMany(mappedBy = "campsite")
    private Set<SpotDb> spots = new LinkedHashSet<>();

    public CampsiteDb() { }
    public CampsiteDb(String id){
        this.setId(id);
    }

    public Campsite toBusinessEntity(){
        Campsite campsite = new Campsite();
        campsite.setId(getId());
        campsite.setStayingRestriction(
            CampsiteStayingDateRestrictions.builder()
                .initialCheckinTime(getInitialCheckinTime())
                .limitCheckinTime(getLimitCheckinTime())
                .openedFrom(getOpenedFrom())
                .openedTo(getOpenedTo())
                .maximumPeriodBeforeReservation(getMaximumPeriodBeforeReservation())
                .minimumPeriodBeforeReservation(getMinimumPeriodBeforeReservation())
                .maximumReservationTime(getMaximumReservationTime())
                .build()
        );
        return campsite;
    }
}
