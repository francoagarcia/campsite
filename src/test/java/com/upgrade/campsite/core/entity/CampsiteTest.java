package com.upgrade.campsite.core.entity;

import com.upgrade.campsite.core.exception.UseCaseException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mockito;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class CampsiteTest {

    private Campsite campsite;
    private CampsiteStayingDateRestrictions stayingRestrictions;

    @BeforeEach
    void setUp() {
        this.campsite = Mockito.spy(Campsite.class);
        this.stayingRestrictions =  Mockito.spy(
            CampsiteStayingDateRestrictions.builder()
                .maximumReservationTime(Duration.ofDays(3))
                .openedFrom(LocalDate.of(2019, 2, 1))
                .openedTo(LocalDate.of(2019, 3, 31))
                .initialCheckinTime(LocalTime.of(11, 0))
                .limitCheckinTime(LocalTime.of(11, 0))
                .maximumPeriodBeforeReservation(Duration.ofDays(30))
                .minimumPeriodBeforeReservation(Duration.ofDays(1))
                .build()
        );
        this.campsite.setStayingRestriction( stayingRestrictions );

        campsite.setSpots( createSpots() );
        campsite.setReservations( createReservations() );
    }

    private List<Reservation> createReservations() {
        return new ArrayList<>(Arrays.asList(
            Reservation.builder()
                .id(1L)
                .spots( Arrays.asList(
                    new Spot(1L),
                    new Spot(2L))
                )
                .arrivalDate( LocalDate.of(2019, 2, 15) )
                .departureDate( LocalDate.of(2019, 2, 18) )
                .status(ReservationStatus.PENDING)
                .camper(Camper.builder().email("test_1@gmail.com").build())
                .build(),
            Reservation.builder()
                .id(2L)
                .spots( Arrays.asList(
                    new Spot(3L),
                    new Spot(4L))
                )
                .arrivalDate( LocalDate.of(2019, 2, 15) )
                .departureDate( LocalDate.of(2019, 2, 18) )
                .status(ReservationStatus.PENDING)
                .camper(Camper.builder().email("test_2@gmail.com").build())
                .build()
        ));
    }

    private Set<Spot> createSpots(){
        return new HashSet<>( Arrays.asList(
            new Spot(1L),
            new Spot(2L),
            new Spot(3L),
            new Spot(4L),
            new Spot(5L)
        ));
    }

    @AfterEach
    void tearDown() {
    }

    /*
     * Tests for Campsite#getFreeSpots
     */
    @Test
    void test_getFreeSpots_whenHasNoMoreSpace() {
        //given
        LocalDate dateFrom = LocalDate.parse("2019-02-16");
        LocalDate dateTo = LocalDate.parse("2019-02-17");

        doReturn(
            Arrays.asList(
                new ReservedSpot(new Spot(1L)),
                new ReservedSpot(new Spot(2L)),
                new ReservedSpot(new Spot(3L)),
                new ReservedSpot(new Spot(4L)),
                new ReservedSpot(new Spot(5L))
            )
        ).when(campsite).getReservedSpots(any(), any());

        //when
        List<Spot> freeSpots = campsite.getFreeSpots(dateFrom, dateTo);

        //then
        assertNotNull( freeSpots );
        assertEquals( 0, freeSpots.size() );
    }

    @Test
    void test_getFreeSpots_whenHasOneAvailableSpot() {
        //given
        LocalDate dateFrom = LocalDate.parse("2019-02-16");
        LocalDate dateTo = LocalDate.parse("2019-02-17");

        doReturn(
            Arrays.asList(
                new ReservedSpot(new Spot(1L)),
                new ReservedSpot(new Spot(2L)),
                new ReservedSpot(new Spot(3L)),
                new ReservedSpot(new Spot(4L))
            )
        )
            .when(campsite).getReservedSpots(any(), any());

        //when
        List<Spot> freeSpots = campsite.getFreeSpots(dateFrom, dateTo);

        //then
        assertNotNull( freeSpots );
        assertEquals( 1, freeSpots.size() );
    }

    /*
     * Tests for Campsite#getReservedSpots
     */
    @ParameterizedTest
    @CsvSource({
        "2019-02-16, 2019-02-17, 4",
        "2019-02-16, 2019-02-20, 4",
        "2019-02-14, 2019-02-17, 4",
        "2019-02-16, 2019-02-18, 4",
        "2019-02-15, 2019-02-19, 4",
        "2019-02-10, 2019-02-13, 0",
    })
    void test_getReservedSpots(String dateFromStr, String dateToStr, int resultsExpected) {
        //given
        LocalDate dateFrom = LocalDate.parse(dateFromStr);
        LocalDate dateTo = LocalDate.parse(dateToStr);

        //when
        List<ReservedSpot> reservedSpots = campsite.getReservedSpots(dateFrom, dateTo);

        //then
        assertNotNull( reservedSpots );
        assertEquals( resultsExpected, reservedSpots.size() );
    }

    /*
     * Tests for Campsite#canHarborTheRequiredSpots
     */
    @ParameterizedTest
    @CsvSource({ "5, 1", "4, 4", "1, 5" })
    void test_canHarborTheRequiredSpots_when_hasNotEnoughSpace(Integer quantityReservedSpots, Integer requiredSpots) {
        //given
        LocalDate dateFrom = LocalDate.parse("2019-02-16");
        LocalDate dateTo = LocalDate.parse("2019-02-17");

        var reservedSpots = new ArrayList<ReservedSpot>();
        IntStream.range(0,quantityReservedSpots).forEach(i ->reservedSpots.add(new ReservedSpot()));
        doReturn(reservedSpots)
            .when(campsite).getReservedSpots(any(), any());

        //when
        Boolean results = campsite.canHarborTheRequiredSpots(dateFrom, dateTo, requiredSpots);

        //then
        assertNotNull( results );
        assertEquals( false, results );
    }

    @Test
    void test_canHarborTheRequiredSpots_when_hasSpace() {
        //given
        LocalDate dateFrom = LocalDate.parse("2019-02-16");
        LocalDate dateTo = LocalDate.parse("2019-02-17");
        Integer requiredSpots = 1;

        var reservedSpots = new ArrayList<ReservedSpot>();
        IntStream.range(1,4).forEach(i ->reservedSpots.add(new ReservedSpot()));
        doReturn(reservedSpots)
            .when(campsite).getReservedSpots(any(), any());

        //when
        Boolean results = campsite.canHarborTheRequiredSpots(dateFrom, dateTo, requiredSpots);

        //then
        assertNotNull( results );
        assertEquals( true, results );
    }

    /*
     * Tests for Campsite#getAvailabilitySchedule
     */
    @Test
    void test_getAvailabilitySchedule_bySpot_ok() {
        //given
        LocalDate dateFrom = LocalDate.of(2019, 2, 10);
        LocalDate dateTo = LocalDate.of(2019, 3, 10);
        Integer requiredSpots = 1;
        Integer amounDaysForTheReservation = 1;
        ScheduleType scheduleType = ScheduleType.BY_SPOT;

        doReturn(Boolean.FALSE).when(stayingRestrictions).isMaxReservationTimeExceed(any());
        doReturn(Boolean.TRUE).when(stayingRestrictions).isCampsiteOpenInRange(any(), any());

        ScheduleEventsHandler scheduleEventsHandler = spy(ScheduleEventsHandler.class);
        doReturn(Arrays.asList(new ScheduleEventBySpot(), new ScheduleEventBySpot())).when(scheduleEventsHandler).execute();
        doReturn(scheduleEventsHandler).when(campsite).createScheduleHandler(any(), any(), any(), any(), any());

        //when
        CampsiteAvailabilitySchedule<ScheduleEventBySpot> schedule = campsite.getAvailabilitySchedule(dateFrom, dateTo, requiredSpots, amounDaysForTheReservation, scheduleType);

        //then
        assertNotNull(schedule);
        assertEquals(CampsiteAvailabilityStatus.AVAILABLE, schedule.getStatus());
        assertEquals(2, schedule.getSchedule().size());
    }

    @Test
    void test_getAvailabilitySchedule_byRange_ok() {
        //given
        LocalDate dateFrom = LocalDate.of(2019, 2, 10);
        LocalDate dateTo = LocalDate.of(2019, 3, 10);
        Integer requiredSpots = 1;
        Integer amounDaysForTheReservation = 1;
        ScheduleType scheduleType = ScheduleType.BY_RANGE;

        doReturn(Boolean.FALSE).when(stayingRestrictions).isMaxReservationTimeExceed(any());
        doReturn(Boolean.TRUE).when(stayingRestrictions).isCampsiteOpenInRange(any(), any());

        ScheduleEventsHandler scheduleEventsHandler = spy(ScheduleEventsHandler.class);
        doReturn(Arrays.asList(new ScheduleEventByRange(), new ScheduleEventByRange())).when(scheduleEventsHandler).execute();
        doReturn(scheduleEventsHandler).when(campsite).createScheduleHandler(any(), any(), any(), any(), any());

        //when
        CampsiteAvailabilitySchedule<ScheduleEventBySpot> schedule = campsite.getAvailabilitySchedule(dateFrom, dateTo, requiredSpots, amounDaysForTheReservation, scheduleType);

        //then
        assertNotNull(schedule);
        assertEquals(CampsiteAvailabilityStatus.AVAILABLE, schedule.getStatus());
        assertEquals(2, schedule.getSchedule().size());
    }

    @Test
    void test_getAvailabilitySchedule_invalidRequiredSpots() {
        //given
        LocalDate dateFrom = LocalDate.of(2019, 2, 10);
        LocalDate dateTo = LocalDate.of(2019, 3, 10);
        Integer requiredSpots = 100000;
        Integer amounDaysForTheReservation = 1;
        ScheduleType scheduleType = ScheduleType.BY_SPOT;

        //then
        UseCaseException ex = assertThrows(UseCaseException.class, () -> {
            campsite.getAvailabilitySchedule(dateFrom, dateTo, requiredSpots, amounDaysForTheReservation, scheduleType);
        });
        assertEquals(ex.getMessageId(), MessageId.REQUIRED_SPOTS_GREATER_THAN_MAX_CAPACITY.getId());
    }

    @Test
    void test_getAvailabilitySchedule_maxReservationTimeExceed() {
        //given
        LocalDate dateFrom = LocalDate.of(2019, 2, 10);
        LocalDate dateTo = LocalDate.of(2019, 3, 10);
        Integer requiredSpots = 1;
        Integer amounDaysForTheReservation = 1;
        ScheduleType scheduleType = ScheduleType.BY_SPOT;

        doReturn(Boolean.TRUE).when(stayingRestrictions).isMaxReservationTimeExceed(any());

        //then
        UseCaseException ex = assertThrows(UseCaseException.class, () -> {
            campsite.getAvailabilitySchedule(dateFrom, dateTo, requiredSpots, amounDaysForTheReservation, scheduleType);
        });
        assertEquals(ex.getMessageId(), MessageId.MAX_RESERVATION_TIME_EXCEED.getId());
    }

    @Test
    void test_getAvailabilitySchedule_campsiteIsClosed() {
        //given
        LocalDate dateFrom = LocalDate.of(2019, 2, 10);
        LocalDate dateTo = LocalDate.of(2019, 3, 10);
        Integer requiredSpots = 1;
        Integer amounDaysForTheReservation = 1;
        ScheduleType scheduleType = ScheduleType.BY_SPOT;

        doReturn(Boolean.FALSE).when(stayingRestrictions).isMaxReservationTimeExceed(any());
        doReturn(Boolean.FALSE).when(stayingRestrictions).isCampsiteOpenInRange(any(), any());

        //when
        CampsiteAvailabilitySchedule schedule = campsite.getAvailabilitySchedule(dateFrom, dateTo, requiredSpots, amounDaysForTheReservation, scheduleType);

        //then
        assertNotNull(schedule);
        assertEquals(CampsiteAvailabilityStatus.CLOSED, schedule.getStatus());
        assertEquals(0, schedule.getSchedule().size());
    }

    @Test
    void test_getAvailabilitySchedule_campsiteIsFull() {
        //given
        LocalDate dateFrom = LocalDate.of(2019, 2, 10);
        LocalDate dateTo = LocalDate.of(2019, 3, 10);
        Integer requiredSpots = 1;
        Integer amounDaysForTheReservation = 1;
        ScheduleType scheduleType = ScheduleType.BY_SPOT;

        doReturn(Boolean.FALSE).when(stayingRestrictions).isMaxReservationTimeExceed(any());
        doReturn(Boolean.TRUE).when(stayingRestrictions).isCampsiteOpenInRange(any(), any());

        ScheduleEventsHandler scheduleEventsHandler = spy(ScheduleEventsHandler.class);
        doReturn(Collections.emptyList()).when(scheduleEventsHandler).execute();
        doReturn(scheduleEventsHandler).when(campsite).createScheduleHandler(any(), any(), any(), any(), any());

        //when
        CampsiteAvailabilitySchedule<ScheduleEventBySpot> schedule = campsite.getAvailabilitySchedule(dateFrom, dateTo, requiredSpots, amounDaysForTheReservation, scheduleType);

        //then
        assertNotNull(schedule);
        assertEquals(CampsiteAvailabilityStatus.FULL, schedule.getStatus());
        assertEquals(0, schedule.getSchedule().size());
    }

    /*
     * Tests for Campsite#reserve
     */
    @Test
    void test_validateReservation_ok() {
        //given
        Camper camper = Camper.builder().email("test_9999@gmail.com").build();
        LocalDate dateFrom = LocalDate.of(2019, 2, 10);
        LocalDate dateTo = LocalDate.of(2019, 3, 10);
        Integer requiredSpots = 1;

        doNothing().when(stayingRestrictions).validateStayingDate( any(), any());

        //when
        campsite.validateReservation(camper, dateFrom, dateTo, requiredSpots);
    }

    @Test
    void test_validateReservation_camperHasAnActiveReservation() {
        //given
        Camper camper = Camper.builder().email("test_1@gmail.com").build();
        LocalDate dateFrom = LocalDate.of(2019, 2, 10);
        LocalDate dateTo = LocalDate.of(2019, 3, 10);
        Integer requiredSpots = 1;

        doNothing().when(stayingRestrictions).validateStayingDate( any(), any());

        //then
        assertThrows(UseCaseException.class, () -> {
            campsite.validateReservation(camper, dateFrom, dateTo, requiredSpots);
        });
    }

    /*
     * Tests for Campsite#reserve
     */
    @Test
    void test_reserve_ok() {
        //given
        Camper camper = Camper.builder().email("test_1@gmail.com").build();
        LocalDate dateFrom = LocalDate.of(2019, 2, 10);
        LocalDate dateTo = LocalDate.of(2019, 3, 10);
        Integer requiredSpots = 1;

        doNothing().when(campsite).validateReservation(any(), any(), any(), any());
        doReturn(
            new ArrayList<>(Arrays.asList(
                new Spot(5L)
            ))
        ).when(campsite).getFreeSpots(any(), any());

        //when
        Reservation reservation = campsite.reserve(camper, dateFrom, dateTo, requiredSpots);

        //then
        assertNotNull(reservation);
        assertEquals( 3, campsite.getReservations().size());
        assertEquals( ReservationStatus.PENDING, reservation.getStatus());
        assertEquals( requiredSpots.intValue(), reservation.getReservedSpots().size());
    }

    @Test
    void test_reserve_validateReservationThrowsAnException() {
        //given
        Camper camper = Camper.builder().email("test_1@gmail.com").build();
        LocalDate dateFrom = LocalDate.of(2019, 2, 10);
        LocalDate dateTo = LocalDate.of(2019, 3, 10);
        Integer requiredSpots = 1;

        doThrow(UseCaseException.class).when(campsite).validateReservation(any(), any(), any(), any());

        //then
        assertThrows(UseCaseException.class, () -> {
            campsite.reserve(camper, dateFrom, dateTo, requiredSpots);
        });
    }

    @Test
    void test_reserve_getFreeSpotsThrowsAnException() {
        //given
        Camper camper = Camper.builder().email("test_1@gmail.com").build();
        LocalDate dateFrom = LocalDate.of(2019, 2, 10);
        LocalDate dateTo = LocalDate.of(2019, 3, 10);
        Integer requiredSpots = 1;

        doNothing().when(campsite).validateReservation(any(), any(), any(), any());
        doThrow(UseCaseException.class).when(campsite).getFreeSpots(any(), any());

        //then
        assertThrows(UseCaseException.class, () -> {
            campsite.reserve(camper, dateFrom, dateTo, requiredSpots);
        });
    }

}
