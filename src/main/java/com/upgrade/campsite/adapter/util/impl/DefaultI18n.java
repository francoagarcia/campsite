package com.upgrade.campsite.adapter.util.impl;

import com.upgrade.campsite.core.port.util.I18n;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;

import javax.inject.Inject;
import java.util.Locale;
import java.util.logging.Logger;

public class DefaultI18n implements I18n {

    private static final Logger logger = Logger.getLogger(DefaultI18n.class.getName());
    private MessageSource messageSource;

    @Inject
    public DefaultI18n(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Override
    public String tr(String key, Locale locale) {
        String message = null;
        try {
            message = messageSource.getMessage(key, null, locale);
        } catch(NoSuchMessageException e){
            logger.severe(e.getMessage());
        }
        return message;
    }

    @Override
    public String tr(String key, Object[] args, Locale locale) {
        String message = null;
        try {
            message = messageSource.getMessage(key, args, locale);
        } catch(NoSuchMessageException e){
            logger.severe(e.getMessage());
        }
        return message;
    }

}
