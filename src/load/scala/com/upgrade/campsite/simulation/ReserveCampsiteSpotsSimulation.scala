package com.upgrade.campsite.simulation

import io.gatling.core.Predef._
import io.gatling.core.feeder.SourceFeederBuilder
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder

import scala.concurrent.duration._
import scala.util.Random

class ReserveCampsiteSpotsSimulation extends Simulation {

    val rampUpTimeSecs: Integer = 5
    val testTimeSecs: Integer = 20
    val noOfUsers: Integer = 20
    val minWaitMs: FiniteDuration = 1000 milliseconds
    val maxWaitMs: FiniteDuration = 3000 milliseconds

    val baseURL: String = "http://localhost:8080"
    val baseName: String = "reserve-campsite-spots"
    val requestName: String = baseName + "-request"
    val scenarioName: String = baseName + "-scenario"
    val URI: String = "/campsite/reservations"
    val bodyJson: String =
      """
        |{
        |    "email": "test_${id}@gmail.com",
        |    "fullname": "Test Name ${id}",
        |    "arrival_date": "2019-05-${from}",
        |    "departure_date": "2019-05-${to}",
        |    "required_spots": 1
        |}
      """.stripMargin

    var r = new Random()
    val feederId: Iterator[Map[String, Int]] = Iterator.continually( Map("id" -> r.nextInt()) )
    val feederDates: SourceFeederBuilder[String] = Array(
        Map("from" -> "01", "to" -> "02"),
        Map("from" -> "02", "to" -> "03"),
        Map("from" -> "03", "to" -> "04"),
        Map("from" -> "04", "to" -> "05"),
        Map("from" -> "05", "to" -> "06"),
        Map("from" -> "06", "to" -> "07"),
        Map("from" -> "07", "to" -> "08"),
        Map("from" -> "08", "to" -> "09"),
        Map("from" -> "09", "to" -> "10"),
        Map("from" -> "10", "to" -> "11"),
        Map("from" -> "11", "to" -> "12"),
        Map("from" -> "12", "to" -> "13"),
        Map("from" -> "13", "to" -> "14"),
    ).random

    val httpConfig: HttpProtocolBuilder = http
        .baseUrl(baseURL)
        .header("Accept", "application/json")
        .header("Content-Type", "application/json")
        .doNotTrackHeader("1")
        .acceptLanguageHeader("en-US,en;q=0.5")
        .acceptEncodingHeader("gzip, deflate")
        .userAgentHeader("Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0")

    val scenarioBuilder: ScenarioBuilder = scenario(scenarioName)
        .during(testTimeSecs) {
            exec(
                http(requestName)
                    .post(URI)
                    .body(StringBody(bodyJson)).asJson
                    .check(status.is(200))

            ).pause(minWaitMs, maxWaitMs)
             .feed(feederId)
             .feed(feederDates)
        }

    setUp(
        scenarioBuilder.inject(atOnceUsers(noOfUsers))
    ).protocols(httpConfig)

}
