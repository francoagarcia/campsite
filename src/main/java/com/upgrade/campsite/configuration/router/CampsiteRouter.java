package com.upgrade.campsite.configuration.router;

import com.upgrade.campsite.core.entity.ScheduleType;
import com.upgrade.campsite.entrypoint.CancelReservationEndpoint;
import com.upgrade.campsite.entrypoint.ModifyReservationEndpoint;
import com.upgrade.campsite.entrypoint.ReserveCampsiteSpotsEndpoint;
import com.upgrade.campsite.entrypoint.RetrieveCampsiteAvailabilityEndpoint;
import com.upgrade.campsite.entrypoint.resource.CampsiteAvailabilityResource;
import com.upgrade.campsite.entrypoint.resource.ReservationResource;
import com.upgrade.campsite.entrypoint.resource.ScheduleEventByRangeResource;
import com.upgrade.campsite.entrypoint.resource.ScheduleEventBySpotResource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.beans.PropertyEditorSupport;
import java.time.LocalDate;

import static org.springframework.http.ResponseEntity.noContent;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * Router for the campsite related requests.
 *
 * NOTE: Since we have only one campsite, I have decided keep both paths:
 *   "/campsites/{campsite_id}" one in plural, then you can specify a campsite ID.
 *   "/campsite" another in singular, taking the campsite ID by default.
 */
@RestController
public class CampsiteRouter {

    /**
     * Get campsite availability grouping results by Spot
     */
    @Transactional(readOnly = true)
    @RequestMapping(method = GET, path = {"/campsites/{id}/availability", "/campsite/availability"}) //Default
    public ResponseEntity<CampsiteAvailabilityResource<ScheduleEventBySpotResource>> retrieveCamperIsAvailableBySpot(
        @PathVariable(value = "id", required = false) String campsiteId,
        @RequestParam(value = "date_from", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateFrom,
        @RequestParam(value = "date_to", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateTo,
        @RequestParam(value = "required_spots", required = false) Integer requiredSpots,
        @RequestParam(value = "reservation_days", required = false) Integer reservationDays,
        @RequestParam(value = "schedule_type", required = false) ScheduleType scheduleType,
        RetrieveCampsiteAvailabilityEndpoint<ScheduleEventBySpotResource> retrieveCampsiteAvailabilityEndpoint){

        CampsiteAvailabilityResource<ScheduleEventBySpotResource> campsiteAvailability = retrieveCampsiteAvailabilityEndpoint
            .execute(campsiteId, dateFrom, dateTo, requiredSpots, reservationDays, scheduleType);
        return ok(campsiteAvailability);
    }

    /**
     * Get campsite availability grouping results by Date Range
     */
	@Transactional(readOnly = true)
	@RequestMapping(method = GET, path = {"/campsites/{id}/availability", "/campsite/availability"}, params = "schedule_type=by_range")
	public ResponseEntity<CampsiteAvailabilityResource<ScheduleEventByRangeResource>> retrieveCamperIsAvailableByRange(
        @PathVariable(value = "id", required = false) String campsiteId,
        @RequestParam(value = "date_from", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateFrom,
        @RequestParam(value = "date_to", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateTo,
        @RequestParam(value = "required_spots", required = false) Integer requiredSpots,
        @RequestParam(value = "reservation_days", required = false) Integer reservationDays,
        @RequestParam(value = "schedule_type", required = false) ScheduleType scheduleType,
        RetrieveCampsiteAvailabilityEndpoint<ScheduleEventByRangeResource> retrieveCampsiteAvailabilityEndpoint){

        CampsiteAvailabilityResource<ScheduleEventByRangeResource> campsiteAvailability = retrieveCampsiteAvailabilityEndpoint
            .execute(campsiteId, dateFrom, dateTo, requiredSpots, reservationDays, scheduleType);
        return ok(campsiteAvailability);
	}

    /**
     * Make a reservation
     */
    @Transactional
    @RequestMapping(method = POST, path = {"/campsites/{id}/reservations", "/campsite/reservations"})
    public ResponseEntity<ReservationResource> reserveSpots(
        @PathVariable(value = "id", required = false) String campsiteId,
        @Valid @RequestBody ReserveCampsiteSpotsEndpoint.ReservationRequest reservationRequest,
        ReserveCampsiteSpotsEndpoint reserveCampsiteSpotsEndpoint){

        ReservationResource reservationResource = reserveCampsiteSpotsEndpoint.execute(campsiteId, reservationRequest);
        return ok(reservationResource);
    }

    /**
     * Modify a reservation
     */
    @Transactional
    @RequestMapping(method = PUT, path = {"/campsites/{campsiteId}/reservations/{reservationId}", "/campsite/reservations/{reservationId}"})
    public ResponseEntity<Void> modifyReservation(
        @PathVariable(value = "campsiteId", required = false) String campsiteId,
        @PathVariable(value = "reservationId", required = false) String reservationId,
        @RequestBody ModifyReservationEndpoint.ModifyReservationRequest request,
        ModifyReservationEndpoint modifyReservationEndpoint){

        modifyReservationEndpoint.execute(campsiteId, reservationId, request);
        return noContent().build();
    }

    /**
     * Cancel a reservation
     */
    @Transactional
    @RequestMapping(method = DELETE, path = {"/campsites/{campsiteId}/reservations/{reservationId}", "/campsite/reservations/{reservationId}"})
    public ResponseEntity<Void> cancelReservation(
        @PathVariable(value = "campsiteId", required = false) String campsiteId,
        @PathVariable(value = "reservationId", required = false) String reservationId,
        CancelReservationEndpoint cancelReservationEndpoint){

        cancelReservationEndpoint.execute(campsiteId, reservationId);
        return noContent().build();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Spring workaround for lower_case enums values in request params.
     */
    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        dataBinder.registerCustomEditor(ScheduleType.class, new PropertyEditorSupport(){
            @Override
            public void setAsText(String text) throws IllegalArgumentException {
                setValue(ScheduleType.valueOf(text.toUpperCase()));
            }
        });
    }
}
