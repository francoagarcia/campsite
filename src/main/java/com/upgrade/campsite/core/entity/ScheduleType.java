package com.upgrade.campsite.core.entity;

public enum ScheduleType {

    BY_RANGE,
    BY_SPOT;

    @Override
    public String toString() {
        return this.name().toLowerCase();
    }
}
