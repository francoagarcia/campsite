package com.upgrade.campsite.core.entity;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

//TODO upgrade gradle to version 5
//Run it only with the IDE. If you run this test with gradle, it will fail because a compatibility issue between java11 and gradle 4.10.3
class ScheduleEventsHandlerTest {

    @Test
    void getCommands() {
        //given
        ScheduleEventsHandler scheduleEventsHandler = new ScheduleEventsHandler();

        //when
        Map commands = scheduleEventsHandler.getCommands();

        //then
        assertNotNull( commands );
        assertEquals( 2, commands.size() );
        assertEquals( CreateScheduleBySpotCommand.class, commands.get(ScheduleType.BY_SPOT).getClass() );
        assertEquals( CreateScheduleByRangeCommand.class, commands.get(ScheduleType.BY_RANGE).getClass() );
    }

    @Test
    void execute_withCommandBySpot() {
        //given
        LocalDate dateFrom = LocalDate.of(2019, 2, 10);
        LocalDate dateTo = LocalDate.of(2019, 3, 10);
        Integer requiredSpots = 1;
        Integer amounDaysForTheReservation = 1;
        ScheduleType scheduleType = ScheduleType.BY_SPOT;
        Campsite campsite = Mockito.spy(Campsite.class);

        ScheduleCommand command = Mockito.mock(CreateScheduleBySpotCommand.class);
        doReturn(Arrays.asList(new ScheduleEventBySpot(), new ScheduleEventBySpot())).when(command).execute(any());

        ScheduleEventsHandler scheduleEventsHandler = new ScheduleEventsHandler(dateFrom, dateTo, requiredSpots, amounDaysForTheReservation, scheduleType);
        scheduleEventsHandler.setCommands(new HashMap<>(){{
            put(ScheduleType.BY_SPOT, command);
        }});

        scheduleEventsHandler.setCampsite(campsite);

        //when
        List events = scheduleEventsHandler.execute();

        //then
        assertNotNull(events);
        assertEquals(2, events.size());
    }

    @Test
    void execute_withCommandByRange() {
        //given
        LocalDate dateFrom = LocalDate.of(2019, 2, 10);
        LocalDate dateTo = LocalDate.of(2019, 3, 10);
        Integer requiredSpots = 1;
        Integer amounDaysForTheReservation = 1;
        ScheduleType scheduleType = ScheduleType.BY_RANGE;
        Campsite campsite = Mockito.spy(Campsite.class);

        ScheduleCommand command = Mockito.mock(CreateScheduleByRangeCommand.class);
        doReturn(Arrays.asList(new ScheduleEventByRange(), new ScheduleEventByRange())).when(command).execute(any());

        ScheduleEventsHandler scheduleEventsHandler = new ScheduleEventsHandler(dateFrom, dateTo, requiredSpots, amounDaysForTheReservation, scheduleType);
        scheduleEventsHandler.setCommands(new HashMap<>(){{
            put(ScheduleType.BY_RANGE, command);
        }});

        scheduleEventsHandler.setCampsite(campsite);

        //when
        List events = scheduleEventsHandler.execute();

        //then
        assertNotNull(events);
        assertEquals(2, events.size());
    }

    @Test
    void execute_withAnInvalidCommand() {
        //given
        LocalDate dateFrom = LocalDate.of(2019, 2, 10);
        LocalDate dateTo = LocalDate.of(2019, 3, 10);
        Integer requiredSpots = 1;
        Integer amounDaysForTheReservation = 1;
        ScheduleType scheduleType = null;
        Campsite campsite = Mockito.spy(Campsite.class);

        ScheduleEventsHandler scheduleEventsHandler = new ScheduleEventsHandler(dateFrom, dateTo, requiredSpots, amounDaysForTheReservation, scheduleType);
        scheduleEventsHandler.setCampsite(campsite);

        //then
        assertThrows(UnsupportedOperationException.class, scheduleEventsHandler::execute);
    }
}
