package com.upgrade.campsite.core.usecase.impl;

import com.upgrade.campsite.core.entity.*;
import com.upgrade.campsite.core.exception.NotFoundException;
import com.upgrade.campsite.core.exception.UseCaseException;
import com.upgrade.campsite.core.port.repository.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;

class DefaultModifyReservationTest {

    private DefaultModifyReservation useCase;
    private CamperRepository camperRepository;
    private CampsiteRepository campsiteRepository;
    private SpotRepository spotRepository;
    private ReservationRepository reservationRepository;
    private ReservedSpotRepository reservedSpotRepository;

    @BeforeEach
    void setUp() {
        this.camperRepository = Mockito.mock(CamperRepository.class);
        this.campsiteRepository = Mockito.mock(CampsiteRepository.class);
        this.reservationRepository = Mockito.mock(ReservationRepository.class);
        this.spotRepository = Mockito.mock(SpotRepository.class);
        this.reservedSpotRepository = Mockito.mock(ReservedSpotRepository.class);

        this.useCase = Mockito.spy(
            new DefaultModifyReservation(camperRepository, campsiteRepository, reservationRepository, spotRepository, reservedSpotRepository)
        );
    }

    @AfterEach
    void tearDown() {
    }

    /*
     * Tests for DefaultModifyReservation#initialize
     */
    @Test
    void test_initialize_ok() {
        //given
        String campsiteId = "123ABC";
        Long reservationId = 1L;
        String email = "test_1@gmail.com";
        String name = "Test Name";
        LocalDate arrivalDate = LocalDate.of(2019, 3,10);
        LocalDate departureDate = LocalDate.of(2019, 3,13);
        Integer requiredSpots = 1;

        //when
        assertDoesNotThrow(() -> {
            useCase.initialize(campsiteId, reservationId, name, email, arrivalDate, departureDate, requiredSpots);
        });
    }

    @Test
    void test_initialize_reservationIdIsRequired() {
        //given
        String campsiteId = "123ABC";
        Long reservationId = null;
        String email = "test_1@gmail.com";
        String name = "Test Name";
        LocalDate arrivalDate = LocalDate.of(2019, 3,10);
        LocalDate departureDate = LocalDate.of(2019, 3,13);
        Integer requiredSpots = 1;

        //when
        UseCaseException ex = assertThrows(UseCaseException.class, () -> {
            useCase.initialize(campsiteId, reservationId, name, email, arrivalDate, departureDate, requiredSpots);
        });
        assertEquals(MessageId.RESERVATION_ID_REQUIRED.getId(), ex.getMessageId());
    }

    @Test
    void test_initialize_dateRangeIsInvalid() {
        //given
        String campsiteId = "123ABC";
        Long reservationId = 1L;
        String email = "test_1@gmail.com";
        String name = "Test Name";
        LocalDate arrivalDate = LocalDate.of(2019, 3,10);
        LocalDate departureDate = LocalDate.of(2019, 3,9);
        Integer requiredSpots = 1;

        //when
        UseCaseException ex = assertThrows(UseCaseException.class, () -> {
            useCase.initialize(campsiteId, reservationId, name, email, arrivalDate, departureDate, requiredSpots);
        });
        assertEquals(MessageId.INVALID_DATE_RANGE.getId(), ex.getMessageId());
    }

    @Test
    void test_initialize_requiredSpotsIsInvalid() {
        //given
        String campsiteId = "123ABC";
        Long reservationId = 1L;
        String email = "test_1@gmail.com";
        String name = "Test Name";
        LocalDate arrivalDate = LocalDate.of(2019, 3,10);
        LocalDate departureDate = LocalDate.of(2019, 3,13);
        Integer requiredSpots = -1;

        //when
        UseCaseException ex = assertThrows(UseCaseException.class, () -> {
            useCase.initialize(campsiteId, reservationId, name, email, arrivalDate, departureDate, requiredSpots);
        });
        assertEquals(MessageId.INVALID_SPOTS_NUMBER.getId(), ex.getMessageId());
    }

    /*
     * Tests for DefaultModifyReservation#setDefaults
     */
    @Test
    void test_setDefaults_campsiteId() {
        //given
        String campsiteId = null;
        Long reservationId = 1L;
        String email = "test_1@gmail.com";
        String name = "Test Name";
        LocalDate arrivalDate = LocalDate.of(2019, 3,10);
        LocalDate departureDate = LocalDate.of(2019, 3,13);
        Integer requiredSpots = 1;

        DefaultModifyReservation.Model model = new DefaultModifyReservation.Model(campsiteId, reservationId, email, name, arrivalDate, departureDate, requiredSpots);

        //when
        useCase.setDefaults(model);

        //then
        assertEquals(Campsite.CAMPSITE_ID, model.getCampsiteId());
    }

    /*
     * Tests for DefaultModifyReservation#get
     *
     * Happy path:
     * Reservation ok & email provided + camper exists & modify the camper name
     * Reservation ok & email provided + camper is new + previous full name
     * Reservation ok & email provided + camper is new + new full name
     * Reservation ok & arrivalDate provided
     * Reservation ok & departureDate provided
     * Reservation ok & requiredSpots provided
     *
     * Unhappy path:
     * Reservation not found
     * Reservation found but not active
     * Campsite not found
     */
    @Test
    void test_get_reservationOk_emailProvided_camperExists_changeName_HAPPY() {
        //given
        String campsiteId = "123ABC";
        Long reservationId = 1L;
        String email = "test_1@gmail.com";
        String name = "NEW NAME";
        LocalDate arrivalDate = null;
        LocalDate departureDate = null;
        Integer requiredSpots = null;

        Reservation reservation = createReservation();

        Camper camper = Camper.builder().email(email).fullname("another name wow").build();

        //campsite mocks
        var campsite = Mockito.spy(Campsite.class);
        doNothing()
            .when(campsite).removeReservation(any());
        doNothing()
            .when(campsite).addReservation(any());
        doNothing()
            .when(campsite).validateReservation(any(), any(), any(), any());
        doReturn(new ArrayList<>(Arrays.asList(new Spot(5L))))
            .when(campsite).getFreeSpots( any(), any());

        //Repos mocks
        doReturn(Optional.of(reservation)).when(reservationRepository).findOne(any());
        doReturn(Optional.of(camper)).when(camperRepository).findOneByEmail(anyString());
        doReturn(Optional.of(campsite)).when(campsiteRepository).findOne(anyString());
        doReturn(Collections.<Spot>emptySet()).when(spotRepository).findForCampsite(any());
        doReturn(Collections.<Reservation>emptyList()).when(reservationRepository).findActiveReservations(any(), any());
        doReturn(Collections.<Reservation>emptyList()).when(reservedSpotRepository).findAllByReservation(any());

        useCase.initialize(campsiteId, reservationId, name, email, arrivalDate, departureDate, requiredSpots);

        //when
        Reservation result = useCase.get();

        //then
        assertNotNull(result);
        assertEquals(reservationId, result.getId());
        assertEquals(name, result.getCamper().getFullname());
    }

    @Test
    void test_get_reservationOk_emailProvided_fullnameNotProvided_camperNew_usePreviousFullname_HAPPY() {
        //given
        String campsiteId = "123ABC";
        Long reservationId = 1L;
        String email = "test_NEW_EMAIL@gmail.com";
        String name = null;
        LocalDate arrivalDate = null;
        LocalDate departureDate = null;
        Integer requiredSpots = null;

        Reservation reservation = createReservation();

        //campsite mocks
        var campsite = Mockito.spy(Campsite.class);
        doNothing()
            .when(campsite).removeReservation(any());
        doNothing()
            .when(campsite).addReservation(any());
        doNothing()
            .when(campsite).validateReservation(any(), any(), any(), any());
        doReturn(new ArrayList<>(Arrays.asList(new Spot(5L))))
            .when(campsite).getFreeSpots( any(), any());

        //Repos mocks
        doReturn(Optional.of(reservation)).when(reservationRepository).findOne(any());
        doReturn(Optional.empty()).when(camperRepository).findOneByEmail(anyString());
        doReturn(Optional.of( campsite )).when(campsiteRepository).findOne(anyString());
        doReturn(Collections.<Spot>emptySet()).when(spotRepository).findForCampsite(any());
        doReturn(Collections.<Reservation>emptyList()).when(reservationRepository).findActiveReservations(any(), any());
        doReturn(Collections.<Reservation>emptyList()).when(reservedSpotRepository).findAllByReservation(any());

        useCase.initialize(campsiteId, reservationId, name, email, arrivalDate, departureDate, requiredSpots);

        //when
        Reservation result = useCase.get();

        //then
        assertNotNull(result);
        assertEquals(reservationId, result.getId());
        assertEquals(email, result.getCamper().getEmail());
        assertEquals("Test Name", result.getCamper().getFullname());
    }

    @Test
    void test_get_reservationOk_emailProvided_fullnameProvided_camperNew_newFullname_HAPPY() {
        //given
        String campsiteId = "123ABC";
        Long reservationId = 1L;
        String email = "test_NEW_EMAIL@gmail.com";
        String name = "NEW PERSON WOW";
        LocalDate arrivalDate = null;
        LocalDate departureDate = null;
        Integer requiredSpots = null;

        Reservation reservation = createReservation();

        //campsite mocks
        var campsite = Mockito.spy(Campsite.class);
        doNothing()
            .when(campsite).removeReservation(any());
        doNothing()
            .when(campsite).addReservation(any());
        doNothing()
            .when(campsite).validateReservation(any(), any(), any(), any());
        doReturn(new ArrayList<>(Arrays.asList(new Spot(5L))))
            .when(campsite).getFreeSpots( any(), any());

        //Repos mocks
        doReturn(Optional.of(reservation)).when(reservationRepository).findOne(any());
        doReturn(Optional.empty()).when(camperRepository).findOneByEmail(anyString());
        doReturn(Optional.of( campsite )).when(campsiteRepository).findOne(anyString());
        doReturn(Collections.<Spot>emptySet()).when(spotRepository).findForCampsite(any());
        doReturn(Collections.<Reservation>emptyList()).when(reservationRepository).findActiveReservations(any(), any());
        doReturn(Collections.<Reservation>emptyList()).when(reservedSpotRepository).findAllByReservation(any());

        useCase.initialize(campsiteId, reservationId, name, email, arrivalDate, departureDate, requiredSpots);

        //when
        Reservation result = useCase.get();

        //then
        assertNotNull(result);
        assertEquals(reservationId, result.getId());
        assertEquals(email, result.getCamper().getEmail());
        assertEquals(name, result.getCamper().getFullname());
    }

    @Test
    void test_get_reservationOk_arrivalDateProvided_HAPPY() {
        //given
        String campsiteId = "123ABC";
        Long reservationId = 1L;
        String email = null;
        String name = null;
        LocalDate arrivalDate = LocalDate.of(2019, 2, 16);
        LocalDate departureDate = null;
        Integer requiredSpots = null;

        Reservation reservation = createReservation();

        //campsite mocks
        var campsite = Mockito.spy(Campsite.class);
        doNothing()
            .when(campsite).removeReservation(any());
        doNothing()
            .when(campsite).addReservation(any());
        doNothing()
            .when(campsite).validateReservation(any(), any(), any(), any());
        doReturn(new ArrayList<>(Arrays.asList(new Spot(5L))))
            .when(campsite).getFreeSpots( any(), any());

        //Repos mocks
        doReturn(Optional.of(reservation)).when(reservationRepository).findOne(any());
        doReturn(Optional.empty()).when(camperRepository).findOneByEmail(anyString());
        doReturn(Optional.of( campsite )).when(campsiteRepository).findOne(anyString());
        doReturn(Collections.<Spot>emptySet()).when(spotRepository).findForCampsite(any());
        doReturn(Collections.<Reservation>emptyList()).when(reservationRepository).findActiveReservations(any(), any());
        doReturn(Collections.<Reservation>emptyList()).when(reservedSpotRepository).findAllByReservation(any());

        useCase.initialize(campsiteId, reservationId, name, email, arrivalDate, departureDate, requiredSpots);

        //when
        Reservation result = useCase.get();

        //then
        assertNotNull(result);
        assertEquals(reservationId, result.getId());
        assertEquals(arrivalDate, result.getArrivalDate());
    }

    @Test
    void test_get_reservationOk_departureDateProvided_HAPPY() {
        //given
        String campsiteId = "123ABC";
        Long reservationId = 1L;
        String email = null;
        String name = null;
        LocalDate arrivalDate = null;
        LocalDate departureDate = LocalDate.of(2019, 2, 17);
        Integer requiredSpots = null;

        Reservation reservation = createReservation();

        //campsite mocks
        var campsite = Mockito.spy(Campsite.class);
        doNothing()
            .when(campsite).removeReservation(any());
        doNothing()
            .when(campsite).addReservation(any());
        doNothing()
            .when(campsite).validateReservation(any(), any(), any(), any());
        doReturn(new ArrayList<>(Arrays.asList(new Spot(5L))))
            .when(campsite).getFreeSpots( any(), any());

        //Repos mocks
        doReturn(Optional.of(reservation)).when(reservationRepository).findOne(any());
        doReturn(Optional.empty()).when(camperRepository).findOneByEmail(anyString());
        doReturn(Optional.of( campsite )).when(campsiteRepository).findOne(anyString());
        doReturn(Collections.<Spot>emptySet()).when(spotRepository).findForCampsite(any());
        doReturn(Collections.<Reservation>emptyList()).when(reservationRepository).findActiveReservations(any(), any());
        doReturn(Collections.<Reservation>emptyList()).when(reservedSpotRepository).findAllByReservation(any());

        useCase.initialize(campsiteId, reservationId, name, email, arrivalDate, departureDate, requiredSpots);

        //when
        Reservation result = useCase.get();

        //then
        assertNotNull(result);
        assertEquals(reservationId, result.getId());
        assertEquals(departureDate, result.getDepartureDate());
    }

    @Test
    void test_get_reservationOk_requiredSpotsProvided_HAPPY() {
        //given
        String campsiteId = "123ABC";
        Long reservationId = 1L;
        String email = null;
        String name = null;
        LocalDate arrivalDate = null;
        LocalDate departureDate = null;
        Integer requiredSpots = 3;

        Reservation reservation = createReservation();

        //campsite mocks
        var campsite = Mockito.spy(Campsite.class);
        doNothing()
            .when(campsite).removeReservation(any());
        doNothing()
            .when(campsite).addReservation(any());
        doNothing()
            .when(campsite).validateReservation(any(), any(), any(), any());
        doReturn(new ArrayList<>(Arrays.asList(new Spot(1L), new Spot(2L), new Spot(5L))))
            .when(campsite).getFreeSpots( any(), any());

        //Repos mocks
        doReturn(Optional.of(reservation)).when(reservationRepository).findOne(any());
        doReturn(Optional.empty()).when(camperRepository).findOneByEmail(anyString());
        doReturn(Optional.of( campsite )).when(campsiteRepository).findOne(anyString());
        doReturn(Collections.<Spot>emptySet()).when(spotRepository).findForCampsite(any());
        doReturn(Collections.<Reservation>emptyList()).when(reservationRepository).findActiveReservations(any(), any());
        doReturn(Collections.<Reservation>emptyList()).when(reservedSpotRepository).findAllByReservation(any());

        useCase.initialize(campsiteId, reservationId, name, email, arrivalDate, departureDate, requiredSpots);

        //when
        Reservation result = useCase.get();

        //then
        assertNotNull(result);
        assertEquals(reservationId, result.getId());
        assertEquals(requiredSpots.intValue(), result.getReservedSpots().size());
    }

    @Test
    void test_get_reservationNotFound_UNHAPPY() {
        //given
        String campsiteId = "123ABC";
        Long reservationId = 999999L;
        String email = null;
        String name = null;
        LocalDate arrivalDate = null;
        LocalDate departureDate = null;
        Integer requiredSpots = null;

        //Repos mocks
        doReturn(Optional.empty()).when(reservationRepository).findOne(any());

        useCase.initialize(campsiteId, reservationId, name, email, arrivalDate, departureDate, requiredSpots);

        //when
        var ex = assertThrows(NotFoundException.class, () -> {
            useCase.get();
        });
        assertEquals(MessageId.RESERVATION_NOT_FOUND.getId(), ex.getMessageId());
    }

    @Test
    void test_get_reservationNotActive_UNHAPPY() {
        //given
        String campsiteId = "123ABC";
        Long reservationId = 1L;
        String email = null;
        String name = null;
        LocalDate arrivalDate = null;
        LocalDate departureDate = LocalDate.of(2019, 2, 17);
        Integer requiredSpots = null;

        Reservation reservation = createReservation();
        reservation.setStatus(ReservationStatus.FINALIZED);

        //Repos mocks
        doReturn(Optional.of(reservation)).when(reservationRepository).findOne(any());

        useCase.initialize(campsiteId, reservationId, name, email, arrivalDate, departureDate, requiredSpots);

        //when
        var ex = assertThrows(UseCaseException.class, () -> {
            useCase.get();
        });
        assertEquals(MessageId.INVALID_UPDATE_RESERVATION.getId(), ex.getMessageId());
    }

    @Test
    void test_get_campsiteNotFound_UNHAPPY() {
        //given
        String campsiteId = "XXXXX_99999";
        Long reservationId = 1L;
        String email = null;
        String name = null;
        LocalDate arrivalDate = null;
        LocalDate departureDate = LocalDate.of(2019, 2, 17);
        Integer requiredSpots = null;

        Reservation reservation = createReservation();

        //Repos mocks
        doReturn(Optional.of(reservation)).when(reservationRepository).findOne(any());
        doReturn(Optional.empty()).when(campsiteRepository).findOne(anyString());

        useCase.initialize(campsiteId, reservationId, name, email, arrivalDate, departureDate, requiredSpots);

        //when
        var ex = assertThrows(NotFoundException.class, () -> {
            useCase.get();
        });
        assertEquals(MessageId.CAMPSITE_NOT_FOUND.getId(), ex.getMessageId());
    }

    private Reservation createReservation(){
        return Reservation.builder()
            .id(1L)
            .spots( new ArrayList<>(Arrays.asList(
                new Spot(1L),
                new Spot(2L))
            ))
            .arrivalDate( LocalDate.of(2019, 2, 15) )
            .departureDate( LocalDate.of(2019, 2, 18) )
            .status(ReservationStatus.PENDING)
            .camper(Camper.builder().email("test_1@gmail.com").fullname("Test Name").build())
            .build();
    }

}
