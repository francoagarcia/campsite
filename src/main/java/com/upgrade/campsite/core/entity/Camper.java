package com.upgrade.campsite.core.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@Builder
public class Camper {

    private Long id;
	private String email;
	private String fullname;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Camper camper = (Camper) o;
        return email.equals(camper.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email);
    }
}
