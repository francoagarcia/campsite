insert into camper(`id`, `email`, `fullname`)
values (1, 'francoagarcia@gmail.com', 'Franco');

insert into campsite(id,
                     maximum_reservation_time,
                     minimum_period_before_reservation,
                     maximum_period_before_reservation,
                     opened_from,
                     opened_to,
                     initial_checkin_time,
                     limit_checkin_time)
values ('5f108bfc-47d6-4a75-8a52-ce79790c73d6',
        259200000000000, -- 3 days in nanos
        86400000000000, -- 1 day in nanos
        2592000000000000, -- 30 days in nanos
        PARSEDATETIME('2019-1-01', 'yyyy-MM-dd'),
        PARSEDATETIME('2019-3-31', 'yyyy-MM-dd'),
        PARSEDATETIME('12:00', 'HH:MM'),
        PARSEDATETIME('12:00', 'HH:MM'));

insert into spot(id, description, campsite_id)
values (1, 'SpotDb number 1', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

insert into spot(id, description, campsite_id)
values (2, 'SpotDb number 2', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

-- insert into reservation(id, arrival_date, departure_date, status, camper_id)
-- values ('14cc67b7-a3ef-4dee-bcd8-b5dc09bfda83',
--          PARSEDATETIME('2019-1-10 11:00', 'yyyy-MM-dd HH:MM'),
--          PARSEDATETIME('2019-1-12 11:00', 'yyyy-MM-dd HH:MM'),
--          'CONFIRMED',
--         1);
--
-- insert into reserved_spot(reservation_id, spot_id, check_in, check_out)
-- values ('14cc67b7-a3ef-4dee-bcd8-b5dc09bfda83',
--          1,
--          PARSEDATETIME('11:00', 'HH:MM'),
--          PARSEDATETIME('11:00', 'HH:MM'));

