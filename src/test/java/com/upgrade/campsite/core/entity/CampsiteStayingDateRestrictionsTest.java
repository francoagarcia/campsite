package com.upgrade.campsite.core.entity;

import com.upgrade.campsite.core.exception.UseCaseException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mockito;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

class CampsiteStayingDateRestrictionsTest {

    private CampsiteStayingDateRestrictions stayingRestrictions;

    @BeforeEach
    void setUp() {
        this.stayingRestrictions = Mockito.spy(CampsiteStayingDateRestrictions.builder()
            .maximumReservationTime(Duration.ofDays(3))
            .openedFrom(LocalDate.of(2019, 2, 1))
            .openedTo(LocalDate.of(2019, 3, 31))
            .initialCheckinTime(LocalTime.of(11, 0))
            .limitCheckinTime(LocalTime.of(11, 0))
            .maximumPeriodBeforeReservation(Duration.ofDays(30))
            .minimumPeriodBeforeReservation(Duration.ofDays(1))
            .build());
    }

    @AfterEach
    void tearDown() {
    }

    /*
     * Tests for CampsiteStayingDateRestrictions#isCampsiteOpenInRange
     */
    @Test
    void test_isCampsiteOpenInRange_return_TRUE() {
        //given
        LocalDate dateFrom = LocalDate.of(2019, 2, 10);
        LocalDate dateTo = LocalDate.of(2019, 3, 10);

        //when
        Boolean result = stayingRestrictions.isCampsiteOpenInRange(dateFrom, dateTo);

        //then
        assertNotNull(result);
        assertTrue(result);
    }

    @Test
    void test_isCampsiteOpenInRange_when_dateFromIsBeforeCampsiteOpened_return_FALSE() {
        //given
        LocalDate dateFrom = LocalDate.of(2019, 1, 10);
        LocalDate dateTo = LocalDate.of(2019, 3, 10);

        //when
        Boolean result = stayingRestrictions.isCampsiteOpenInRange(dateFrom, dateTo);

        //then
        assertNotNull(result);
        assertFalse(result);
    }

    @Test
    void test_isCampsiteOpenInRange_when_dateFromIsAfterCampsiteOpened_return_FALSE() {
        //given
        LocalDate dateFrom = LocalDate.of(2019, 4, 1);
        LocalDate dateTo = LocalDate.of(2019, 4, 10);

        //when
        Boolean result = stayingRestrictions.isCampsiteOpenInRange(dateFrom, dateTo);

        //then
        assertNotNull(result);
        assertFalse(result);
    }

    @Test
    void test_isCampsiteOpenInRange_when_dateToIsAfterCampsiteOpened_return_FALSE() {
        //given
        LocalDate dateFrom = LocalDate.of(2019, 3, 10);
        LocalDate dateTo = LocalDate.of(2019, 4, 10);

        //when
        Boolean result = stayingRestrictions.isCampsiteOpenInRange(dateFrom, dateTo);

        //then
        assertNotNull(result);
        assertFalse(result);
    }

    @Test
    void test_isCampsiteOpenInRange_when_dateToIsBeforeCampsiteOpened_return_FALSE() {
        //given
        LocalDate dateFrom = LocalDate.of(2019, 3, 10);
        LocalDate dateTo = LocalDate.of(2019, 1, 10);

        //when
        Boolean result = stayingRestrictions.isCampsiteOpenInRange(dateFrom, dateTo);

        //then
        assertNotNull(result);
        assertFalse(result);
    }

    /*
     * Tests for CampsiteStayingDateRestrictions#validateStayingDate
     */
    @Test
    void test_validateStayingDate_ok(){
        //given
        LocalDate arrivalDate = LocalDate.of(2019, 3, 10);
        LocalDate departureDate = LocalDate.of(2019, 3, 12);

        doReturn(Boolean.FALSE).when(stayingRestrictions).isMaxReservationTimeExceed(any(), any());
        doReturn(Boolean.FALSE).when(stayingRestrictions).isPeriodBeforeReservationLesserThanMinimum(any(), any());
        doReturn(Boolean.FALSE).when(stayingRestrictions).isPeriodBeforeReservationGreaterThanMaximum(any(), any());

        //then
        assertDoesNotThrow(() -> {
            stayingRestrictions.validateStayingDate(arrivalDate, departureDate);
        });
    }

    @Test
    void test_validateStayingDate_maxReservationTimeExceed(){
        //given
        LocalDate arrivalDate = LocalDate.of(2019, 3, 10);
        LocalDate departureDate = LocalDate.of(2019, 3, 12);

        doReturn(Boolean.TRUE).when(stayingRestrictions).isMaxReservationTimeExceed(any(), any());
        doReturn(Boolean.FALSE).when(stayingRestrictions).isPeriodBeforeReservationLesserThanMinimum(any(), any());
        doReturn(Boolean.FALSE).when(stayingRestrictions).isPeriodBeforeReservationGreaterThanMaximum(any(), any());

        //then
        UseCaseException exception = assertThrows(UseCaseException.class, () -> {
            stayingRestrictions.validateStayingDate(arrivalDate, departureDate);
        });
        assertEquals(MessageId.MAX_RESERVATION_TIME_EXCEED.getId(), exception.getMessageId());
    }

    @Test
    void test_validateStayingDate_periodBeforeReservationLesserThanMinimum(){
        //given
        LocalDate arrivalDate = LocalDate.of(2019, 3, 10);
        LocalDate departureDate = LocalDate.of(2019, 3, 12);

        doReturn(Boolean.FALSE).when(stayingRestrictions).isMaxReservationTimeExceed(any(), any());
        doReturn(Boolean.TRUE).when(stayingRestrictions).isPeriodBeforeReservationLesserThanMinimum(any(), any());
        doReturn(Boolean.FALSE).when(stayingRestrictions).isPeriodBeforeReservationGreaterThanMaximum(any(), any());

        //then
        UseCaseException exception = assertThrows(UseCaseException.class, () -> {
            stayingRestrictions.validateStayingDate(arrivalDate, departureDate);
        });
        assertEquals(MessageId.MIN_PERIOD_BEFORE_RESERVATION_EXCEED.getId(), exception.getMessageId());
    }

    @Test
    void test_validateStayingDate_periodBeforeReservationGreaterThanMaximum(){
        //given
        LocalDate arrivalDate = LocalDate.of(2019, 3, 10);
        LocalDate departureDate = LocalDate.of(2019, 3, 12);

        doReturn(Boolean.FALSE).when(stayingRestrictions).isMaxReservationTimeExceed(any(), any());
        doReturn(Boolean.FALSE).when(stayingRestrictions).isPeriodBeforeReservationLesserThanMinimum(any(), any());
        doReturn(Boolean.TRUE).when(stayingRestrictions).isPeriodBeforeReservationGreaterThanMaximum(any(), any());

        //then
        UseCaseException exception = assertThrows(UseCaseException.class, () -> {
            stayingRestrictions.validateStayingDate(arrivalDate, departureDate);
        });
        assertEquals(MessageId.MAX_PERIOD_BEFORE_RESERVATION_EXCEED.getId(), exception.getMessageId());
    }

    /*
     * Tests for CampsiteStayingDateRestrictions#isMaxReservationTimeExceed
     */
    @ParameterizedTest
    @CsvSource({
        "2019-03-10, 2019-03-12, false",
        "2019-03-10, 2019-03-13, false",
        "2019-03-10, 2019-03-14, true",
        "2019-02-10, 2019-03-12, true",
    })
    void test_isMaxReservationTimeExceed(String from, String to, Boolean expected){
        //given
        LocalDate arrivalDate = LocalDate.parse(from);
        LocalDate departureDate = LocalDate.parse(to);

        //when
        Boolean result = stayingRestrictions.isMaxReservationTimeExceed(arrivalDate, departureDate);

        //then
        assertEquals(expected, result);
    }

    /*
     * Tests for CampsiteStayingDateRestrictions#isPeriodBeforeReservationLesserThanMinimum
     */
    @ParameterizedTest
    @CsvSource({
        "2019-03-11T08:10:00, 2019-03-11, true",
        "2019-03-10T22:10:00, 2019-03-11, true",
        "2019-03-10T11:00:00, 2019-03-11, false",
        "2019-03-10T10:58:00, 2019-03-11, false",
        "2019-03-09T22:58:00, 2019-03-11, false",
    })
    void test_isPeriodBeforeReservationLesserThanMinimum(String requestInstant, String from, Boolean expected){
        //given
        LocalDateTime now = LocalDateTime.parse(requestInstant);
        LocalDate arrivalDate = LocalDate.parse(from);

        //when
        Boolean result = stayingRestrictions.isPeriodBeforeReservationLesserThanMinimum(now, arrivalDate);

        //then
        assertEquals(expected, result);
    }

    /*
     * Tests for CampsiteStayingDateRestrictions#isPeriodBeforeReservationGreaterThanMaximum
     */
    @ParameterizedTest
    @CsvSource({
        "2019-03-05T08:10:00, 2019-03-31, false",
        "2019-03-01T11:50:00, 2019-03-31, false",
        "2019-03-01T10:58:00, 2019-03-31, true",
        "2019-03-01T05:00:00, 2019-03-31, true",
        "2019-02-28T12:00:00, 2019-03-31, true",
        "2019-02-28T11:00:00, 2019-03-31, true",
        "2019-02-05T08:10:00, 2019-03-31, true",
    })
    void isPeriodBeforeReservationGreaterThanMaximum(String requestInstant, String from, Boolean expected){
        //given
        LocalDateTime now = LocalDateTime.parse(requestInstant);
        LocalDate arrivalDate = LocalDate.parse(from);

        //when
        Boolean result = stayingRestrictions.isPeriodBeforeReservationGreaterThanMaximum(now, arrivalDate);

        //then
        assertEquals(expected, result);
    }
}
