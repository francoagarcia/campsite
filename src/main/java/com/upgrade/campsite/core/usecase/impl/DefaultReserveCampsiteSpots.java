package com.upgrade.campsite.core.usecase.impl;

import com.upgrade.campsite.core.entity.Camper;
import com.upgrade.campsite.core.entity.Campsite;
import com.upgrade.campsite.core.entity.Reservation;
import com.upgrade.campsite.core.exception.NotFoundException;
import com.upgrade.campsite.core.exception.UseCaseException;
import com.upgrade.campsite.core.port.repository.*;
import com.upgrade.campsite.core.usecase.ReserveCampsiteSpots;

import javax.inject.Inject;

import static com.upgrade.campsite.core.entity.MessageId.*;

public class DefaultReserveCampsiteSpots implements ReserveCampsiteSpots {

	private Model model;
	private CamperRepository camperRepository;
	private CampsiteRepository campsiteRepository;
	private ReservationRepository reservationRepository;
	private SpotRepository spotRepository;
    private ReservedSpotRepository reservedSpotRepository;

	@Inject
	public DefaultReserveCampsiteSpots(CamperRepository camperRepository,
                                       CampsiteRepository campsiteRepository,
                                       ReservationRepository reservationRepository,
                                       SpotRepository spotRepository,
                                       ReservedSpotRepository reservedSpotRepository){
		this.camperRepository = camperRepository;
		this.campsiteRepository = campsiteRepository;
		this.reservationRepository = reservationRepository;
		this.spotRepository = spotRepository;
		this.reservedSpotRepository = reservedSpotRepository;
	}

	@Override
	public Reservation get() {
        // Load data
        Campsite campsite = this.campsiteRepository
            .findOne(this.model.getCampsiteId())
            .orElseThrow(NotFoundException.campsite());
        campsite.setReservations( this.reservationRepository.findActiveReservations(campsite, this.model.getDateRange()) );
        campsite.getReservations().forEach( reservation -> reservation.setReservedSpots(this.reservedSpotRepository.findAllByReservation(reservation)) );
        campsite.setSpots( this.spotRepository.findForCampsite(campsite) );

        Camper camper = this.camperRepository.findOneByEmail(this.model.getEmail())
            .orElse(
                Camper.builder()
                    .email(this.model.getEmail())
                    .fullname(this.model.getFullname())
                    .build()
            );

        // Business rules execution
        Reservation reservation = campsite.reserve(camper, this.model.getArrivalDate(), this.model.getDepartureDate(), this.model.getRequiredSpots());

        // Persist
        this.camperRepository.createOrUpdate(camper);
        this.reservationRepository.createReservationForCampsite(reservation, campsite);
        this.reservedSpotRepository.createForReservation(reservation.getReservedSpots(), reservation);

        // Response
	    return reservation;
	}

	@Override
	public void setModel(Model model) {
	    this.setDefaults(model);
		this.validateModel(model);
		this.model = model;
	}

    /**
     * Set defaults for the request model
     * @param model
     */
	public void setDefaults(Model model) {
	    if(model.getCampsiteId() == null){
	        model.setCampsiteId(Campsite.CAMPSITE_ID);
        }
        if(model.getRequiredSpots() == null){
            model.setRequiredSpots(1);
        }
    }

    /**
     * Validates RequestModel and throw an UseCaseException if necessary
     * @param model requestModel
     */
    public void validateModel(Model model){
		if (model.getEmail() == null) {
			throw new UseCaseException(CAMPER_EMAIL_REQUIRED);
		}
        if (model.getFullname() == null) {
            throw new UseCaseException(CAMPER_FULLNAME_REQUIRED);
        }
        if(model.getArrivalDate() == null){
            throw new UseCaseException(ARRIVAL_DATE_REQUIRED);
        }
        if(model.getDepartureDate() == null){
            throw new UseCaseException(DEPARTURE_DATE_REQUIRED);
        }
        //I commented this for facilitate the testing
//        if(!model.getArrivalDate().isAfter(LocalDate.now())){
//            throw new UseCaseException(INVALID_ARRIVAL_DATE);
//        }
//        if(!model.getDepartureDate().isAfter(LocalDate.now())){
//            throw new UseCaseException(INVALID_DEPARTURE_DATE);
//        }
        if(!model.getArrivalDate().isBefore(model.getDepartureDate())){
            throw new UseCaseException(INVALID_DATE_RANGE);
        }
        if(model.getRequiredSpots() <= 0){
            throw new UseCaseException(INVALID_SPOTS_NUMBER);
        }
	}

}
