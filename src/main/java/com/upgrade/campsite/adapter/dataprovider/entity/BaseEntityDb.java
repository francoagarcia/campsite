package com.upgrade.campsite.adapter.dataprovider.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.UUID;

@MappedSuperclass
public abstract class BaseEntityDb {

    @Id
	@Column(columnDefinition = "CHAR(36)")
    private String id;

    public BaseEntityDb(){
        this.id = UUID.randomUUID().toString();
    }

	public String getId() {
		return id;
	}

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public final boolean equals(Object obj) {
        if (this == obj) return true;

        if (obj == null || !(obj instanceof BaseEntityDb)) {
            return false;
        }
        return id.equals(((BaseEntityDb) obj).getId());
    }

    @Override
    public final int hashCode() {
        return id.hashCode();
    }
}
