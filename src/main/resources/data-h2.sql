insert into camper(`id`, `email`, `fullname`)
values (1, 'francoagarcia@gmail.com', 'Franco');

insert into campsite(id,
                     maximum_reservation_time,
                     minimum_period_before_reservation,
                     maximum_period_before_reservation,
                     opened_from,
                     opened_to,
                     initial_checkin_time,
                     limit_checkin_time)
values ('5f108bfc-47d6-4a75-8a52-ce79790c73d6',
        259200000000000, -- 3 days in nanos
        86400000000000, -- 1 day in nanos
        2592000000000000, -- 30 days in nanos
        PARSEDATETIME('2019-1-01', 'yyyy-MM-dd'),
        PARSEDATETIME('2019-6-31', 'yyyy-MM-dd'),
        PARSEDATETIME('12:00', 'HH:MM'),
        PARSEDATETIME('12:00', 'HH:MM'));

insert into spot(id, description, campsite_id)
values (1, 'Spot #1', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

insert into spot(id, description, campsite_id)
values (2, 'Spot #2', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

insert into spot(id, description, campsite_id)
values (3, 'Spot #3', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

insert into spot(id, description, campsite_id)
values (4, 'Spot #4', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

insert into spot(id, description, campsite_id)
values (5, 'Spot #5', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

insert into spot(id, description, campsite_id)
values (6, 'Spot #6', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

insert into spot(id, description, campsite_id)
values (7, 'Spot #7', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

insert into spot(id, description, campsite_id)
values (8, 'Spot #8', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

insert into spot(id, description, campsite_id)
values (9, 'Spot #9', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

insert into spot(id, description, campsite_id)
values (10, 'Spot #10', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

insert into spot(id, description, campsite_id)
values (11, 'Spot #11', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

insert into spot(id, description, campsite_id)
values (12, 'Spot #12', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

insert into spot(id, description, campsite_id)
values (13, 'Spot #!3', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

insert into spot(id, description, campsite_id)
values (14, 'Spot #14', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

insert into spot(id, description, campsite_id)
values (15, 'Spot #15', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

insert into spot(id, description, campsite_id)
values (16, 'Spot #16', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

insert into spot(id, description, campsite_id)
values (17, 'Spot #17', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

insert into spot(id, description, campsite_id)
values (18, 'Spot #18', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

insert into spot(id, description, campsite_id)
values (19, 'Spot #19', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

insert into spot(id, description, campsite_id)
values (20, 'Spot #20', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

insert into spot(id, description, campsite_id)
values (21, 'Spot #21', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

insert into spot(id, description, campsite_id)
values (22, 'Spot #22', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

insert into spot(id, description, campsite_id)
values (23, 'Spot #23', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

insert into spot(id, description, campsite_id)
values (24, 'Spot #24', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

insert into spot(id, description, campsite_id)
values (25, 'Spot #25', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

insert into spot(id, description, campsite_id)
values (26, 'Spot #26', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

insert into spot(id, description, campsite_id)
values (27, 'Spot #27', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

insert into spot(id, description, campsite_id)
values (28, 'Spot #28', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

insert into spot(id, description, campsite_id)
values (29, 'Spot #29', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');

insert into spot(id, description, campsite_id)
values (30, 'Spot #29', '5f108bfc-47d6-4a75-8a52-ce79790c73d6');


