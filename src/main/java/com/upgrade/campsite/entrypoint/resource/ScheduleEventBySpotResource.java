package com.upgrade.campsite.entrypoint.resource;

import com.upgrade.campsite.core.entity.DateRange;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class ScheduleEventBySpotResource {

    private Long spotId;
    private List<DateRange> datesAvailable;
}
