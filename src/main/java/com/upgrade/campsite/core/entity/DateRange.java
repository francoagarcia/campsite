package com.upgrade.campsite.core.entity;

import lombok.Getter;

import java.time.LocalDate;
import java.time.Period;
import java.util.Objects;
import java.util.StringJoiner;

import static java.util.Objects.requireNonNull;

@Getter
public class DateRange implements Comparable<DateRange> {

    private LocalDate from;
    private LocalDate to;

    private DateRange(){}

    public static DateRangeBuilder builder(){
        return new DateRangeBuilder();
    }

    public boolean isImmediatelyPreviousOrIncludes(DateRange anotherDateRange){
        boolean isRangeIncluded = include(anotherDateRange);
        boolean isTheNextRange = isPreviousByDays(anotherDateRange, 0);
        return isRangeIncluded || isTheNextRange;
    }

    public boolean include(DateRange another){
        return another.getFrom().isBefore(this.to) && (another.getTo().isEqual(this.to) || another.getTo().isAfter(this.to));
    }

    public boolean isPreviousByDays(DateRange another, int daysBetween){
        return Period.between(this.to, another.getFrom()).equals(Period.ofDays(daysBetween));
    }

    /**
     * Compare this date range with another.
     * A date range is lesser than another if: 'this' is beforeOrIncludes the other
     *
     * @param o another date range
     * @return -1 if this < another; 0 if this == another; 1 if this > another
     */
    @Override
    public int compareTo(DateRange o) {
        if(this.equals(o)) return 0;
        if(this.isPrevious(o)) return -1;
        return 1;
    }

    public boolean isPrevious(DateRange another){
        boolean fromIsBeforeAndToIsLesserOrEquals = this.getFrom().isBefore(another.getFrom()) && (this.getTo().isBefore(another.getTo()) || this.getTo().isEqual(another.getTo()));
        boolean fromIsEqualsAndToIsLesser = this.getFrom().isEqual(another.getFrom()) && this.getTo().isBefore(another.getTo());
        return fromIsBeforeAndToIsLesserOrEquals || fromIsEqualsAndToIsLesser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DateRange dateRange = (DateRange) o;
        return from.equals(dateRange.from) && to.equals(dateRange.to);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", DateRange.class.getSimpleName() + "[", "]")
            .add("from=" + from)
            .add("to=" + to)
            .toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(from, to);
    }

    public static class DateRangeBuilder {
        private LocalDate from;
        private LocalDate to;

        public DateRangeBuilder from(LocalDate from){
            this.from = from;
            return this;
        }

        public DateRangeBuilder to(LocalDate to){
            this.to = to;
            return this;
        }

        public DateRange build(){
            requireNonNull(this.from, "from's Date Range cannot be null");
            requireNonNull(this.to, "to's Date Range cannot be null");

            DateRange dateRange = new DateRange();
            dateRange.from = this.from;
            dateRange.to = this.to;

            if(this.from.isAfter(this.to) || this.from.isEqual(this.to)){
                throw new IllegalArgumentException("from's Date Range must be greater than to's DateRange");
            }

            return dateRange;
        }
    }
}

