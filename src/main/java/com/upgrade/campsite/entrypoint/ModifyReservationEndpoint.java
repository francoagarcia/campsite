package com.upgrade.campsite.entrypoint;

import com.upgrade.campsite.entrypoint.resource.ReservationResource;
import lombok.Data;

import java.time.LocalDate;

/**
 * Entry point for the ModifyReservation use case.
 * Initialize the use case and return the presentation model.
 */
public interface ModifyReservationEndpoint {

    /**
     * Execute the use case and return the presentation model.
     *
     * @param campsiteId campsite id
     * @param reservationId obfuscated reservation id
     * @param reservationRequest modification request
     * @return reservation resource
     */
    ReservationResource execute(String campsiteId, String reservationId, ModifyReservationEndpoint.ModifyReservationRequest reservationRequest);

    @Data
    class ModifyReservationRequest {
        private String campsiteId;
        private String reservationId;
        private String email;
        private String fullname;
        private LocalDate arrivalDate;
        private LocalDate departureDate;
        private Integer requiredSpots;
    }
}
