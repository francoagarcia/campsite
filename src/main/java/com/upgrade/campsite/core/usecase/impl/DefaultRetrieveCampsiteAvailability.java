package com.upgrade.campsite.core.usecase.impl;

import com.upgrade.campsite.core.entity.*;
import com.upgrade.campsite.core.exception.NotFoundException;
import com.upgrade.campsite.core.exception.UseCaseException;
import com.upgrade.campsite.core.port.repository.CampsiteRepository;
import com.upgrade.campsite.core.port.repository.ReservationRepository;
import com.upgrade.campsite.core.port.repository.SpotRepository;
import com.upgrade.campsite.core.usecase.RetrieveCampsiteAvailability;

import javax.inject.Inject;
import java.time.LocalDate;

public class DefaultRetrieveCampsiteAvailability<EVENTENTITY extends ScheduleEvent> implements RetrieveCampsiteAvailability<EVENTENTITY> {

	private Model model;
	private CampsiteRepository campsiteRepository;
	private SpotRepository spotRepository;
	private ReservationRepository reservationRepository;

	@Inject
	public DefaultRetrieveCampsiteAvailability(CampsiteRepository campsiteRepository,
                                               SpotRepository spotRepository,
                                               ReservationRepository reservationRepository){
	    this.campsiteRepository = campsiteRepository;
	    this.spotRepository = spotRepository;
	    this.reservationRepository = reservationRepository;
	}

	@Override
	public CampsiteAvailabilitySchedule<EVENTENTITY> get() {
	    // Load data
        Campsite campsite = this.campsiteRepository
            .findOne(this.model.getCampsiteId())
            .orElseThrow(NotFoundException.campsite());
        campsite.setSpots( this.spotRepository.findForCampsite(campsite) );
        campsite.setReservations( this.reservationRepository.findActiveReservations(campsite, this.model.getDateRange()) );

        // Business rules execution
        CampsiteAvailabilitySchedule<EVENTENTITY> campsiteAvailabilitySchedule = campsite.getAvailabilitySchedule(
            this.model.getDateFrom(),
            this.model.getDateTo(),
            this.model.getRequiredSpots(),
            this.model.getReservationDays(),
            this.model.getScheduleType());

        // Response
		return campsiteAvailabilitySchedule;
	}

	@Override
	public void setModel(Model model) {
	    this.setDefaults(model);
		this.validateModel(model);
		this.model = model;
	}

    /**
     * Set defaults for the request model
     * @param model
     */
	public void setDefaults(Model model){
	    // Default campsite
        if(model.getCampsiteId() == null){
            model.setCampsiteId(Campsite.CAMPSITE_ID);
        }

        // Defaults for spots_required
	    if(model.getRequiredSpots() == null){
            model.setRequiredSpots(1);
        }

        // Defaults for days
        if(model.getReservationDays() == null){
            model.setReservationDays(1);
        }

        // Default campsite
        if(model.getScheduleType() == null){
            model.setScheduleType(ScheduleType.BY_SPOT);
        }

	    // Defaults for dates
        LocalDate dateFrom = model.getDateFrom();
        LocalDate dateTo = model.getDateTo();
        if(dateFrom == null && dateTo != null){
            dateFrom = dateTo.minusMonths(1);
        }
        if(dateFrom != null && dateTo == null){
            dateTo = dateFrom.plusMonths(1);
        }
        if(dateFrom == null && dateTo == null){
            dateFrom = LocalDate.now();
            dateTo = LocalDate.now().plusMonths(1);
        }
        model.setDateFrom(dateFrom);
        model.setDateTo(dateTo);
    }

    /**
     * Validates RequestModel and throw an UseCaseException if necessary
     * @param model requestModel
     */
    public void validateModel(Model model){
        if(!model.getDateFrom().isBefore(model.getDateTo())){
            throw new UseCaseException(MessageId.INVALID_DATE_RANGE);
        }
        if(model.getRequiredSpots() <= 0){
            throw new UseCaseException(MessageId.INVALID_SPOTS_NUMBER);
        }
        if(model.getReservationDays() <= 0){
            throw new UseCaseException(MessageId.INVALID_RESERVATION_DAYS);
        }
	}
}
