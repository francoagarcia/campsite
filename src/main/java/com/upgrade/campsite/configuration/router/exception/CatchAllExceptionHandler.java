package com.upgrade.campsite.configuration.router.exception;

import com.upgrade.campsite.core.entity.MessageId;
import com.upgrade.campsite.core.port.util.I18n;
import com.upgrade.campsite.entrypoint.resource.ErrorApi;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.inject.Inject;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@ControllerAdvice
public class CatchAllExceptionHandler {

    private static final Logger logger = Logger.getLogger(CatchAllExceptionHandler.class.getName());
    private I18n i18n;

    @Inject
    public CatchAllExceptionHandler(I18n i18n) {
        this.i18n = i18n;
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorApi> handle(Exception exception, Locale locale) {
        HttpStatus internalServerError = INTERNAL_SERVER_ERROR;

        String message = ObjectUtils.firstNonNull(i18n.tr(MessageId.INTERNAL_SERVER_ERROR.getId(), locale), exception.getMessage());

        String detail = String.format("[%s] - %s", exception.getClass().getName(), exception.getMessage());

        ErrorApi response = ErrorApi.builder()
            .message(message)
            .detail(detail)
            .status(internalServerError.value())
            .build();

        logger.log(Level.WARNING, detail);

        return ResponseEntity
            .status(internalServerError)
            .body(response);
    }
}
