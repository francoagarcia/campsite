package com.upgrade.campsite.core.entity;

public enum ReservationStatus {
    PENDING,    // when the camper make the reservation
    CONFIRMED,  // when the camper checks in
    CANCELLED,  // when the camper cancels the reservation
    FINALIZED  // when the camper checks out
    ;

    @Override
    public String toString() {
        return this.name().toLowerCase();
    }
}
