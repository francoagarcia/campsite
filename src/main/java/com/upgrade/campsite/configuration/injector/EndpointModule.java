package com.upgrade.campsite.configuration.injector;

import com.upgrade.campsite.core.entity.ScheduleEventByRange;
import com.upgrade.campsite.core.entity.ScheduleEventBySpot;
import com.upgrade.campsite.core.port.util.HashingHelper;
import com.upgrade.campsite.core.usecase.*;
import com.upgrade.campsite.entrypoint.*;
import com.upgrade.campsite.entrypoint.impl.*;
import com.upgrade.campsite.entrypoint.resource.ScheduleEventByRangeResource;
import com.upgrade.campsite.entrypoint.resource.ScheduleEventBySpotResource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import javax.inject.Named;

import static org.springframework.beans.factory.config.BeanDefinition.SCOPE_PROTOTYPE;

@Configuration
public class EndpointModule {

	@Bean
	@Scope(SCOPE_PROTOTYPE)
	public RetrieveCamperByIdEndpoint retrieveCamperByIdEndpoint(RetrieveCamperById retrieveCamperById) {
		return new DefaultRetrieveCamperByIdEndpoint(retrieveCamperById);
	}

    @Bean
    @Scope(SCOPE_PROTOTYPE)
    @Named("retrieveCampsiteIsAvailableEndpointByRange")
    public RetrieveCampsiteAvailabilityEndpoint<ScheduleEventByRangeResource> retrieveCampsiteIsAvailableEndpointByRange(
        RetrieveCampsiteAvailability<ScheduleEventByRange> retrieveCampsiteAvailability)
    {
        return new DefaultRetrieveCampsiteAvailabilityEndpoint<ScheduleEventByRange, ScheduleEventByRangeResource>(retrieveCampsiteAvailability);
    }

    @Bean
    @Scope(SCOPE_PROTOTYPE)
    @Named("retrieveCampsiteIsAvailableEndpointBySpot")
    public RetrieveCampsiteAvailabilityEndpoint<ScheduleEventBySpotResource> retrieveCampsiteIsAvailableEndpointBySpot(
        RetrieveCampsiteAvailability<ScheduleEventBySpot> retrieveCampsiteAvailability)
    {
        return new DefaultRetrieveCampsiteAvailabilityEndpoint<ScheduleEventBySpot, ScheduleEventBySpotResource>(retrieveCampsiteAvailability);
    }

    @Bean
    @Scope(SCOPE_PROTOTYPE)
    public ReserveCampsiteSpotsEndpoint reserveCampsiteSpotsEndpoint(ReserveCampsiteSpots reserveCampsiteSpots, HashingHelper hashingHelper) {
        return new DefaultReserveCampsiteSpotsEndpoint(reserveCampsiteSpots, hashingHelper);
    }

    @Bean
    @Scope(SCOPE_PROTOTYPE)
    public CancelReservationEndpoint cancelReservationEndpoint(CancelReservation cancelReservation, HashingHelper hashingHelper) {
        return new DefaultCancelReservationEndpoint(cancelReservation, hashingHelper);
    }

    @Bean
    @Scope(SCOPE_PROTOTYPE)
    public ModifyReservationEndpoint modifyReservationEndpoint(ModifyReservation modifyReservation, HashingHelper hashingHelper) {
        return new DefaultModifyReservationEndpoint(modifyReservation, hashingHelper);
    }
}
