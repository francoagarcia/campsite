package com.upgrade.campsite.core.usecase;

import com.upgrade.campsite.core.entity.DateRange;
import com.upgrade.campsite.core.entity.Reservation;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.function.Supplier;

/**
 * Reserve a number of spots for the given campsite.
 * Given an arrival & departure date, number of spots and camper data, creates a Reservation for the campsite.
 * Returns the reservation ID recently created.
 *
 * Throws UseCaseException if any constraint is violated.
 * Throws NotFoundException if any required entity is not found.
 */
public interface ReserveCampsiteSpots extends Supplier<Reservation> {

    /**
     * Initialize the use case.
     *
     * @param campsiteId Campsite for which the reservation was made.
     * @param fullname camper's full name
     * @param email camper's email
     * @param arrivalDate arrival date
     * @param departureDate departure date
     * @param requiredSpots number of required spots
     * @return ReserveCampsiteSpots instance initialized
     */
	default ReserveCampsiteSpots initialize(String campsiteId, String fullname, String email, LocalDate arrivalDate, LocalDate departureDate, Integer requiredSpots){
		this.setModel(new Model(campsiteId, fullname, email, arrivalDate, departureDate, requiredSpots));
		return this;
	}

    /**
     * Set & validate the request model handled by the Use Case.
     *
     * @param model request model
     */
	void setModel(Model model);

    /**
     * Request model
     */
    @Getter
    @Setter
	class Model {
	    private String campsiteId;
		private String fullname;
		private String email;
        private LocalDate arrivalDate;
        private LocalDate departureDate;
        private Integer requiredSpots;

        public Model(String campsiteId, String fullname, String email, LocalDate arrivalDate, LocalDate departureDate, Integer requiredSpots) {
            this.campsiteId = campsiteId;
            this.fullname = fullname;
            this.email = email;
            this.arrivalDate = arrivalDate;
            this.departureDate = departureDate;
            this.requiredSpots = requiredSpots;
        }

        public DateRange getDateRange(){
            return DateRange.builder().from(this.arrivalDate).to(this.departureDate).build();
        }
    }
}
