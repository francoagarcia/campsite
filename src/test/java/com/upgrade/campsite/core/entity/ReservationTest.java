package com.upgrade.campsite.core.entity;

import com.upgrade.campsite.core.exception.UseCaseException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

class ReservationTest {

    private Reservation reservation;

    @BeforeEach
    void setUp() {
        this.reservation = Mockito.spy(
            Reservation.builder()
                .id(1L)
                .spots( Arrays.asList(
                    new Spot(1L),
                    new Spot(2L))
                )
                .arrivalDate( LocalDate.of(2019, 2, 15) )
                .departureDate( LocalDate.of(2019, 2, 18) )
                .status(ReservationStatus.PENDING)
                .camper(Camper.builder().email("test_1@gmail.com").build())
                .build()
        );

    }

    @AfterEach
    void tearDown() {
    }

    /*
     * Tests for Reservation#collapseWithAnotherReservation
     */
    @ParameterizedTest
    @CsvSource({
        "2019-02-16, 2019-02-17, true",
        "2019-02-16, 2019-02-20, true",
        "2019-02-14, 2019-02-17, true",
        "2019-02-16, 2019-02-18, true",
        "2019-02-15, 2019-02-19, true",
        "2019-02-10, 2019-02-13, false",
    })
    void test_collapseWithAnotherReservation(String dateFromStr, String dateToStr, Boolean expected) {
        //given
        LocalDate dateFrom = LocalDate.parse(dateFromStr);
        LocalDate dateTo = LocalDate.parse(dateToStr);

        //when
        Boolean result = reservation.collapseWithAnotherReservation(dateFrom, dateTo);

        //then
        assertNotNull( result );
        assertEquals( result, expected );
    }

    /*
     * Tests for Reservation#isActive
     */
    @ParameterizedTest
    @CsvSource({
        "PENDING, true",
        "CONFIRMED, true",
        "CANCELLED, false",
        "FINALIZED, false",
    })
    void test_isActive(String status, Boolean expected){
        //given
        reservation.setStatus(ReservationStatus.valueOf(status));

        //when
        Boolean result = reservation.isActive();

        //then
        assertNotNull( result );
        assertEquals( result, expected );
    }

    /*
     * Tests for Reservation#isOwnedBy
     */
    @ParameterizedTest
    @CsvSource({
        "test_1@gmail.com, true",
        "test_XXXXX@gmail.com, false",
    })
    void test_isOwnedBy(String email, Boolean expected){
        //given
        Camper camper = Camper.builder().email(email).build();

        //when
        Boolean result = reservation.isOwnedBy(camper);

        //then
        assertNotNull( result );
        assertEquals( result, expected );
    }

    /*
     * Tests for Reservation#isActiveAndOwnedBy
     */
    @Test
    void test_isActiveAndOwnedBy_ok(){
        //given
        Camper camper = Camper.builder().email("some_email@gmail.com").build();

        doReturn(Boolean.TRUE).when(reservation).isActive();
        doReturn(Boolean.TRUE).when(reservation).isOwnedBy( any());

        //when
        Boolean result = reservation.isActiveAndOwnedBy(camper);

        //then
        assertNotNull( result );
        assertTrue( result );
    }

    @Test
    void test_isActiveAndOwnedBy_notActive(){
        //given
        Camper camper = Camper.builder().email("some_email@gmail.com").build();

        doReturn(Boolean.FALSE).when(reservation).isActive();
        doReturn(Boolean.TRUE).when(reservation).isOwnedBy( any());

        //when
        Boolean result = reservation.isActiveAndOwnedBy(camper);

        //then
        assertNotNull( result );
        assertFalse( result );
    }

    @Test
    void test_isActiveAndOwnedBy_notOwnedByCamper(){
        //given
        Camper camper = Camper.builder().email("some_email@gmail.com").build();

        doReturn(Boolean.TRUE).when(reservation).isActive();
        doReturn(Boolean.FALSE).when(reservation).isOwnedBy( any());

        //when
        Boolean result = reservation.isActiveAndOwnedBy(camper);

        //then
        assertNotNull( result );
        assertFalse( result );
    }

    /*
     * Tests for Reservation#isActiveAndOwnedBy
     */
    @Test
    void test_cancel_ok(){
        //given
        doReturn(Boolean.TRUE).when(reservation).isActive();

        //then
        assertDoesNotThrow(() -> {
            reservation.cancel();
        });
        assertEquals(ReservationStatus.CANCELLED, reservation.getStatus());
    }

    @Test
    void test_cancel_throwsException(){
        //given
        doReturn(Boolean.FALSE).when(reservation).isActive();

        //then
        UseCaseException ex = assertThrows(UseCaseException.class, () -> {
            reservation.cancel();
        });
        assertEquals(MessageId.RESERVATION_IS_NOT_ACTIVE.getId(), ex.getMessageId());
    }
}
