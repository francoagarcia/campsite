package com.upgrade.campsite.entrypoint;

import com.upgrade.campsite.core.entity.ScheduleType;
import com.upgrade.campsite.entrypoint.resource.CampsiteAvailabilityResource;

import java.time.LocalDate;

/**
 * Entry point for the RetrieveCampsiteAvailability use case.
 * Initialize the use case and return the presentation model.
 */
public interface RetrieveCampsiteAvailabilityEndpoint<RESOURCE> {

    /**
     * Execute the use case and return the presentation model.
     *
     * @param campsiteId campsite id
     * @param dateFrom arrival date
     * @param dateTo departure date
     * @param requiredSpots number of required spots
     * @param reservationDays number of days for the desired reservation
     * @return campsite schedule resource
     */
    CampsiteAvailabilityResource<RESOURCE> execute(String campsiteId, LocalDate dateFrom, LocalDate dateTo, Integer requiredSpots, Integer reservationDays, ScheduleType scheduleType);

}
