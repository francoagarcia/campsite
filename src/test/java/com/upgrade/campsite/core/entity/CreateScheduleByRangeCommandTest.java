package com.upgrade.campsite.core.entity;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

class CreateScheduleByRangeCommandTest {

    private CreateScheduleByRangeCommand command;
    private Campsite campsite;

    @BeforeEach
    void setUp() {
        this.command = new CreateScheduleByRangeCommand();
        this.campsite = Mockito.mock(Campsite.class);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void test_execute_oneDayOfTheReservation_allSpotsAvailableInAllDays() {
        //given
        LocalDate dateFrom = LocalDate.of(2019, 3, 1);
        LocalDate dateTo = LocalDate.of(2019, 3, 5);
        Integer requiredSpots = 1;
        Integer amountDaysForTheReservation = 1;

        doReturn(new ArrayList<>(Arrays.asList(new Spot(1L), new Spot(2L))))
            .when(campsite)
            .getFreeSpots(LocalDate.parse("2019-03-01"), LocalDate.parse("2019-03-02"));

        doReturn(new ArrayList<>(Arrays.asList(new Spot(1L), new Spot(2L))))
            .when(campsite)
            .getFreeSpots(LocalDate.parse("2019-03-02"), LocalDate.parse("2019-03-03"));

        doReturn(new ArrayList<>(Arrays.asList(new Spot(1L), new Spot(2L))))
            .when(campsite)
            .getFreeSpots(LocalDate.parse("2019-03-03"), LocalDate.parse("2019-03-04"));

        doReturn(new ArrayList<>(Arrays.asList(new Spot(1L), new Spot(2L))))
            .when(campsite)
            .getFreeSpots(LocalDate.parse("2019-03-04"), LocalDate.parse("2019-03-05"));

        doReturn(new ArrayList<>(Arrays.asList(new Spot(1L), new Spot(2L))))
            .when(campsite)
            .getFreeSpots(LocalDate.parse("2019-03-05"), LocalDate.parse("2019-03-06"));

        ScheduleEventsHandler handler = new ScheduleEventsHandler(dateFrom, dateTo, requiredSpots, amountDaysForTheReservation, ScheduleType.BY_RANGE);
        handler.setCampsite(campsite);

        //when
        List<ScheduleEventByRange> events = command.execute(handler);

        //then
        assertNotNull(events);
        assertEquals(4, events.size());
        assertEquals(8, events.stream().map(ev -> new ArrayList<>(ev.getFreeSpots())).mapToLong(Collection::size).sum() );
    }

    @Test
    void test_execute_oneDayOfTheReservation_someDaysNotAvailable_someSpotsReserved() {
        //given
        LocalDate dateFrom = LocalDate.of(2019, 3, 1);
        LocalDate dateTo = LocalDate.of(2019, 3, 5);
        Integer requiredSpots = 1;
        Integer amountDaysForTheReservation = 1;

        doReturn(new ArrayList<>(Arrays.asList(new Spot(1L))))
            .when(campsite)
            .getFreeSpots(LocalDate.parse("2019-03-01"), LocalDate.parse("2019-03-02"));

        doReturn(Collections.emptyList())
            .when(campsite)
            .getFreeSpots(LocalDate.parse("2019-03-02"), LocalDate.parse("2019-03-03"));

        doReturn(Collections.emptyList())
            .when(campsite)
            .getFreeSpots(LocalDate.parse("2019-03-03"), LocalDate.parse("2019-03-04"));

        doReturn(new ArrayList<>(Arrays.asList(new Spot(2L))))
            .when(campsite)
            .getFreeSpots(LocalDate.parse("2019-03-04"), LocalDate.parse("2019-03-05"));

        doReturn(new ArrayList<>(Arrays.asList(new Spot(1L), new Spot(2L))))
            .when(campsite)
            .getFreeSpots(LocalDate.parse("2019-03-05"), LocalDate.parse("2019-03-06"));

        ScheduleEventsHandler handler = new ScheduleEventsHandler(dateFrom, dateTo, requiredSpots, amountDaysForTheReservation, ScheduleType.BY_RANGE);
        handler.setCampsite(campsite);

        //when
        List<ScheduleEventByRange> events = command.execute(handler);

        //then
        assertNotNull(events);
        assertEquals(2, events.size());
        assertEquals(2, events.stream().map(ev -> new ArrayList<>(ev.getFreeSpots())).mapToLong(Collection::size).sum() );
    }

    @Test
    void test_execute_campsiteHasNoSpace() {
        //given
        LocalDate dateFrom = LocalDate.of(2019, 3, 1);
        LocalDate dateTo = LocalDate.of(2019, 3, 5);
        Integer requiredSpots = 1;
        Integer amountDaysForTheReservation = 1;

        doReturn(Collections.emptyList())
            .when(campsite)
            .getFreeSpots(any(), any());

        ScheduleEventsHandler handler = new ScheduleEventsHandler(dateFrom, dateTo, requiredSpots, amountDaysForTheReservation, ScheduleType.BY_RANGE);
        handler.setCampsite(campsite);

        //when
        List<ScheduleEventByRange> events = command.execute(handler);

        //then
        assertNotNull(events);
        assertEquals(0, events.size());
    }

    @Test
    void test_execute_twoDaysOfTheReservation_allSpotsAvailableInAllDays() {
        //given
        LocalDate dateFrom = LocalDate.of(2019, 3, 1);
        LocalDate dateTo = LocalDate.of(2019, 3, 5);
        Integer requiredSpots = 1;
        Integer amountDaysForTheReservation = 2;

        doReturn(new ArrayList<>(Arrays.asList(new Spot(1L), new Spot(2L))))
            .when(campsite)
            .getFreeSpots(LocalDate.parse("2019-03-01"), LocalDate.parse("2019-03-03"));

        doReturn(new ArrayList<>(Arrays.asList(new Spot(1L), new Spot(2L))))
            .when(campsite)
            .getFreeSpots(LocalDate.parse("2019-03-02"), LocalDate.parse("2019-03-04"));

        doReturn(new ArrayList<>(Arrays.asList(new Spot(1L), new Spot(2L))))
            .when(campsite)
            .getFreeSpots(LocalDate.parse("2019-03-03"), LocalDate.parse("2019-03-05"));

        doReturn(new ArrayList<>(Arrays.asList(new Spot(1L), new Spot(2L))))
            .when(campsite)
            .getFreeSpots(LocalDate.parse("2019-03-04"), LocalDate.parse("2019-03-06"));

        doReturn(new ArrayList<>(Arrays.asList(new Spot(1L), new Spot(2L))))
            .when(campsite)
            .getFreeSpots(LocalDate.parse("2019-03-05"), LocalDate.parse("2019-03-07"));

        ScheduleEventsHandler handler = new ScheduleEventsHandler(dateFrom, dateTo, requiredSpots, amountDaysForTheReservation, ScheduleType.BY_RANGE);
        handler.setCampsite(campsite);

        //when
        List<ScheduleEventByRange> events = command.execute(handler);

        //then
        assertNotNull(events);
        assertEquals(3, events.size());
        assertEquals(6, events.stream().map(ev -> new ArrayList<>(ev.getFreeSpots())).mapToLong(Collection::size).sum() );
    }

    @Test
    void test_execute_fiveDaysOfTheReservation_allSpotsAvailableInAllDays() {
        //given
        LocalDate dateFrom = LocalDate.of(2019, 3, 1);
        LocalDate dateTo = LocalDate.of(2019, 3, 5);
        Integer requiredSpots = 1;
        Integer amountDaysForTheReservation = 5;

        doReturn(new ArrayList<>(Arrays.asList(new Spot(1L), new Spot(2L))))
            .when(campsite)
            .getFreeSpots(LocalDate.parse("2019-03-01"), LocalDate.parse("2019-03-05"));

        doReturn(new ArrayList<>(Arrays.asList(new Spot(1L), new Spot(2L))))
            .when(campsite)
            .getFreeSpots(LocalDate.parse("2019-03-02"), LocalDate.parse("2019-03-07"));

        doReturn(new ArrayList<>(Arrays.asList(new Spot(1L), new Spot(2L))))
            .when(campsite)
            .getFreeSpots(LocalDate.parse("2019-03-03"), LocalDate.parse("2019-03-08"));

        doReturn(new ArrayList<>(Arrays.asList(new Spot(1L), new Spot(2L))))
            .when(campsite)
            .getFreeSpots(LocalDate.parse("2019-03-04"), LocalDate.parse("2019-03-09"));

        doReturn(new ArrayList<>(Arrays.asList(new Spot(1L), new Spot(2L))))
            .when(campsite)
            .getFreeSpots(LocalDate.parse("2019-03-05"), LocalDate.parse("2019-03-10"));

        ScheduleEventsHandler handler = new ScheduleEventsHandler(dateFrom, dateTo, requiredSpots, amountDaysForTheReservation, ScheduleType.BY_RANGE);
        handler.setCampsite(campsite);

        //when
        List<ScheduleEventByRange> events = command.execute(handler);

        //then
        assertNotNull(events);
        assertEquals(0, events.size());
    }
}
