package com.upgrade.campsite.core.exception;

import com.upgrade.campsite.core.entity.MessageId;
import org.springframework.http.HttpStatus;

import java.util.function.Supplier;

public class NotFoundException extends UseCaseException {

    public NotFoundException(MessageId messageId) {
        super(messageId);
        super.statusCode = HttpStatus.NOT_FOUND.value();
    }

    public static Supplier<NotFoundException> camper(){
        return () -> new NotFoundException(MessageId.CAMPER_NOT_FOUND);
    }
    public static Supplier<NotFoundException> campsite(){
        return () -> new NotFoundException(MessageId.CAMPSITE_NOT_FOUND);
    }
    public static Supplier<NotFoundException> reservation(){
        return () -> new NotFoundException(MessageId.RESERVATION_NOT_FOUND);
    }
}
