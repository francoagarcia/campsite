package com.upgrade.campsite.core.usecase.impl;

import com.upgrade.campsite.core.entity.Camper;
import com.upgrade.campsite.core.exception.NotFoundException;
import com.upgrade.campsite.core.exception.UseCaseException;
import com.upgrade.campsite.core.port.repository.CamperRepository;
import com.upgrade.campsite.core.usecase.RetrieveCamperById;

import javax.inject.Inject;

import static com.upgrade.campsite.core.entity.MessageId.CAMPER_ID_REQUIRED;

public class DefaultRetrieveCamperById implements RetrieveCamperById {

	private Model model;
	private CamperRepository camperRepository;

	@Inject
	public DefaultRetrieveCamperById(CamperRepository camperRepository){
		this.camperRepository = camperRepository;
	}

	@Override
	public Camper get() {
		Camper camper = this.camperRepository
            .findOne(this.model.getId())
            .orElseThrow(NotFoundException.camper());

		return camper;
	}

	@Override
	public void setModel(Model model) {
		this.validateModel(model);
		this.model = model;
	}

    /**
     * Validates RequestModel and throw an UseCaseException if necessary
     * @param model requestModel
     */
	private void validateModel(Model model){
		if (model.getId() == null) {
			throw new UseCaseException(CAMPER_ID_REQUIRED);
		}
	}

}
