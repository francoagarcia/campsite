package com.upgrade.campsite.entrypoint.impl;

import com.upgrade.campsite.core.port.util.HashingHelper;
import com.upgrade.campsite.core.usecase.ReserveCampsiteSpots;
import com.upgrade.campsite.entrypoint.ReserveCampsiteSpotsEndpoint;
import com.upgrade.campsite.entrypoint.resource.ReservationResource;

import javax.inject.Inject;
import java.time.LocalDate;

public class DefaultReserveCampsiteSpotsEndpoint implements ReserveCampsiteSpotsEndpoint {

    private ReserveCampsiteSpots reserveCampsiteSpots;
    private HashingHelper hashingHelper;

    @Inject
    public DefaultReserveCampsiteSpotsEndpoint(ReserveCampsiteSpots reserveCampsiteSpots,
                                               HashingHelper hashingHelper) {
        this.reserveCampsiteSpots = reserveCampsiteSpots;
        this.hashingHelper = hashingHelper;
    }

    @Override
    public ReservationResource execute(String campsiteId, ReservationRequest reservationRequest) {
        String email = reservationRequest.getEmail();
        String fullname = reservationRequest.getFullname();
        LocalDate arrivalDate = reservationRequest.getArrivalDate();
        LocalDate departureDate = reservationRequest.getDepartureDate();
        Integer requiredSpots = reservationRequest.getRequiredSpots();

        var reservation = reserveCampsiteSpots.initialize(campsiteId, fullname, email, arrivalDate, departureDate, requiredSpots).get();

        String bookingId = hashingHelper.obfuscate(reservation.getBookingNumber());

        return new ReservationResource(bookingId);
    }
}
