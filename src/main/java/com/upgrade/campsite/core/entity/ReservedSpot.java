package com.upgrade.campsite.core.entity;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Objects;

@Getter
@Setter
public class ReservedSpot {

    private Spot spot;
    private LocalDateTime checkIn;
    private LocalDateTime checkOut;

    public ReservedSpot() { }

    public ReservedSpot(Spot spot) {
        this.spot = spot;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReservedSpot that = (ReservedSpot) o;
        return Objects.equals(spot, that.spot) &&
            Objects.equals(checkIn, that.checkIn) &&
            Objects.equals(checkOut, that.checkOut);
    }

    @Override
    public int hashCode() {
        return Objects.hash(spot, checkIn, checkOut);
    }
}
