package com.upgrade.campsite.configuration.router.exception;

import com.upgrade.campsite.core.entity.MessageId;
import com.upgrade.campsite.core.port.util.I18n;
import com.upgrade.campsite.entrypoint.resource.ErrorApi;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;

@ControllerAdvice
@Order(HIGHEST_PRECEDENCE)
public class ValidationExceptionHandler {

    private static final Logger logger = Logger.getLogger(ValidationExceptionHandler.class.getName());
    private I18n i18n;

    @Inject
    public ValidationExceptionHandler(I18n i18n) {
        this.i18n = i18n;
    }

    /**
     * A HttpMessageNotReadableException is thrown when cannot deserialize the body.
     */
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<ErrorApi> onHttpMessageNotReadble(HttpMessageNotReadableException exception, Locale locale) {
        HttpStatus unprocessableEntity = UNPROCESSABLE_ENTITY;

        String message = ObjectUtils.firstNonNull(i18n.tr(MessageId.UNPROCESSABLE_ENTITY_ERROR.getId(), locale), exception.getMessage());

        String detail = String.format("[%s] - %s", exception.getClass().getName(), exception.getMessage());

        ErrorApi response = ErrorApi.builder()
            .message(message)
            .detail(detail)
            .status(unprocessableEntity.value())
            .build();

        logger.log(Level.SEVERE, detail);

        return ResponseEntity
            .status(unprocessableEntity)
            .body(response);
    }

    /**
     * A MethodArgumentTypeMismatchException is raised while resolving a controller method argument.
     */
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<ErrorApi> onMethodArgTypeMismatch(MethodArgumentTypeMismatchException e, Locale locale) {
        HttpStatus unprocessableEntity = UNPROCESSABLE_ENTITY;

        Object[] messageArgs = {e.getName(), e.getValue(), e.getRequiredType().getSimpleName()};
        String message = ObjectUtils.firstNonNull(i18n.tr(MessageId.UNPROCESSABLE_ENTITY_METHOD_ARG_ERROR.getId(), messageArgs, locale), e.getMessage());

        String detail = String.format("[%s] - %s", e.getClass().getName(), e.getMessage());

        ErrorApi response = ErrorApi.builder()
            .message(message)
            .detail(detail)
            .status(unprocessableEntity.value())
            .build();

        logger.log(Level.SEVERE, detail);

        return ResponseEntity
            .status(unprocessableEntity)
            .body(response);
    }

    /**
     * A MethodArgumentNotValidException is thrown when validation on an argument annotated with {@code @Valid} fails.
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorApi> onMethodArgumentNotValid(MethodArgumentNotValidException exception, Locale locale) {
        HttpStatus badRequest = BAD_REQUEST;

        BindingResult bindingResult = exception.getBindingResult();

        String message = this.getMessageFromBindingResult(bindingResult, locale);

        String detail = String.format("[%s] - %s", exception.getClass().getName(), exception.getMessage());

        ErrorApi response = ErrorApi.builder()
            .message(message)
            .detail(detail)
            .status(badRequest.value())
            .build();

        logger.log(Level.INFO, detail);

        return ResponseEntity
            .status(badRequest)
            .body(response);
    }

    /**
     * A ConstraintViolationException is thrown when a check from the javax.validation package fails.
     */
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<?> onConstraintViolation(ConstraintViolationException exception, Locale locale) {
        HttpStatus badRequest = BAD_REQUEST;

        var constraintViolations = exception.getConstraintViolations();

        String message = this.getMessageFromConstraintViolation(constraintViolations , locale);

        String detail = String.format("[%s] - %s", exception.getClass().getName(), exception.getMessage());

        ErrorApi response = ErrorApi.builder()
            .message(message)
            .detail(detail)
            .status(badRequest.value())
            .build();

        logger.log(Level.INFO, detail);

        return ResponseEntity
            .status(badRequest)
            .body(response);
    }

    public String getMessageFromBindingResult(BindingResult bindingResult, Locale locale) {
        requireNonNull(bindingResult, "Can't build from a response without validation results");

        List<String> errors = bindingResult.getAllErrors()
            .stream()
            .map(e -> getErrorMessage(e.getDefaultMessage(), e.getArguments(), locale))
            .collect(toList());

        return String.join(", ", errors);
    }

    public String getMessageFromConstraintViolation(Set<ConstraintViolation<?>> violations, Locale locale) {
        requireNonNull(violations, "Can't build from null violations");

        List<String> messages = violations.stream()
            .map(v -> getErrorMessage(v.getMessage(), v.getExecutableParameters(), locale))
            .collect(toList());

        return String.join(", ", messages);
    }

    private String getErrorMessage(String message, Object[] executableParameters, Locale locale) {
        return ObjectUtils.firstNonNull(i18n.tr(message, executableParameters, locale), message);
    }
}
