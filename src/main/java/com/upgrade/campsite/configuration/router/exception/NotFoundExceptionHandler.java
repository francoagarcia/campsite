package com.upgrade.campsite.configuration.router.exception;

import com.upgrade.campsite.core.entity.MessageId;
import com.upgrade.campsite.core.exception.NotFoundException;
import com.upgrade.campsite.core.port.util.I18n;
import com.upgrade.campsite.entrypoint.resource.ErrorApi;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.inject.Inject;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;

@ControllerAdvice
@Order(HIGHEST_PRECEDENCE)
public class NotFoundExceptionHandler {

    private static final Logger logger = Logger.getLogger(UseCaseExceptionHandler.class.getName());
    private I18n i18n;

    @Inject
    public NotFoundExceptionHandler(I18n i18n) {
        this.i18n = i18n;
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorApi> handleNotFoundException(NotFoundException e, Locale locale) {
        HttpStatus notFound = HttpStatus.NOT_FOUND;

        String message = ObjectUtils.firstNonNull(i18n.tr(e.getMessageId(), e.getArgs(), locale), e.getMessage());

        String detail = String.format("[%s] - %s", e.getClass().getName(), e.getMessage());

        ErrorApi response = ErrorApi.builder()
            .message(message)
            .detail(detail)
            .status(notFound.value())
            .build();

        logger.log(Level.INFO, String.format("[%s] - %s", response.getTimestamp(), message));

        return ResponseEntity
            .status(notFound)
            .body(response);
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    public ResponseEntity<ErrorApi> handleNoHandlerFoundException(NoHandlerFoundException e, Locale locale) {
        HttpStatus notFound = HttpStatus.NOT_FOUND;

        String message = ObjectUtils.firstNonNull(i18n.tr(MessageId.RESOURCE_NOT_FOUND.getId(), locale), e.getMessage());

        String detail = String.format("[%s] - %s", e.getClass().getName(), e.getMessage());

        ErrorApi response = ErrorApi.builder()
            .message(message)
            .detail(detail)
            .status(notFound.value())
            .build();

        logger.log(Level.WARNING, String.format("[%s] - %s", response.getTimestamp(), message));

        return ResponseEntity
            .status(notFound)
            .body(response);
    }
}
