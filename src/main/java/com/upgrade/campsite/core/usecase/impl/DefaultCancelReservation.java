package com.upgrade.campsite.core.usecase.impl;

import com.upgrade.campsite.core.entity.Campsite;
import com.upgrade.campsite.core.entity.Reservation;
import com.upgrade.campsite.core.exception.NotFoundException;
import com.upgrade.campsite.core.exception.UseCaseException;
import com.upgrade.campsite.core.port.repository.ReservationRepository;
import com.upgrade.campsite.core.usecase.CancelReservation;

import javax.inject.Inject;

import static com.upgrade.campsite.core.entity.MessageId.RESERVATION_ID_REQUIRED;

public class DefaultCancelReservation implements CancelReservation {

	private Model model;
	private ReservationRepository reservationRepository;

	@Inject
	public DefaultCancelReservation(ReservationRepository reservationRepository){
		this.reservationRepository = reservationRepository;
	}

    @Override
    public void execute() {
	    Reservation reservation = this.reservationRepository
            .findOne(this.model.getReservationId())
            .orElseThrow(NotFoundException.reservation());

	    reservation.cancel();

	    this.reservationRepository.update(reservation);
    }

    @Override
	public void setModel(Model model) {
	    this.setDefaults(model);
		this.validateModel(model);
		this.model = model;
	}

    /**
     * Set defaults for the request model
     * @param model
     */
    public void setDefaults(Model model) {
        if (model.getCampsiteId() == null) {
            model.setCampsiteId(Campsite.CAMPSITE_ID);
        }
    }

    /**
     * Validates RequestModel and throw an UseCaseException if necessary
     * @param model requestModel
     */
    public void validateModel(Model model){
		if (model.getReservationId() == null) {
			throw new UseCaseException(RESERVATION_ID_REQUIRED);
		}
	}

}
