package com.upgrade.campsite.configuration.injector;

import com.upgrade.campsite.adapter.dataprovider.entity.*;
import com.upgrade.campsite.adapter.dataprovider.repository.impl.*;
import com.upgrade.campsite.core.port.repository.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import static org.springframework.beans.factory.config.BeanDefinition.SCOPE_SINGLETON;

@Configuration
public class RepositoryModule {

    @Inject
    private EntityManager em;

	@Bean
	@Scope(SCOPE_SINGLETON)
	public CamperRepository defaultCamperRepository() {
		return new DefaultCamperRepository(CamperDb.class, em);
	}

    @Bean
    @Scope(SCOPE_SINGLETON)
    public CampsiteRepository defaultCampsiteRepository() {
        return new DefaultCampsiteRepository(CampsiteDb.class, em);
    }

    @Bean
    @Scope(SCOPE_SINGLETON)
    public ReservationRepository defaultReservationRepository() {
        return new DefaultReservationRepository(ReservationDb.class, em);
    }

    @Bean
    @Scope(SCOPE_SINGLETON)
    public SpotRepository defaultSpotRepository() {
        return new DefaultSpotRepository(SpotDb.class, em);
    }

    @Bean
    @Scope(SCOPE_SINGLETON)
    public ReservedSpotRepository defaultReservedSpotRepository() {
        return new DefaultReservedSpotRepository(ReservedSpotDb.class, em);
    }
}
