package com.upgrade.campsite.core.port.repository;

import com.upgrade.campsite.core.entity.Camper;

import java.util.Optional;

public interface CamperRepository {

    /**
     * Find camper by ID.
     *
     * @param id camper ID
     * @return optional container for the Camper
     */
	Optional<Camper> findOne(Long id);

    /**
     * Find a camper by Email.
     *
     * @param email camper email
     * @return optional container for the Camper
     */
    Optional<Camper> findOneByEmail(String email);

    /**
     * Create or update the Camper data.
     *
     * @param camper Camper with the data to create or update
     * @return The camper created or updated
     */
    Camper createOrUpdate(Camper camper);
}
