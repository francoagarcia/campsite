package com.upgrade.campsite.entrypoint.impl;

import com.upgrade.campsite.core.port.util.HashingHelper;
import com.upgrade.campsite.core.usecase.ModifyReservation;
import com.upgrade.campsite.entrypoint.ModifyReservationEndpoint;
import com.upgrade.campsite.entrypoint.resource.ReservationResource;

import javax.inject.Inject;
import java.time.LocalDate;

public class DefaultModifyReservationEndpoint implements ModifyReservationEndpoint {

    private ModifyReservation modifyReservation;
    private HashingHelper hashingHelper;

    @Inject
    public DefaultModifyReservationEndpoint(ModifyReservation modifyReservation,
                                            HashingHelper hashingHelper) {
        this.modifyReservation = modifyReservation;
        this.hashingHelper = hashingHelper;
    }

    @Override
    public ReservationResource execute(String campsiteId, String bookingNumber, ModifyReservationRequest request) {
        Long reservationId = this.hashingHelper.decode( bookingNumber );
        String fullname = request.getFullname();
        String email = request.getEmail();
        LocalDate arrivalDate = request.getArrivalDate();
        Integer requiredSpots = request.getRequiredSpots();
        LocalDate departureDate = request.getDepartureDate();

        var reservationModified = modifyReservation
            .initialize(campsiteId, reservationId, fullname, email, arrivalDate, departureDate, requiredSpots)
            .get();

        String bookingId = hashingHelper.obfuscate(reservationModified.getBookingNumber());

        return new ReservationResource(bookingId);
    }
}
