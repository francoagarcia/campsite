package com.upgrade.campsite.configuration.injector;

import com.upgrade.campsite.entrypoint.*;
import com.upgrade.campsite.entrypoint.resource.ScheduleEventByRangeResource;
import com.upgrade.campsite.entrypoint.resource.ScheduleEventBySpotResource;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

@ControllerAdvice
public class ModelBinder implements ApplicationContextAware {
    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @ModelAttribute
    public RetrieveCamperByIdEndpoint retrieveCamperByIdEndpoint() {
        return applicationContext.getBean(RetrieveCamperByIdEndpoint.class);
    }

    @ModelAttribute
    public RetrieveCampsiteAvailabilityEndpoint<ScheduleEventByRangeResource> retrieveCampsiteIsAvailableEndpointByRange() {
        return applicationContext.getBean("retrieveCampsiteIsAvailableEndpointByRange", RetrieveCampsiteAvailabilityEndpoint.class);
    }

    @ModelAttribute
    public RetrieveCampsiteAvailabilityEndpoint<ScheduleEventBySpotResource> retrieveCampsiteIsAvailableEndpointBySpot() {
        return applicationContext.getBean("retrieveCampsiteIsAvailableEndpointBySpot", RetrieveCampsiteAvailabilityEndpoint.class);
    }

    @ModelAttribute
    public ReserveCampsiteSpotsEndpoint reserveCampsiteSpotsEndpoint() {
        return applicationContext.getBean(ReserveCampsiteSpotsEndpoint.class);
    }

    @ModelAttribute
    public CancelReservationEndpoint cancelReservationEndpoint() {
        return applicationContext.getBean(CancelReservationEndpoint.class);
    }

    @ModelAttribute
    public ModifyReservationEndpoint modifyReservationEndpoint() {
        return applicationContext.getBean(ModifyReservationEndpoint.class);
    }

}
