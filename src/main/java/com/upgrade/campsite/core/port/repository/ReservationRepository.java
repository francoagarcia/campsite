package com.upgrade.campsite.core.port.repository;

import com.upgrade.campsite.core.entity.Campsite;
import com.upgrade.campsite.core.entity.DateRange;
import com.upgrade.campsite.core.entity.Reservation;

import java.util.List;
import java.util.Optional;

public interface ReservationRepository {

    /**
     * Find Reservation by ID with it's Camper data loaded.
     *
     * @param id reservation ID
     * @return optional container for the Reservation
     */
    Optional<Reservation> findOne(Long id);

    /**
     * Find all reservations that are active for a given Campsite.
     * Each Reservation with it's Camper data loaded.
     *
     * The 'active' reservations are those whose status is not Canceled or Finalized.
     *
     * @param campsite
     * @return list with the reservations
     */
    List<Reservation> findActiveReservations(Campsite campsite);

    /**
     * Find all reservations that are active for a given Campsite and DateRange.
     * Each Reservation with it's Camper data loaded.
     *
     * The 'active' reservations are those whose status is not Canceled or Finalized.
     *
     * @param campsite
     * @return list with the reservations
     */
    List<Reservation> findActiveReservations(Campsite campsite, DateRange dateRange);

    /**
     * Create a new reservation for a given campsite.
     *
     * @param reservation Reservation to create
     * @param campsite Reservation's campsite
     * @return the reservation created with its id
     */
    Reservation createReservationForCampsite(Reservation reservation, Campsite campsite);

    /**
     * Update the reservation's status, arrivalDate, departureDate
     * and campsite_id. Updates only the non-null attributes.
     *
     * @param reservation reservation with the data to update
     */
    void update(Reservation reservation);
}
